%!TEX root=zenvisageql.tex

\later{
\section{Expressiveness}
\label{subsec:expressivenesss}

So far, we have described, via a collection of examples,
the various types of queries that can be posted using \zq.
A natural question now arises: {\em how can we characterize
the space of queries expressible via \zq?}
In this section, we provide both intuition for the space
of queries expressible via \zq, as well as more formal arguments.
Note, however, that \zq provides considerable flexibility to the user,
specifically via the Process and the Viz column;
as a result, there may be uses of \zq we still haven't foreseen,
and thus, our arguments will be necessarily incomplete.

\subsection{Formalized Capabilities}

We now discuss
the capabilities of \zq more formally.
For the rest of our discussion, 
we will assume that the Viz column 
uses
a simple aggregate, 
as opposed to user-defined interpolation or summarization.
We will relax this restriction later on in the section.






\paragraph{Language Formalization.}
The \zq language is straightforward to describe 
at a high level.
A \zq query $Q$ is formally defined
to be a collection of expressions $E$,
where each 
expression\footnote{\scriptsize Note that while we enforced that every Process must be associated with a Visual Component in our tabular interface, it need not be so; this decision 
was made primarily for compactness and intuitiveness. Here,
we decouple the two.} 
$E$
can be either a Visual Component expression $V$,
or a Process expression $P$.
A Visual Component expression $V$
must contain a tuple of sub-expressions,
for each of $X, Y$ and optionally, $Z_1,$$ Z_2,$$ \ldots,$$ Constraints,$ $Viz$.
A Process expression $P$
consists of three parts: a ranking function expression $S$
describing how the visualizations must be ranked,
a functional expression $F$ describing the computation that determines the ranking, and $k$, describing how many outputs are desired from the ranking.
Thus, formally:

$Q:= \{R\}$ where $ R:= V | P$

$V:= (X, Y, Z_1, Z_2, \ldots, Z_q, Constraints)$

$P:= (S, F, k)$

\noindent We next describe how $V$ and $P$ are interpreted.

\paragraph{Visual Components and Unit Visualizations.}
To describe how $V$ is interpreted,
we begin with the notion
of a {\em unit visualization}.
A unit visualization corresponds to 
a table that is the result of
a query of the following type:
\begin{quote} \small
\begin{verbatim}
SELECT X, A(Y)
FROM R1, R2, ..., Rp
WHERE Conditions
GROUP BY X 
ORDER BY X
\end{verbatim}
\end{quote}
where {\tt X, Y, A, Conditions} can vary.
Thus, a unit visualization corresponds to
a two-attribute aggregated table.
Such two-attribute tables can be readily visualized.
It is easy to see that the following holds:
\vspace{-5pt}
\begin{lemma}
Every visual component generates one or more unit visualizations.
\end{lemma}
We define a {\em unit visualization cross-product}
to be all unit visualizations formed from:
 combining
any $X_i$ in a set of attributes \overl{$X$}, 
any $Y_j$ in a set of attributes \overl{$Y$},
such that attribute $Z_i$ is equated to any attribute value $v_i$ in a set \overl{$V_i$}.
These visualizations can be represented using the following query,
where $X_i, Y_j, v_1, ..., v_q$ can vary:
\begin{quote} \small
\begin{verbatim}
SELECT Xi, A(Yj) 
FROM R1, R2, ..., Rp
WHERE Z1 = v1 AND ... AND Zq = vq AND Conditions
GROUP BY Xi
ORDER BY Xi
\end{verbatim}
\end{quote}
We have the following:
\begin{lemma}
Any unit visualization cross-product can be represented using a visual component.
\end{lemma}
\vspace{-5pt}
Thus, unit visualizations have close ties
to the visual components:
essentially, the visual components enable us
to assemble a collection of unit visualizations
by retrieving data from the database.

\paragraph{Visual Component Processor.}
%\agp{correspondence to axis variables? to polaris table algebra?}
We now describe how a visual component expression $V$ 
is processed
within \zv. 
To do that, we need to formally define name and axis variables. 
Name variables are variables that contain an array of unit
visualizations.
(As a result, we can say that each name variable corresponds
to a sequence of visualizations.)
Axis variables, on the other hand are variables 
that contain an array of elements: this could be attribute values,
or names of attributes themselves. 


\begin{figure}[!h]
\vspace{-5pt}

\begin{center}
\includegraphics[width=0.9\columnwidth]{figs/types.pdf}
\includegraphics[width=0.9\columnwidth]{figs/process-types.pdf}

\end{center}
\vspace{-10pt}
\caption{Visual Component Processor Flow (above) Task Processor Flow (below)}
\label{fig:flow}
\vspace{-5pt}
\end{figure}

A visual component processor 
(depicted in Figure~\ref{fig:flow} (top)) 
accepts as input
a visual component expression $V$,
as well as values for unspecified 
input axis variables $u_1, ..., u_n$,
within the visual component expression.
The output is an instantiation of the 
name variable: that is, it is an array of 
unit visualizations.
Additional outputs may include 
instantiations of output axis variables.
(These are used to refer to 
specific aspects or metadata of the unit visualizations.)
For instance, one axis variable $v_i$ may hold
an array with the values taken on by attribute $Z_i$
for each unit visualization in the name variable array.

\begin{figure}
\begin{center}
\vspace{-10pt}
\includegraphics[width=\columnwidth]{figs/patterns.pdf}
\end{center}
\vspace{-10pt}
\caption{Patterns in Task Computation (left: star pattern,  middle: complete graph pattern, right: bipartite pattern)}
\label{fig:patterns}
\vspace{-15pt}
\end{figure}

\paragraph{Task Processor.}
A task processor takes as a process expression $P$, 
as well as values for the 
unspecified input axis variables $u_1, ..., u_n$,
and unspecified name variables 
(or arrays of unit visualizations) $f_1, ..., f_m$.
It then generates instantiations of output axis variables. 



Thus, the Task Processor accepts a 
sequence of unit visualizations as input via name variables,
and performs some sub-selection (filtration) and/or ordering on these visualizations.
The eventual result is some subset of the unit visualizations that
are provided as input, ordered in some way.
Note that instead of generating an instantiation of
a single name variable (like the Visual Component Processor), the task processor instantiates
multiple axis variables: this distinction is important, because
by instantiating multiple axis variables, 
these axis variables could be used to generate multiple sets
of output unit visualizations via Visual Components.
It is the role of the Visual Component Processor to
generate the unit visualizations.

\paragraph{Process Primitives.} For populating
the Process Expression, 
\zq provides a few default functional primitives, 
including $D, T, R$.
Since the process can be overloaded by users
to specify their own user defined functions, it is not clear a-priori 
what types of computation may be possible via the Process expression.

Notice however, that all three of $D, T, R$ are computing
distance of some sort:
$D$ is explicitly computing a distance between a pair of visualizations,
$T$ is checking the overall trend of a visualization 
so is computing a distance between a visualization and the
trend,
$R$ is identifying representatives, and implicitly needs
to compute distances between all pairs of visualizations
in the collection.

Using these three primitives, and how we've used them in queries,
we have identified three patterns of distance computation 
that are expressible
in tasks, and capture all the examples described in the paper.
These patterns are depicted in Figure~\ref{fig:patterns}.
Each node depicts a visualization, while each edge between two visualizations corresponds to a distance computation between them.
The pattern on the left is a {\em star} pattern,
which compares the distance between 
a collection of unit visualizations to
a reference. 
This pattern is used for queries in Table~\ref{tab:motiv-ex-2} and Table~\ref{tab:name-1}.
The pattern in the middle is a {\em complete graph} pattern,
where unit visualizations are all compared against each other.
This pattern is used for queries in Table~\ref{tab:process-2}, Table~\ref{tab:ex-1}, and Table~\ref{tab:ex-4}.
Lastly, the pattern on the right is a {\em bipartite graph} pattern,
where pairs of corresponding unit visualizations
are compared against each other.
This pattern is used for queries in Table~\ref{tab:vars-1} and Table~\ref{tab:vars-3}.


\paragraph{Overall Capabilities.}
Thus, at a high level, 
\zv is tailored towards first, enumerating an array of unit visualizations
using visual components, and second,
task-based computation involving one of the three patterns 
displayed in Figure~\ref{fig:patterns},
to identify unit visualizations within this array 
with desired properties,
followed by repetitions of these two above steps.


% Note that the task cannot be used to compare an arbitrary
% collection of unit visualizations with each other:
% specifically, the schema of these unit visualizations
% must be the same, even if XXX


% \zv provides a few functional primitives by default as discussed in sub-section~\ref{sec:process}, and users can add their own simple primitives, or build complex functions using the simple functions. $F$ takes as input one or more name variables $v*$ pointing to a set of visualizations, and axis variable $a*$ to iterate over the sets. In particular, for each input axis variable, a {\tt for} loop is created and nested. The input axis variables are then used to step through the arrays specified by the name variables $v*$, and F is called at each iteration. $F$ can step through a set of visualizations and give a score based on how closely the visualization meets the objective. $F$ can also iterate through multiple sets of visualizations and compare mutiple visualizations corresponding to the same axis variable value. The output of $F$ is a list of axis variable values, and their scores. A ranking (and filtering) function $R$ is then applied to sort and pick top $k$ axis variable values. The output of the P is an iterator to the top $k$ values returned by $R$ which can be used in $V$ of successive rows.

% \tar{add examples of specific applications that the language can support}

\paragraph{Viz Column.}
So far, we have assumed that the Viz column
uses a simple aggregate for $Y$:
in fact, the Viz column could
apply some restricted set of 
data transformations, binning in arbitrary ways, noise removal,
and mappings to different types of visualizations,
as described in Section~\ref{sec:viz}.
However, as described in that section, we are
directly leveraging the capabilities of the grammar
of graphics to support these operations. 

% Through the Viz column, the language supports data transformations, data summarization, and mapping to different types of visualizations as discussed in sub-section~\ref{sec:viz}: If the summarization is specified in the Viz column, additional clauses and keywords such as {\tt GROUP BY}s and aggregations may be added. Additionally, result visualizer can map the fetched data into the visualization type specified under the Viz column. 

}

\section{Discussion of the Space of Use Cases}

As described above, \zq is targeted at 
generating simple unit visualizations 
via visual components, and then filtrating 
and ordering them via tasks,
which can be fed as input to 
other visual components or output to the user.

The visual components generate unit visualizations: 
the unit visualizations \zq is capable of 
handling correspond to a single 
SELECT-FROM-WH\-ERE-GROUPBY-ORDER; 
complicated nested queries are not the target
of \zv, since they are not usually the target 
of visual data exploration.
The tasks computation is limited to selecting 
visualizations with specific properties
from a set: even this encompasses a large collection of use-cases,
ranging from similarity and 
dissimilarity search, or to check if a visualization
has a specific property, to outlier detection and 
identification of representatives, to finding differences between 
corresponding visualizations, as we saw in the previous sections.

This implies that there is a large swath of data mining 
tasks that are 
not captured by \zq: this includes prediction of any sort, classification,
regression, dimensionality reduction, 
or frequent pattern mining. 
There are other data processing tasks that are not captured
by \zq, including data cleaning or pre-processing, data integration,
or expressing complex queries or computation that don't correspond to the unit visualization template described above.

\later{
\begin{figure}[!htb]
\begin{center}
\includegraphics[width=0.8\columnwidth]{figs/steps.pdf}
\end{center}
\vspace{-20pt}
\caption{The three stages of data analytics currently: the shaded box
is our focus}
\label{fig:steps}
\vspace{-10pt}
\end{figure}
}

Indeed, \zq is not meant to be a one-stop-shop for all of the computation on data:
it is meant to be the next logical step for current data visualization tools,
that severely lack the functionality of helping users find desired patterns
in visualizations
or compare visualizations.
This means that the data is already cleaned before it is
visualized: this is assumed in all data visualization tools. 
This also means that complex analytics, including prediction or mining,
and are not the focus of simple visualization, are better suited for 
subsequent tools.

%In Figure~\ref{fig:steps}, we depict our conceptualization of the three stages of data analytics;
%our target is not the first or the third stages, 
%which are better handled by other tools.
%Instead, our focus is on the middle, second stage,
%which is also the focus of current visual analytics tools.








% \newpage
% \tar{I have merged expressiveness and naive translation into one as they are related, next section could directly be on Query Optimisation.}
% A \zq query $Q$ can be broken up into two parts: the visual components $V$ and the
% the Process column $P$. In the \zq compiler, $V$
% determines the set of data \zv needs to retrieve from the database,and $P$ translates into the post-processing computation to be done on
% the returned data. Formally,

% $Q:$  $R+$  

% $R:$  $V[P]$

% \textbf{[Lemma 1] Since $V$ is translated into one or more relational algebra queries, \zq queries can be executed on relational data, and are limited to the expressive power of relational queries for retrieving data:}
% For each row, the \zq compiler loops over all combinations of values \tar{we can say we use Polaris Table Algebra to determine the combination} for each of the $n$ axis variables in $V$ and for each combination, issues a \sql query, the results of which are stored in the corresponding location in an $n$ dimensional array denoted as $N$. In other words, for each row, at a high level, the \zq compiler issues a \sql query corresponding to each visualization. Each generated \sql query has the form:
% \begin{quote} \small
% \begin{verbatim}
% SELECT X, Y 
% WHERE Z=v and (CONSTRAINTS) 
% ORDER BY X
% \end{verbatim}
% \end{quote}
% \textbf{[Lemma 2] Through the Viz column, the language supports data transformations, data summarization, and mapping to different types of visualizations as discussed in sub-section}~\ref{sec:viz}: If the summarization is specified in the Viz column, additional clauses and keywords such as {\tt GROUP BY}s and aggregations may be added. Additionally, result visualizer can map the fetched data into the visualization type specified under the Viz column. 

% Once the name variable array $N$ has been filled in with results from the \sql queries, the \zq compiler can directly visualize it or pass it as an input to to the Process column $P$. 

% \textbf{[Lemma 3] Process column P supports two kinds of explorations: a) single visualization exploration: how close a visualization matches a specific visual property, and/or b) comparative exploration: compare multiple visualizations and measure how similar they are:} $P$ consists of a functional primitive $F$ and a ranking function $R$.  $F$ are considered black boxes to the \zq compiler and are unaltered. \zv provides a few funtional primitives by default as discussed in sub-section~\ref{sec:process}, and users can add their own simple primitives, or build complex functions using the simple functions. $F$ takes as input one or more name variables $v*$ pointing to a set of visualizations, and axis variable $a*$ to iterate over the sets. In particular, for each input axis variable, a {\tt for} loop is created and nested. The input axis variables are then used to step through the arrays specified by the name variables $v*$, and F is called at each iteration. $F$ can step through a set of visualizations and give a score based on how closely the visualization meets the objective. $F$ can also iterate through multiple sets of visualizations and compare mutliple visualizations corresponding to the same axis variable value. The output of $F$ is a list of axis variable values, and their scores. A ranking (and filtering) function $R$ is then applied to sort and pick top $k$ axis variable values. Formally,

% $P:$  $R(F(v*,a*),k)$

% The output of the P is an iterator to the top $k$ values returned by $R$ which can be used in $V$ of successive rows.
 
% \textbf{Applications}: Using single and comparative visualization exploration based functional primitives, \zv can filter 2D visualizations based on a visual property, can find similar or dissimilar visualizations, can find outliers, can find k nearest- neighbour visualizations, can cluster and pick trends representing each cluster or combinations of these across any of the X, Y or Z dimensions. Other similar data mining tasks can integrated by introducing a functional primitive.

% \textbf{Limitations}: \zq is currently limited to exploration of visualization with only two dimensions. Given that \zq supports only single and comparative visaualization explorations, predictive explorations such as classification, or frequent pattern mining tasks are beyond the scope of the language. Moreover, since V is translated to relational queries, \zq cannot be used where fetching data needed for visualizations depends on recursion, and other query patterns that cannot be expressed using relational algebra \tar{the last point can be more clear}. 

