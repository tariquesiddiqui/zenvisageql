%!TEX root=zenvisageql.tex


\begin{table*}[!htbp]
  \centering\scriptsize
 \vspace{-10pt}
  {\tt
    %\resizebox{\linewidth}{!}{%
      \begin{tabular}{|r|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z} & \textbf{Process}
        \\ \hline
        f1 & `year' & `sales' & v1 \IN `product'.* & v2 \IN $R(10, v1, f1)$ \\
        f2 & `year' & `sales' & v2 & v3 \IN $argmax_{v1}[k=10] \; min_{v2}
        D(f1,f2)$ \\
        *f3 & `year' & `sales' & v3  & \\ \hline
      \end{tabular}
    %}
  }
  \vspace{-10pt}\caption{A \zq query which returns 10 sales over years visualizations for
  products which are outliers compared to the rest.}
  \label{tab:process-2}
\vspace{-10pt}\end{table*}


\subsubsection{Process Column}
\label{sec:process}
%\tar{Maybe we need to revise this section, and make it more consistent with expressiveness section}
Once the visual component for a row has been named, the user may use the
Process column to sort, filter, and compare the visual component with
previously defined
visual components to isolate the set of visualizations she is interested in.
While the user is free to define her own functions for the Process column,
we have come up with a core set of primitives based on our case
studies which we believe can handle the vast majority of the common operations.
Each non-empty Process column entry is defined to be a {\em task}. 
Implicitly, a task may be impacted by one or more rows of a \zq query\techreport{ (which would be input to the process optimization),}
and may also impact one or more rows\techreport{ (which will use the outputs of the process optimization)}, 
as we will see below.

\noindent {\bf Functional Primitives.}
First, we introduce three simple functions that can be applied to
visualizations: $T$, $D$, and $R$.
\zv will use default settings for each of these functions, but
the user is free to specify their own variants for each of these functions
that are more suited to their application.


\noindent $\bullet \  T(f)$ measures the overall trend of visualization $f$. 
It is positive
if the overall trend indicates ``growth'' and negative if the overall trend goes
``down''.
There are obviously many ways that such a function can be
implemented, but one example implementation might be to measure the slope of a
linear fit to the given input visualization $f$.

\noindent $\bullet \ D(f,f')$ measures the distance between the two visualizations $f$ and
$f'$. For example, this might mean calculating the Earth Mover's Distance or the 
Kullback-Leibler Divergence between the induced probability distributions. 

\noindent $\bullet \ R(k,v,f)$ computes the set of $k$-representative visualizations given an axis
variable, $v$, and an iterator over the set of visualizations, $f$. Different
users might have different notions of what representative means, but one
example would be to run $k$-means clustering on the given set of visualizations
and return the $k$ centroids. In addition to taking in a single
axis variable, $v$, $R$ may also take in a tuple of axis variables to iterate
over. The return value of $R$ is the set of axis variable values which produced
the representative visualizations.


\noindent {\bf Processing.}
Given these functional primitives, 
\zq also provides some default sorting and filtering mechanisms: 
$argmin$, $argmax$, and $argany$. Although $argmin$ and $argmax$ are usually
used to find the best value for which the objective function is optimized, \zq
typically returns the top-$k$ values, sorted in order. $argany$ is used to
return any $k$ values. In addition to the top-$k$, the user might also like to
specify that she wants every value for which a certain threshold is met, and
\zq is able to support this as well.
Specifically, the expression {\tt v2 \IN $argmax_{v1}[k=10] D(f1,f2)$} returns
the top 10 {\tt v1} values for which $D(f1,f2)$ are maximized, sorts those in
decreasing order of the distance, and declares the variable {\tt v2} to iterate
over that set. 
This could be useful, for instance, to find the 10 visualizations with
the most variation on some attributes.
 The expression {\tt v2 \IN $argmin_{v1}[t<0] D(f1,f2)$} returns
the {\tt v1} values for which the objective function $D(f1,f2)$ is below the
threshold 0, sorts the values in increasing order of the objective function,
and declares {\tt v2} to iterate over that set. If a filtering option ($k$,$t$)
is not specified, the mechanism simply sorts the values, so {\tt v2} \IN
$argmin_{v1} T(f1)$ would bind {\tt v2} to iterate over the values of {\tt v1}
sorted in increasing order of $T(f1)$. {\tt v2 \IN $argany_{v1}[t>0] T(f1)$}
would set {\tt v2} to iterate over the values of {\tt v1} for which the $T(f1)$
is greater than 0. 
\later{Note that when using the $k$ filtering option for $argany$,
no objective function is required: {\tt v2 \IN $argany_{v1}[k=10]$}.}

Note that the mechanisms may also take in multiple axis variables, as we saw
from Table~\ref{tab:vars-3}.  The mechanism iterates over the Cartesian
product of its input variables. If a mechanism is given $k$ axis variables to
iterate over, the resulting set of values must also be bound to $k$ declared
variables. In the case of Table~\ref{tab:vars-3}, since there were two
variables to iterate over, {\tt x1} and {\tt y1}, there are two output
variables, {\tt x2} and {\tt y2}. The order of the variables is important as
the values of the $i$th input variable are set to the $i$th output variable.

\noindent 
{\bf User Exploration Tasks.}
By building on these mechanisms, the user can perform most
common tasks.
This includes the {\em similarity/dissimilarity search} 
performed in Table~\ref{tab:vars-1},
to find products that are dissimilar;
the {\em comparative search} performed in Table~\ref{tab:vars-3},
to identify attributes on which
two slices of data are dissimilar, or even the {\em outlier search}
query shown in Table~\ref{tab:process-2}, to identify the products
that are outliers on the sales over year visualizations. 
%\alkim{Verify that the outlier case is
%correct and similar to what Tarique's implementation of outliers.} 
Note that in
Table~\ref{tab:process-2}, we use two levels of iteration. 
%\agp{we should explain what these tasks mean}
\later{Other mathematical
operations such as $\sum$ and $\prod$ are also available to the user. However,
other mathematical operations such as $max$ and $min$ retain their original
meaning.}

