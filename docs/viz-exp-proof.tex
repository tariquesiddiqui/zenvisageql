%!TEX root=zenvisageql.tex

\subsection{Proof of \VzCmpness}
\label{subsec:viz-exp-proof}
We now attempt to quantify the expressiveness of \zq within the context of
\vzalg and the two functional primtives $T$ and $D$.
More
formally, we prove the following theorem:

\vspace{-7pt}
\begin{framed}
  \vspace{-12pt}
  \begin{theorem}
    Given well-defined functional primitives $T$ and $D$, \zq is \vzcmp
    with respect to $T$ and $D$: $VEC_{T,D}(\text{\zq})$ is true.
    \label{thm:main}
  \end{theorem}
  \vspace{-12pt}
\end{framed}
\vspace{-7pt}

% \papertext{
%   Although we omit the full proof here due to lack of space, the basic idea of
%   the proof is that the visualization collections in \zq are equivalent in expressive
%   power as \vzgrps in \vzalg and for each operator in \vzalg, there is a \zq
%   query that appropriately transforms the corresponding visualization collections.
%   For more details, see the extended technical report~\cite{techrepoort}.
% }


Our proof for this theorem involves two major steps:
\begin{description}
  \item[Step 1.] We show that a visualization collection in \zq has as much
    expressive power as a \vzgrp of \vzalg, and therefore a visualization
    collection in
    \zq serves as an appropriate proxy of a \vzgrp in \vzalg.
  \item[Step 2.] For each operator in \vzalg, we show that there exists a \zq
    query which takes in visualization collection semantically equivalent to the
    \vzgrp operands and produces visualization collection semantically equivalent to
    the resultant \vzgrp.
\end{description}

\begin{lemma}
  A visualization collection of \zq has at least as much expressive power as a
  \vzgrp in \vzalg.
  \label{lemma:1}
\end{lemma}
\begin{proof}
  A \vzgrp $V$, with $n$ \vzchns, is a relation with $k+2$ columns and $n$
  rows, where $k$ is the number of attributes in the original relation. We show
  that for any \vzgrp $V$, we can come up with a \zq query $q$ which can
  produce a visualization collection that represents the same set of visualizations as
  $V$.
  
  \begin{table}[H]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} \\ \hline
        f1 & $\pi_{\text{X}}(V[1])$ & $\pi_{\text{Y}}(V[1])$ & $E_{1,1}$ & ... &
        $E_{1,k}$ \\
        \vdots & \vdots & \vdots & \vdots & \vdots & \vdots \\
        fn & $\pi_{\text{X}}(V[n])$ & $\pi_{\text{Y}}(V[n])$ & $E_{n,1}$ & ... &
        $E_{n,k}$ \\
        *fn+1 \IN f1+...+fn & & & & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query $q$ which produces a visualization collection
    equal in expressiveness to \vzgrp $V$.}
    \label{tab:lemma-1}
  \vspace{-10pt}\end{table}

  Query $q$ has the format given by Table~\ref{tab:lemma-1}, where $V[i]$
  denotes the $i$th tuple of relation $V$ and:
  \[
    E_{i,j} = 
    \begin{cases}
      \text{`` ''} & \text{if } \pi_{A_j}(V[i]) = * \\
      A_j.\pi_{A_j}(V[i]) & \text{otherwise}
    \end{cases}
  \]
  Here, $A_j$ refers to the $j$th attribute of the original relation. The
  $i$th \vzchn of $V$ is represented with the {\tt fi} from $q$. The X and Y
  values come directly from the \vzchn using projection. For the {\tt Zj}
  column, if the $A_j$ attribute of \vzchn has any value than other
  than \wild, we must filter the data based on that value, so $E_{i,j} =
  A_j.\pi_{A_j}{V[i]}$. However, if the $A_j$ attribute is equal to
  \wild, then the corresponding element in {\tt fi} is left blank, signaling no
  filtering based on that attribute.

  After, we have defined a visualization collection {\tt fi} for each $i$th \vzchn in
  $V$, we take the sum (or concatenation) across all these visualization collections as
  defined in Appendix \ref{name-appex}, and the resulting
  {\tt fn+1} becomes equal to the \vzgrp $V$.
\end{proof}

\begin{lemma}
  $\vzsel_{\theta}(V)$ is expressible in \zq for all valid constraints $\theta$
  and \vzgrps $V$.
\end{lemma}
\begin{proof}
  We prove this by induction.
  
  The full context-free grammar (CFG) for $\theta$ in
  $\vzsel_{\theta}$ can be given by:
  \begin{flalign}
    \theta &\rightarrow E \;|\; E \land E \;|\; E \lor E \;|\; \epsilon &\\
    E &\rightarrow C \;|\; (E) \;|\; E \land C \;|\; E \lor C & \label{cfg:e}\\
    C &\rightarrow T_1 = B_1 \;|\; T_1 \ne B_1 \;|\; T_2 = B_2 \;|\; T_2 \ne
    B_2 &\\
    T_1 &\rightarrow X \;|\; Y &\\
    B_1 &\rightarrow A_1 \;|\; ... \;|\; A_k &\\
    T_2 &\rightarrow A_1 \;|\; ... \;|\; A_k &\\
    B_2 &\rightarrow \text{string} \;|\; \text{number} \;|\; *
  \end{flalign}
  where $\epsilon$ represents an empty string (no selection), and $X$, $Y$,
  and $A_1$, ..., $A_k$ refer to the attributes of $\calV$.

  To begin the proof by induction,
  we first show that \zq is capable of expressing the base expressions
  $\vzsel_{C}(V)$:
  $\vzsel_{T_1 = B_1}(V)$,
  $\vzsel_{T_1 \ne B_1}(V)$,
  $\vzsel_{T_2 = B_2}(V)$, and
  $\vzsel_{T_2 \ne B_2}(V)$.
  The high level idea for each of these proofs is to be come up with a
  \emph{filtering \vzgrp} $U$ which we take the intersection with to arrive at
  our desired result: $\exists U, \vzsel_{C}(V) = V \vzint U$.

  In the first two expressions, $T_1$ and $B_1$ refer to filters on the $X$ and
  $Y$ attributes of $V$; we
  have the option of either selecting a specific attribute ($T_1=B_1$) or
  excluding a specific attribute ($T_1 \ne B_1$). Tables \ref{tab:vzsel-c1} and
  \ref{tab:vzsel-c2} show \zq queries which express $\vzsel_{T_1=B_1}(V)$ for
  $T_1 \rightarrow X$ and $T_1 \rightarrow Y$ respectively. The \zq queries
  do the approximate equivalent of $\vzsel_{T_1=B_1}(V) = V \vzint
  \vzsel_{T_1=B_1}(\calV)$.

  \begin{table}[H]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} \\ \hline
        f1 & - & - & - & ... & - \\
        f2 \IN f1 & & y1 \IN \_ & v1 \IN $A_1$.\_ & ... & vk \IN $A_k$.\_ \\
        f3 & $B_1$ & y1 & v1 & ... & vk \\
        *f4 \IN f1\^{}f3 & & & & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query which expresses $\vzsel_{X=B_1}(V)$.}
    \label{tab:vzsel-c1}
  \vspace{-10pt}\end{table}

  \begin{table}[H]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} \\ \hline
        f1 & - & - & - & ... & - \\
        f2 \IN f1 & x1 \IN \_ & & v1 \IN $A_1$.\_ & ... & vk \IN $A_k$.\_ \\
        f3 & x1 & $B_1$ & v1 & ... & vk \\
        *f4 \IN f1\^{}f3 & & & & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query which expresses $\vzsel_{Y=B_1}(V)$.}
    \label{tab:vzsel-c2}
  \vspace{-10pt}\end{table}

  We have
  shown with Lemma~\ref{lemma:1} that a visualization collection is capable of
  expressing a \vzgrp, so we assume that {\tt f1}, the
  visualization collection which represents the operand $V$, is given to us for both of
  these tables. Since we
  do not know how {\tt f1} was derived, we use - for its axis variable columns.
  The second rows of these tables derive {\tt f2} from {\tt f1} and bind axis
  variables to the values of the non-filtered attributes. Here, although the
  set of visualizations present in {\tt f2} is exactly the same as {\tt f1},
  we now have a convenient way to iterate over the non-filtered attributes of
  {\tt f1} (for more information on derived visualization collections, please refer to
  Appendix~\ref{name-appex}).
  The third row
  combines the specified attribute $B_1$ with the non-filtered attributes of
  {\tt f2} to form the \emph{filtering visualization collection} {\tt f3}, which
  expresses the filtering \vzgrp $U$ from above. We then take the
  intersection between {\tt f1} and the filtering visualization collection {\tt f3} to
  arrive at our desired visualization collection {\tt f4}, which represents
  the resultant \vzgrp $\vzsel_{T_1=B_1}(V)$. Although, we earlier said that we
  would come up with {\tt f3} $=\vzsel_{T_1=B_1}(\calV)$, in truth, we come up
  with {\tt f3} $=B_1 \times \pi_{Y,A_1,...,A_k}(V)$ for $T_1 \rightarrow X$
  and {\tt f3} $=\pi_{X,A_1,...,A_k}(V) \times B_1$ for $T_1 \rightarrow Y$
  because they are easier to express in \zq; regardless we still end up with
  the correct resulting set of visualizations.

  Tables \ref{tab:vzsel-c3} and \ref{tab:vzsel-c4} show \zq queries which
  express $\vzsel_{T_1 \ne B_1}(V)$ for $T_1 \rightarrow X$ and $T_1
  \rightarrow Y$ respectively. Similar to the queries above, these queries
  perform the approximate
  equivalent of $\vzsel_{T_1 \ne B_1}(V) = V \vzint \vzsel_{T_1 \ne
  B_1}(\calV)$. We once again assume {\tt f1} is a given visualization collection which
  represents the operand $V$, and we come up with a filtering visualization collection
  {\tt f3} which mimics the effects of (though is not completely equivalent to) $\vzsel_{T_1
    \ne B_1}(\calV)$. We then take the intersection between {\tt f1} and {\tt
    f3} to arrive at {\tt f4} which represents the resulting $\vzsel_{T_1 \ne
    B_1}(V)$.

  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} \\ \hline
        f1 & - & - & - & ... & - \\
        f2 \IN f1 & x1 \IN \_ & y1 \IN \_ & v1 \IN $A_1$.\_ & ... & vk \IN $A_k$.\_ \\
        f3 & x2 \IN x1 - \{$B_1$\} & y1 & v1 & ... & vk \\
        *f4 \IN f1\^{}f3 & & & & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query which expresses $\vzsel_{X \ne B_1}(V)$.}
    \label{tab:vzsel-c3}
  \vspace{-10pt}\end{table*}

  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} \\ \hline
        f1 & - & - & - & ... & - \\
        f2 \IN f1 & x1 \IN \_ & y1 \IN \_ & v1 \IN $A_1$.\_ & ... & vk \IN $A_k$.\_ \\
        f3 & x1 & y2 \IN y1 - \{$B_1$\} & v1 & ... & vk \\
        *f4 \IN f1\^{}f3 & & & & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query which expresses $\vzsel_{Y \ne B_1}(V)$.}
    \label{tab:vzsel-c4}
  \vspace{-10pt}\end{table*}

  The expressions $\vzsel_{T_2=B_2}$ and $\vzsel_{T_2 \ne B_2}$ refer to filters on the
  $A_1,...,A_k$ attributes of $V$. Specifically, $T_2$ is some attribute $A_j
  \in \{A_1,...,A_k\}$ and $B_2$ is the attribute value which is selected or
  excluded.
  Here, we have an additional complication to the proof since any attribute
  $A_j$ can also filter for or exclude \wild. First, we
  show \zq is capable of expressing $\vzsel_{T_2 = B_2'}$ and $\vzsel_{T_2
  \ne B_2'}$ for which $B_2' \neq *$; that is $B_2'$ is any attribute
  value which is not \wild. Tables \ref{tab:vzsel-c5} and \ref{tab:vzsel-c6}
  show the \zq queries which express $\vzsel_{T_2 = B_2'}(V)$ and $\vzsel_{T_2
  \ne B_2'}(V)$ respectively. Note the similarity between these queries and the
  queries for $\vzsel_{T_1=B_1}(V)$ and $\vzsel_{T_1 \ne B_1}(V)$.

  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...} &
        \textbf{Zj} & \textbf{...} & \textbf{Zk} \\ \hline
        f1 & - & - & - & ... & - & ... & - \\
        f2 \IN f1 & x1 \IN \_ & y1 \IN \_ & v1 \IN $A_1$.\_ & ... & & ... & vk \IN $A_k$.\_ \\
        f3 & x1 & y1 & v1 & ... & $B_2'$ & ... & vk \\
        *f4 \IN f1\^{}f3 & & & & & & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query which expresses $\vzsel_{A_j =  B_2'}(V)$
    when $B_2' \ne *$.}
    \label{tab:vzsel-c5}
  \vspace{-10pt}\end{table*}

  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...} &
        \textbf{Zj} & \textbf{...} & \textbf{Zk} \\ \hline
        f1 & - & - & - & ... & - & ... & - \\
        f2 \IN f1 & x1 \IN \_ & y1 \IN \_ & v1 \IN $A_1$.\_ & ... & vj \IN $A_j$.\_ & ... & vk \IN $A_k$.\_ \\
        f3 & x1 & y1 & v1 & ... & uj \IN vj - \{$B_2'$\} & ... & vk \\
        *f4 \IN f1\^{}f3 & & & & & & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query which expresses $\vzsel_{A_j \ne
    B_2'}(V)$ when $B_2' \ne *$.}
    \label{tab:vzsel-c6}
  \vspace{-10pt}\end{table*}

  For $\vzsel_{T_2=*}(V)$ and $\vzsel_{T_2 \ne *}(V)$, Tables
  \ref{tab:vzsel-c7} and \ref{tab:vzsel-c8} show the corresponding queries. In
  Table~\ref{tab:vzsel-c7}, we explicitly avoid setting a value for {\tt Zj}
  for {\tt f3} to emulate $A_j=*$ for the filtering visualization collection. In
  Table~\ref{tab:vzsel-c8}, {\tt f3}'s {\tt Zj} takes on all possible values
  from $A_j${\tt.*}, but that means that a value is set for {\tt Zj} (i.e.,
  $T_2 \ne *$).

  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...} &
        \textbf{Zj} & \textbf{...} & \textbf{Zk} \\ \hline
        f1 & - & - & - & ... & - & ... & - \\
        f2 \IN f1 & x1 \IN \_ & y1 \IN \_ & v1 \IN $A_1$.\_ & ... & & ... & vk \IN $A_k$.\_ \\
        f3 & x1 & y1 & v1 & ... & & ... & vk \\
        *f4 \IN f1\^{}f3 & & & & & & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query which expresses $\vzsel_{A_j = *}(V)$.}
    \label{tab:vzsel-c7}
  \vspace{-10pt}\end{table*}

  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...} &
        \textbf{Zj} & \textbf{...} & \textbf{Zk} \\ \hline
        f1 & - & - & - & ... & - & ... & - \\
        f2 \IN f1 & x1 \IN \_ & y1 \IN \_ & v1 \IN $A_1$.\_ & ... & & ... & vk \IN $A_k$.\_ \\
        f3 & x1 & y1 & v1 & ... & vj \IN $A_j$.* & ... & vk \\
        *f4 \IN f1\^{}f3 & & & & & & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query which expresses $\vzsel_{A_j \ne *}(V)$.}
    \label{tab:vzsel-c8}
  \vspace{-10pt}\end{table*}

  Now that we have shown how to express the base operations, we next assume \zq
  is capable of expressing any arbitrary complex filtering operations
  $\vzsel_{E'}$ where $E'$ comes from Line~\ref{cfg:e} of the CFG.
  Specifically, we assume that given a visualization collection {\tt f1} which
  expresses $V$, there exists a filtering visualization collection {\tt f2} for which
  $\vzsel_{E'}(V)=$ {\tt f1\^{}f2}. Given this assumption, we now must take the
  inductive step, apply Line~\ref{cfg:e}, and prove that $\vzsel_{E \rightarrow (E')}(V)$,
  $\vzsel_{E \rightarrow E' \land C}(V)$, and $\vzsel_{E \rightarrow E' \lor
  C}(V)$ are all expressible in
  \zq for any base constraint $C$.

  %\begin{enumerate}
  %  \item
  %    $\vzsel_{E \rightarrow (E')}(V)$ is trivial. Given {\tt f1} which
  %    represents $V$ and {\tt f2} which is the filtering visualization collection for
  %    $E'$, we simply the intersect the two to get {\tt f4 \IN f1\^{}f2} which
  %    represents $\vzsel_{E \rightarrow (E')}$.
  %\end{enumerate}
  \paragraph{$\vzsel_{E \rightarrow (E')}(V)$:} This case is trivial. Given
  {\tt f1} which represents $V$ and {\tt f2} which is the filtering
  visualization
  collection for $E'$, we simply the intersect the two to get {\tt f3 \IN f1\^{}f2}
  which represents $\vzsel_{E \rightarrow (E')}$.

  \paragraph{$\vzsel_{E \rightarrow E' \land C}$:} Once again assume we are
  given {\tt f1} which represents $V$ and {\tt f2} which is the filtering
  visualization collection of $E'$. Based on the base expression proofs above, we know
  that given any base constraint $C$, we can find a filtering visualization collection
  for it; call this filtering visualization collection {\tt f3}. We can then see that
  {\tt f2\^{}f3} is the filtering visualization collection of $E \rightarrow E' \land C$, and {\tt
  f4 \IN f1\^{}(f2\^{}f3)} represents $\vzsel_{E \rightarrow E \land C}(V)$.

  \paragraph{$\vzsel_{E \rightarrow E' \lor C}$:} Once again assume we are
  given {\tt f1} which represents $V$, {\tt f2} which is the filtering
  visualization collection of $E'$, and we can find a filtering visualization collection {\tt
  f3} for $C$.  We can then see that {\tt f2+f3} is the filtering visualization
  collection of $E \rightarrow E' \lor C$, and {\tt f4 \IN f1\^{}(f2+f3)} represents $\vzsel_{E
  \rightarrow E \lor C}(V)$.

  With this inductive step, we have shown that for all complex constraints $E$
  of the form given by Line~\ref{cfg:e} of the CFG, we can find a \zq query
  which expresses $\vzsel_{E}(V)$. Given this, we can finally show that \zq is capable
  of expressing $\vzsel_{\theta}(V)$ for all $\theta$: $\vzsel_{\theta \rightarrow E}(V)$,
  $\vzsel_{\theta \rightarrow E \land E')}(V)$,
  $\vzsel_{\theta \rightarrow E \lor E'}(V)$, and
  $\vzsel_{\theta \rightarrow \epsilon}(V)$.

  \paragraph{$\vzsel_{\theta \rightarrow E}(V)$:} This case is once again
  trivial. Assume, we are given {\tt f1} which represents $V$, and {\tt f2},
  which is the filtering visualization collection of $E$, {\tt f3 \IN f1\^{}f2} represents
  $\vzsel_{\theta \rightarrow E}(V)$.

  \paragraph{$\vzsel_{\theta \rightarrow E \land E'}(V)$:} Assume, we are given
  {\tt f1} which represents $V$, {\tt f2}, which is the filtering visualization collection of
  $E$, and {\tt f3}, which is the filtering visualization collection of $E'$.
  {\tt f2\^{}f3} is the filtering visualization collection of $\theta \rightarrow E
  \land E'$, and {\tt f4 \IN f1\^{}(f2\^{}f3)} represents $\vzsel_{\theta
  \rightarrow E \land E'}(V)$.

  \paragraph{$\vzsel_{\theta \rightarrow E \lor E'}(V)$:} Assume, we are given
  {\tt f1} which represents $V$, {\tt f2} which is the filtering visualization collection of
  $E$, and {\tt f3} which is the filtering visualization collection of $E'$.
  {\tt f2+f3} is the filtering visualization collection of $\theta \rightarrow E
  \lor E'$, and {\tt f4 \IN f1\^{}(f2+f3)} represents $\vzsel_{\theta
  \rightarrow E \lor E'}(V)$.

  \paragraph{$\vzsel_{\theta \rightarrow \epsilon}(V)$:} This is the case in
  which no filtering is done. Therefore, given {\tt f1} which represents $V$,
  we can simply return {\tt f1}.
\end{proof}

\begin{lemma}
  $\vzsort_{F(T)}(V)$ is expressible in \zq for all valid
  functionals $F$ of $T$ and \vzgrps $V$.
\end{lemma}
\begin{proof}
  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} & \textbf{Process} \\ \hline
        f1 & - & - & - & ... & - & \\
        f2 \IN f1 & x1 \IN \_ & y1 \IN \_ & v1 \IN $A_1$.\_ & .. & vk \IN $A_k$.\_
        &
        x2, y2, u1, ..., uk \IN $argmin_{x1,y1,v1,...,vk}[k=\infty] F(T)(f2)$ \\
        *f3 & x2 & y2 & u1 & ... & uk & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query $q$ which expresses $\vzsort_{F(T)}(V)$.
    }
    \label{tab:proof-vzsort}
  \vspace{-10pt}\end{table*}

  Assume {\tt f1} is the visualization collection which represents $V$. Query $q$
  given by Table~\ref{tab:proof-vzsort} produces visualization collection {\tt f3}
  which expresses $\vzsort_{F(T)}(V)$.
\end{proof}

\begin{lemma}
  $\vzlimit_{[a:b]}(V)$ is expressible in \zq for all valid intervals $a:b$
  and \vzgrps $V$.
\end{lemma}
\begin{proof}
  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} & \textbf{Process} \\ \hline
        f1 & - & - & - & ... & - & \\
        *f2 \IN f1[a:b] &  & & & & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query $q$ which expresses $\vzlimit_{[a:b]}(V)$.
    }
    \label{tab:proof-vzlimit}
  \vspace{-10pt}\end{table*}

  Assume {\tt f1} is the visualization collection which represents $V$. Query $q$
  given by Table~\ref{tab:proof-vzlimit} produces visualization collection {\tt f2}
  which expresses $\vzlimit_{[a:b]}(V)$.
\end{proof}

%\begin{lemma}
%  $\vzrep_{R,j}(V)$ is expressible in \zq for all valid numbers $j$
%  and \vzgrps $V$.
%\end{lemma}
%\begin{proof}
%  \begin{table*}[!htbp]
%    \centering\scriptsize
%    {\tt   
%      \begin{tabular}{|r|l|l|l|l|l|l|} \hline
%        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
%        & \textbf{Zk} & \textbf{Process} \\ \hline
%        f1 & - & - & - & ... & - & \\
%        f2 \IN f1 & x1 \IN \_ & y1 \IN \_ & v1 \IN $A_1$.\_ & ... & vk \IN $A_k$.\_
%        & 
%        x2, y2, u1, ..., uk \IN $R(j,x1,y1,v1,...,vk,f2)$ \\
%        *f3 & x2 & y2 & u1 & ... & uk & \\
%        \hline
%      \end{tabular}
%    }
%    \vspace{-10pt}\caption{\zq query $q$ which expresses $\vzrep_{R,j}(V)$.
%    }
%    \label{tab:proof-vzrep}
%  \vspace{-10pt}\end{table*}
%
%  Assume {\tt f1} is the visualization collection which represents $V$ and $R'$ is the
%  corresponding representative-finding algorithm in \zq. Query $q$
%  given by Table~\ref{tab:proof-vzrep} produces visualization collection {\tt f3}
%  which expresses $\vzlimit_{[a:b]}(V)$.
%\end{proof}

\begin{lemma}
  $\vzdedup(V)$ is expressible in \zq for all valid \vzgrps $V$.
\end{lemma}
\begin{proof}
  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} & \textbf{Process} \\ \hline
        f1 & - & - & - & ... & - & \\
        *f2 \IN f1 &  & & & & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query $q$ which expresses $\vzdedup(V)$.
    }
    \label{tab:proof-vzdedup}
  \vspace{-10pt}\end{table*}

  Assume {\tt f1} is the visualization collection which represents $V$. Query $q$
  given by Table~\ref{tab:proof-vzdedup} produces visualization collection {\tt f2}
  which expresses $\vzdedup(V)$.
\end{proof}

\begin{lemma}
  $V \vzunion U$ is expressible in \zq for all valid \vzgrps $V$ and $U$.
\end{lemma}
\begin{proof}
  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} & \textbf{Process} \\ \hline
        f1 & - & - & - & ... & - & \\
        f2 & - & - & - & ... & - & \\
        *f3 \IN f1+f2 & & & & ... & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query $q$ which expresses $V \vzunion U$.
    }
    \label{tab:proof-vzunion}
  \vspace{-10pt}\end{table*}

  Assume {\tt f1} is the visualization collection which represents $V$ and {\tt f2}
  represents $U$.  Query $q$ given by Table~\ref{tab:proof-vzunion} produces
  visualization collection {\tt f3} which expresses $V \vzunion U$.
\end{proof}

\begin{lemma}
  $V \vzdiff U$ is expressible in \zq for all valid \vzgrps $V$ and $U$.
\end{lemma}
\begin{proof}
  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} & \textbf{Process} \\ \hline
        f1 & - & - & - & ... & - & \\
        f2 & - & - & - & ... & - & \\
        *f3 \IN f1-f2 & & & & ... & & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query $q$ which expresses $V \vzdiff U$.
    }
    \label{tab:proof-vzdiff}
  \vspace{-10pt}\end{table*}

  Assume {\tt f1} is the visualization collection which represents $V$ and {\tt f2}
  represents $U$.  Query $q$ given by Table~\ref{tab:proof-vzdiff} produces
  visualization collection {\tt f3} which expresses $V \vzdiff U$. The proof for
  $\vzint$ can be shown similarly.
\end{proof}

\begin{lemma}
  $\vzswap_{A}(V, U)$ is expressible in \zq for all valid attributes $A$ in
  $\calV$ and \vzgrps $V$ and $U$.
\end{lemma}
\begin{proof}

  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} \\ \hline
        f1 & - & - & - & ... & - \\
        f2 & - & - & - & ... & - \\
        f3 \IN f1 & & y1 \IN \_ & v1 \IN $A_1$.\_ & ... & vk \IN $A_k$.\_ \\
        f4 \IN f2 & x1 \IN \_ & & & & \\
        *f5 & x1$^2$ & y1$^1$ & v1$^1$ & ... & vk$^1$ \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query $q$ which expresses $\vzswap_{A}(V, U)$
      where $A=X$.
    }
    \label{tab:proof-vzswap1}
  \vspace{-10pt}\end{table*}

  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} \\ \hline
        f1 & - & - & - & ... & - \\
        f2 & - & - & - & ... & - \\
        f3 \IN f1 & x1 \IN \_ & & v1 \IN $A_1$.\_ & ... & vk \IN $A_k$.\_ \\
        f4 \IN f2 & & y1 \IN \_ & & & \\
        *f5 & x1$^1$ & y1$^2$ & v1$^1$ & ... & vk$^1$ \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query $q$ which expresses $\vzswap_{A}(V, U)$
      where $A=Y$.
    }
    \label{tab:proof-vzswap2}
  \vspace{-10pt}\end{table*}

  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zj-1} & \textbf{Zj} & \textbf{Zj+1} & \textbf{...}
        & \textbf{Zk} \\ \hline
        f1 & - & - & - & ... & - & - & - &... & - \\
        f2 & - & - & - & ... & - & - & - &... & - \\
        f3 \IN f1 & x1 \IN \_ & y1 \IN \_ & v1 \IN $A_1$.\_ & ... & vj-1 \IN
        $A_{j-1}$.\_ & & vj+1 \IN $A_{j+1}$.\_ & ... & vk \IN $A_k$.\_ \\
        f4 \IN f2 & & & & & & uj \IN $A_{j}$.\_ & & & \\
        *f5 & x1$^1$ & y1$^1$ & v1$^1$ & ... & vj-1$^1$ & uj$^2$ & vj+1$^1$ & ... & vk$^1$ \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query $q$ which expresses $\vzswap_{A}(V, U)$
      where $A=A_j$ and $A_j$ is an attribute from $\calR$
    }
    \label{tab:proof-vzswap3}
  \vspace{-10pt}\end{table*}

  Assume {\tt f1} is the visualization collection which represents $V$ and {\tt f2}
  represents $U$. There are three cases we must handle depending on the value
  of $A$ due to the structure of columns in \zq: \begin{inparaenum}[\itshape
    (i)\upshape] \item $A=X$ \item $A=Y$ \item $A=A_j$ where $A_j$ is an
  attribute from the original relation $\calR$ \end{inparaenum}. For each
  of the three cases, we produce a separate query which expresses $\vzswap$.
  For $A=X$, the query given by Table~\ref{tab:proof-vzswap1} produces {\tt f5}
  which is equivalent to $\vzswap_{X}(V,U)$ We use the superscripts 
  in the last row so that cross product conforms to the ordering defined in
  Section~\ref{sec:viz-exp-oper}. For more information about the superscripts,
  please refer to Appendix~\ref{name-appex}. For $A=Y$, the query given by
  Table~\ref{tab:proof-vzswap2} produces {\tt f5} which is equivalent to
  $\vzswap_{Y}(V,U)$, and for $A=A_j$, the query given by
  Table~\ref{tab:proof-vzswap3} produces {\tt f5} which is equivalent to
  $\vzswap_{A_j}(V,U)$.
  %Query $q$ given by Table~\ref{tab:proof-vzswap} produces
  %visualization collection {\tt f5} which expresses $\vzswap_{A_j}(V,U)$.
\end{proof}

\begin{lemma}
  $\vzdist_{F(D),A_1,...,A_j}(V, U)$ is expressible in \zq for all valid
  attributes $A_1,...,A_j$ and \vzgrps $V$ and $U$.
\end{lemma}
\begin{proof}

  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...} &
        \textbf{Zj} &
        \textbf{Process} \\ \hline
        f1 & - & - & - & ... & - & \\
        f2 & - & - & - & ... & - & \\
        f3 \IN f1 & & & v1 \IN $A_1$.\_ & ...  & vj \IN
        $A_j$.\_ & \\
        f4 \IN f2.order & & & v1 \TO & ... & vj \TO & u1, ..., uj \IN
        $argmin_{v1,...,vj}[k=\infty] F(D)(f3,f4)$\\
        *f5 \IN f1.order & & & u1 \TO & ... & uj \TO & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query $q$ which expresses
      $\vzdist_{F(D),A_1,...,A_j}(V, U)$.
    }
    \label{tab:proof-vzdist}
    \vspace{-10pt}\end{table*}

  Assume {\tt f1} is the visualization collection which represents $V$, and {\tt f2}
  represents $U$. Without loss of generality, assume the attributes we want to
  match on ($A_1,...,A_j$) are the first $j$ attributes of $\calR$.
  Query $q$ given by Table~\ref{tab:proof-vzdist} produces
  visualization collection {\tt f5} which expresses $\vzdist_{F(D),A_1,...,A_j}(V,U)$.
  In the table, we first retrieve the values for $(A_1,...,A_j)$ using {\tt f3} and reorder
  {\tt f2} based on these values to get {\tt f4}.
  %We are guaranteed by the
  %operator definition in Section~\ref{sec:viz-exp-oper} that there will be
  %exactly one visualization in both {\tt f1} and {\tt f2} $\forall a_1,...a_j
  %\in (A_1,...,A_j)$
  We then compare the visualizations in {\tt f3}
  and {\tt f4} with respect to $(A_1,...,A_j)$ using the distance function
  $F(D)$ and retrieve the increasingly sorted $(A_1,...,A_j)$ values from the
  $argmin$. We are guaranteed that visualizations in {\tt f3} and {\tt f4}
  match up perfectly with respect to $(A_1,...,A_j)$ since the definition in
  Section~\ref{sec:viz-exp-oper} allows exactly one \vzchn to result from any
  $\vzsel_{A_1=a_1 \land ... \land A_j=a_j}$. Finally, we
  reorder {\tt f1} according to these values to retrieve {\tt f5}.
  For more information on the {\tt .order} operation, please refer to
  Appendix~\ref{name-appex}.
  %Similar queries exist
  %We can imagine coming up with similar
  %Note that a
  %key assumption we make multiple times is that $\vzsel_{A_1,...,A_j}$ for 
\end{proof}

\begin{lemma}
  $\vzfind_{F(D)}(V, U)$ is expressible in \zq for all valid functionals $F$ of
  $D$ and \vzgrps $V$ and singleton \vzgrps $U$.
\end{lemma}
\begin{proof}
  \begin{table*}[!htbp]
    \centering\scriptsize
    {\tt   
      \begin{tabular}{|r|l|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z1} & \textbf{...}
        & \textbf{Zk} & \textbf{Process} \\ \hline
        f1 & - & - & - & ... & - & \\
        f2 & - & - & - & ... & - & \\
        f3 \IN f1 & x1 \IN \_ & y1 \IN \_ & v1 \IN $A_1$.\_ & ... & vk \IN $A_k$.\_
        &
        x2, y2, u1, ..., uk \IN $argmin_{x1,y1,v1,...,vk}[k=\infty] {F(D)}(f3,f2)$
        \\
        *f4 & x2 & y2 & u1 & ... & uk & \\
        \hline
      \end{tabular}
    }
    \vspace{-10pt}\caption{\zq query $q$ which expresses $\vzfind_{F(D)}(V, U)$.
    }
    \label{tab:proof-vzfind}
  \vspace{-10pt}\end{table*}

  Assume {\tt f1} is the visualization collection which represents $V$ and {\tt f2}
  represents $U$.  Query $q$ given by Table~\ref{tab:proof-vzfind} produces
  visualization collection {\tt f4} which expresses $\vzfind_{F(D)}(V, U)$.
\end{proof}

Although we have come up with a formalized algebra to measure the
expressiveness of \zq, \zq is actually more expressive than \vzalg. For
example, \zq allows the user to nest multiple levels of iteration in the
Process column as in Table~\ref{tab:outlier}.
Nevertheless,
\vzalg serves as a useful minimum metric for determining the expressiveness of
\vzlans.
Other visual analytics tools like Tableau are capable of expressing the
selection operator $\vzsel$ in \vzalg, but they are incapable of expressing
the other operators which compare and filter visualizations based on
functional primitives $T$ and $D$. 
General purpose programming
languages with analytics libraries such as Python and
Scikit-learn~\cite{pedregosa2011scikit} are \vzcmp since they are
Turing-complete, but \zq's declarative syntax strikes a novel balance between
simplicity and expressiveness which allows even non-programmers to become data
analysts as we see in Section~\ref{sec:userstudy}.


%\alkim{Should we mention much of \vzalg ggplot/tableau can express here or
%later in related work?} 
%\alkim{Do we need to talk about how the logic \vzalg is necessarily more complicated
%than relational algebra (mention Godel logic for min/max?}
