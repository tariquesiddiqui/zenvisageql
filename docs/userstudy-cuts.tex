%!TEX root=zenvisageql.tex

\section{User Study}
\label{subsec:userstudy}
We conducted a mixed methods study~\cite{creswell2007designing} 
in order to assess the efficacy of \zv. 
We wanted to: 
a) examine and compare the usability of the drag and drop 
interface and the custom query builder interface of \zv; 
b) compare the performance and usefulness of \zv 
with existing tools;
c) identify if, when, and how people would use \zv in their workflow;
and d) receive general feedback for future improvements.

\subsection{User Study Methodology}
\paragraph{Participants.} We recruited 12 graduate students 
as participants with varying degrees of expertise in data analytics. 
Table \ref{tab:toolsrep} depicts the participants' experience of participants in different tool categories of tools.
\begin{table}[H]
\vspace{-10pt}
 \centering\scriptsize
  {\tt    
  \begin{tabular}{|l|l|} \hline
      \textbf{Tools} & \textbf{Count} \\ \hline
      Excel, Google spreadsheet, Google Charts  & 8 \\ \hline
      Tableau & 4 \\ \hline
      SQL, Databases & 6 \\ \hline
      Matlab,R,Python,Java & 8 \\ \hline
      Data mining tools such as weka, JNP & 2 \\ \hline
      Other tools like D3 & 2 \\ \hline
     \end{tabular}
  }
  \vspace{-10pt}\caption{Participants' prior experience with data analytic tools}
  \label{tab:toolsrep}
\vspace{-10pt}\end{table}

\paragraph{Dataset.} 
To conduct the study, we used a housing dataset from Zillow.com~\cite{housing_data} consisting of housing sales data for different cities, counties, and states from 2004--15, with over 245K rows, and 15 attributes. We selected this dataset for two reasons:
First, housing data is often explored by data analysts using existing \john{visualization tools} \cite{elmqvist2008datameadow}. Second, looking for housing is commonplace for graduate students while in school and upon graduation; the participants could relate to the dataset and understand the usefulness of the tasks in the real world.
  
\paragraph{Comparison Points.} 
There are no tools that offer the same functionalities as
\zv: visual analytics tools do not offer the ability
to search for specific patterns, or issue complex data exploration queries; 
data mining toolkits do not offer the ability to generate visualizations, 
search for visual patterns and are instead tailored for general machine learning and prediction.
Since visual analytics tools are closer in spirit and functionality 
to \zv, we decided to implement a visual analytics tool as our baseline.
Thus, our baseline tool replicated the basic query specification and output visualization
capabilities of existing tools such as Tableau \john{(see Figure \ref{fig:baseline})}.  
We augmented the baseline tool with the ability to specify an arbitrary number of filters, allowing
users to use filters to drill-down on specific visualizations.
This baseline visualization tool was implemented with a styling scheme similar to \zv to control for external factors. 
%As depicted in Figure \ref{fig:baseline}, the baseline allowed users to visualize data by allowing them to specify the x-axis, y-axis, category, and filters. The baseline tool would populate all the visualizations, which fit the user specifications, using an alpha-numeric sort order.
In addition to task-based comparisons with this baseline, we
also explicitly asked participants to compare \zv with existing data mining and visual analytics 
tools that they use in their workflow.
 
\paragraph{Study Protocol.} The user study was 
conducted using a within-subject study design~\cite{bordens2002research}. 
The study consisted of three parts. 
In the first part, participants described 
their previous experience with data analytics and visualization tools. 
In the second part, participants performed visual analytics 
tasks using one tool at a time. 
\techreport{\zv was introduced as Tool A and the baseline tool was introduced as Tool B.}
Before starting the tasks on each of the tools, 
participants completed a 15-minute tutorial 
which included an overview of the interface and two practice questions 
to get a general understanding of the tool. 
The order of the tools was randomized to reduce the order effects. 
In order to compare the effectiveness of the drag and drop interface 
and the custom query builder interface of \zv, 
participants performed two separate sets of tasks on \zv, one for each component. Finally, in the third part, participants completed a survey that measured 
their satisfaction levels and preferences, followed by open-ended questions on the strengths and weaknesses 
of the tools, and whether they would use the two tools in their workflow. 
\techreport{
The study lasted for 1 hour and 15 minutes on average. 
Participants were paid 10 dollars per hour for their participation.}

\paragraph{Tasks.}
We prepared three sets of different tasks for the three interfaces (baseline, drag and drop, and custom query builder interfaces). The tasks differed only in the selection attributes so that the performance of the interfaces could be measured objectively (Section~\ref{sec:frontend} lists a few example queries that were used in the study). We carefully selected tasks which can surface during a search for a house. To demonstrate the efficacy of \zv, the tasks were focused on identifying similar trends or anomalies in housing data, which are non-trivial tasks to complete with a large dataset with existing tools. Specifically, the tasks were targeted to test the core features of \zv and solicit responses from participants on how it can complement existing data analytics workflows.

%The tasks that the participants performed on the two tools were designed to be similar in nature (differing only in selection attributes) so that we could compare the performance more objectively. The tasks focused on identifying visualizations with specific insights. Section~\ref{sec:frontend} lists a few example queries that were used in the study. 

\paragraph{Metrics.} 
We manually recorded how participants used the tool, their answers for each task, tracked their browser activity using a screen capture software, collected system interaction logs, recorded audio, and asked the participants to fill in a survey form. Using this data, we collected the following metrics: task completion time, task accuracy, the number of visualizations browsed for each task, the number of edits made on the drawing panel and the query table, survey results, and the answers to open-ended questions.

\paragraph{Ground Truth.} 
We took help of two expert data analysts to prepare the ground truth consisting of ranked answers for each of the tasks. Their inter-rater agreement, measured using Kendall\textquotesingle s Tau rank correlation coefficient, was 0.854. 
% The experts further discussed and resolved the conflicting answers to produce a final single ranked list of answers for each task. 
Then, each expert independently rated the answers on their ranked list on a 0 to 5 scale, with a score of 5 for the highest ranked answer and a score of 0 for the lowest ranked answer. We took the average of the two scores to rate the participants\textquotesingle~answers.


\begin{figure}
\vspace{-10pt}
\begin{center}
\fbox{\includegraphics[width=0.6\columnwidth]{figs/baseline.png}}
\end{center}
\vspace{-20pt}
\caption{The baseline interface implemented for the user study.}
\label{fig:baseline}
\vspace{-15pt}
\end{figure}


\subsection{Key Findings}
Now, we describe the key findings that we discovered through our analysis on the metrics collected during the study. We use $\mu$, $\sigma$, $\chi^2$ to denote average, standard deviation, and Chi-square test scores respectively.

\begin{table}[H]
\vspace{-10pt}
 \centering\scriptsize
  {\tt    
  \begin{tabular}{ | l | l | l  | } \hline
      \textbf{Tool} & \textbf{Time} (\textbf{sd}) & \textbf{Accuracy} (\textbf{sd}) \\ \hline
      Baseline & 172.5s (50.5) & 69.9\% (13.3) \\ \hline
      Drag and drop & 74s (15.1) & 85.3\% (7.61) \\ \hline
      Custom query builder & 115s (51.6) & 96.3\% (5.82) \\ \hline
     \end{tabular}
  }
  \vspace{-10pt}\caption{Average task completion time and average accuracy of the three interfaces.}
  \label{tab:taskresults}
\vspace{-10pt}\end{table}

\paraem{Finding 1: \zv enables over 50\% and 100\% faster task completion time than the baseline for custom query builder and drag and drop interface respectively.} We timed the task completion time for all participants,
and we found that the drag and drop interface has the smallest, and most predictable completion time (Table \ref{tab:taskresults}). The custom query builder has a higher completion time, but also with higher variance, while the baseline had a significantly higher completion time, more than twice the completion time of the drag and drop interface, and almost 50\% more than the custom query builder (Table \ref{tab:taskresults}).
This is not surprising, given that the baseline tool requires more manual exploration of the results, as opposed to either of the other two interfaces. We tested for statistical significance on the difference between the three interfaces using 
one-way between-subject ANOVA, followed by a post-hoc Tukey's test~\cite{tukey1949comparing}.
We found that the drag and drop interface and the custom query builder interface had statistically significantly 
faster task completion times compared to the baseline interface, with $p$ values of 0.0010 and 0.0069 respectively, both $<0.01$, while the difference between drag and drop and custom query builder was not statistically significant,
with a $p$ value of $0.06$. \papertext{Detailed statistics can be found in the technical report~\cite{techreport}.}

% One-way between subject ANOVA~\cite{tukey1949comparing} on the completion time of the tasks revealed that the differences between the three interfaces were significant F(2, 33) = 16.02, p < 0.001. On further applying the post hoc Tukey's test\cite{tukey1949comparing} for comparing the pairwise task completion times among the three interfaces (Table \ref{tab:turkeytest}), we found that both the drag and drop interface and the custom query builder had significantly faster task completion times compared to the baseline tool (p < 0.01), whereas the difference between the drag and drop interface, and the custom query builder was not significantly different.  
\techreport{
\begin{table}[H]
 \centering \scriptsize
  {\tt    
  \scalebox{0.75}{
  \begin{tabular}{|l|l|l|l|} \hline
      \textbf{Treatments} & \textbf{Q statistic} & \textbf{p-value} & \textbf{inference} \\ \hline
	Drag and drop interface vs. Custom query builder & 3.3463 & 0.0605331 & insignificant \\ \hline
	Drag and drop interface vs. Baseline tool & 7.9701 & 0.0010053 & significant (p<0.01) \\ \hline
	Custom query builder vs. Baseline tool & 4.6238 & 0.0069276 & significant (p<0.01) \\ \hline    
     \end{tabular}
  }
  }
\vspace{-10pt}
\caption{Tukey's test on task completion time}
\label{tab:turkeytest}
\vspace{-10pt}
\end{table}
}

\begin{figure}
\begin{center}
\includegraphics[width=0.6\columnwidth]{figs/accuracyvstime.pdf}
\end{center}
\vspace{-25pt}
\caption{Accuracy over time results for zenvisage and baseline}
\label{fig:accuracyvstime}
\vspace{-20pt}
\end{figure}

\paraem{Finding 2: \zv helps retrieve 20\% and 35\% more accurate results than the baseline for drag and drop and custom query builder interface respectively.} 
We rated the participant's answers based on the ground truth answers. 
The ratings revealed that participants performed poorly on the baseline tool (Table \ref{tab:taskresults})
\techreport{Further, any tasks that required comparing multiple
pairs of visualizations or trends ended up having lower accuracy than those that looked
for a pattern, such as a peak or an increasing trend, in a single visualization.}
Overall the low accuracy can be attributed to the
fact that the participants got tired of browsing through
large collections of visualizations and return the first result 
that approximately matches the conditions. 
On the other hand, since \zv is able to
accept more fine-grained user input, and
rank output visualizations, 
assisting participants in retrieving accurate answers
with less effort.
Among the two interfaces in \zv,
while the drag and drop interface had a faster task completion time, it was less accurate. The custom query builder had the highest accuracy (Table \ref{tab:taskresults}).
Thus accuracy is greater than the baseline by over 20\% (for the drag and drop interface)
or over 35\% (for the custom builder interface).
Finally, looking at both accuracy and task completion times together in Figure~\ref{fig:accuracyvstime}, we can conclude that \zv \emph{helps in ``fast-forwarding to desired visualizations'' with high accuracy.}

% In the baseline, comparing multiple pairs of visualizations or trends revealed to be more difficult than looking for a visual property, such as a peak, or an increasing pattern, in a single visualization. on the other hand, since \zv returns a ranked list of filtered answers based on the user specification, which allowed the participants to browse through a smaller number of visualizations compared to the baseline tool. Among the two interfaces in \zv, participants completed the tasks faster ($\mu=74s$, $\sigma=15.1$) on the drag and drop interface, but at the price of accuracy ($\mu=85.3\%$, $\sigma=7.61$). Participants using the custom query builder had the highest accuracy ($\mu=96.3\%$, $\sigma=5.82$) while maintaining comparable speeds to the drag and drop interface ($\mu=115s$, $\sigma=51.6$). Finally, looking at both accuracy and task completion times together (as depicted in Figure~\ref{fig:accuracyvstime}), we can conclude that \zv \textbf{helps in fast-forwarding to desired visualizations with a high accuracy.}

% Figure \ref{fig:accuracyvstime} depicts the average accuracy over time results of each of the participants on each of the interfaces. \agp{move this later.} 



%\textbf{\zv helps retrieve accurate results}: We rated the participant's answers based on the expert's gold set answers. The ratings revealed that participants performed poorly with the baseline, with the lowest accuracy ($\mu=69.9\%$, $\sigma=13.3$) and longest task completion time. In the baseline, comparing multiple pairs of trends or graphs revealed to be more difficult than looking for a single trend such as a peak, or an increasing pattern. \zv, on other hand, returns a ranked list of results based on the user specification, which allowed the participants to browse through a smaller number of visualizations compared to the baseline tool. Among the two interfaces in \zv, participants completed the tasks fastest ($\mu=74s$, $\sigma=15.1$) on the drag and drop interface, but at the price of accuracy ($\mu=85.3\%$, $\sigma=7.61$). Participants using the custom query builder had the highest accuracy ($\mu=96.3\%$, $\sigma=5.82$) while maintaining comparable speeds to the drag and drop interface ($\mu=115s$, $\sigma=51.6$). \textbf{\zv, thus, helps in fast-forwarding to desired visualizations with a high accuracy}.


\paraem{Finding 3: The drag and drop interface, and the custom query builder complement each other.} 
The large majority of participants (8/12) 
mentioned that the drag and drop interface, 
and the custom query builder complement each other in 
making \zv powerful in searching for specific insights. 
About 25 percent (3/12) of the participants 
(all with backgrounds in machine learning and data mining) 
preferred using only the custom query builder while 
just one participant 
(with no prior experience with query languages or programming languages) 
found only the drag and drop interface as useful. 
Participants stated that they would use the drag and drop interface for exploration and simple pattern search, and the custom query builder for specific and complex queries. One participant stated: \textit{``When I know what I am looking for is something exact, I want to use the query table. The other one [drag and drop interface] is for exploration when you are not so sure.''} 
\techreport{We next discuss the participants\textquotesingle responses to the individual components of \zv.}

\paraem{Finding 4: The drag and drop interface is intuitive.} 
Participants liked the simplicity and intuitiveness of the drag and drop interface. 
One stated: \textit{``[The drawing feature] is very easy and intuitive, I can draw the trend and automatically get the results.''} 
The participants rated the usability of this feature on 
a 5-point Likert scale ranging from strongly disagree 
(1) to strongly agree (5), and 
found the drawing feature easy to use, with $\mu=4.45$, $\sigma=0.674$. 
Despite the positive feedback, participants did identify limitations,
specifically that the functionality is restricted to identifying trends
similar to a single hand-drawn trend.

\paraem{Finding 5: The custom query builder is useful for complex queries.}
The majority of the participants (11/12) 
said that the custom query builder 
allowed them to search for complex insights which might be 
difficult to answer in the drag and drop interface. 
When asked if the custom query builder is comparatively difficult
to the drag-and-drop interface, participants tended to agree,
with $\mu=3.73$, $\sigma=0.866$ on a 5-point scale (5 is strongly agree).
At the same time, participants acknowledged 
the flexibility and the efficacy supported by the custom query builder. 
One participant explicitly stated that they 
preferred the custom query builder over the drag and drop interface 
for this reason: 
\textit{``When the query is very complicated, [the drag and drop interface] has its limitations, so I think I will use the query table most of the times unless I want to get a brief impression.''} 
Another participant stated that the custom query builder \textit{``has more functionality and it lets me express what I want to do clearly more than the sketch''}. This is consistent with other responses. 
Participants with a background in machine learning, 
data mining, and statistics (3/12) preferred always 
using the custom query builder over the drag and drop interface.

\paraem{Finding 6: \zv is much more effective (4.27 vs. 2.67 on a 5 point scale)
than the baseline tool at visual data exploration}
 In response to survey question \emph{``I found the tool to be effective in visualizing the data I want to see''}, the participants rated \zv (drag and drop interface + custom query builder) much higher ($\mu=4.27$, $\sigma=0.452$) than the baseline tool ($\mu=2.67$, $\sigma=0.890$) on a 5 point Likert scale.
 A participant experienced in Tableau commented:\textit{``In Tableau, there is no pattern searching. For example, if I see some pattern in Tableau, such as a decreasing pattern, and I want to see if any other variable is decreasing in that month, I have to go one by one to find this trend. But here I can find this through the query table.''}.  

\tar{Point 7 was earlier part of point 6, made it a separate point for emphasis}
\paraem{Finding 7: \zv would be a valuable addition to the participants' analytics workflow, can lead to a quicker initial data exploration before performing complex anaysis using machine learning and imperative programing languages:}
When asked about using the two tools in their current workflow, 9/12 participants stated that they would use \zv in their workflow, whereas only 2/12 participants stated that they would use our baseline tool ($\chi^2=8.22$, p<0.01). When the participants were asked how they would use \zv in their workflow, most of the them mentioned that they would use this tool for tasks such as finding common patterns, gaining an initial understanding of data distribution before running a machine learning task and finding outliers during data cleaning. One participant provided a specific scenario: \textit{``If I am doing my social science study and I want to see some specific behavior among users, then I can use tool A [\zv] since I can find the trend I am looking for and easily see what users fit into the pattern.''}. \nnew{While the pariticpants who had no programming experience considered the insight specification functionality of \zv as something very useful as they had to do it manually before, those with experince in programming languages such as python and MATLAB said \zv would take less time and fewer number of lines of code to do basic data exploration tasks. On asking participants (2/12) who had considerable experince with machine learning and data-mining libraries to write code for a task similar to the one in Table 11 in their favorite language, it took them several minutes and many more operations as compared to a few lines of \zq. \papertext{Example code can be found in our
technical report~\cite{techreport}}}. 

\techreport{
\begin{verbatim}[language=MATLAB]
% x1 = year, x2 = quarter, x3 = month, 
% y1 = sold price, y2 = listing price,
% y3 = turnover ratio
% X = [x1 x2 x3]
% Y_CA = [x1_y1_CA x1_y2_CA ... x3_y3_CA];
% Y_NY = [x1_y1_NY x1_y2_NY ... x3_y3_NY];

% similarity matrix (nX by nY matrix)
% nX = # of x (in this case, 3), 
% nY = # of y (in this case, 3)
sim = zeros(nX,nY);
for i = 1 : nX
    for j = 1 : nY
        val_CA = Y_CA(nX*(i-1)+j);
	val_NY = Y_NY(nX*(i-1)+j);
	sim(nX,nY) = pdist([val_CA,val_NY],'euclidean');
    end
end

% Find max entry M and its index 
% I in the sim matrix
[M,I] = max(sim);   

% Plot line chart (I_x and I_y 
% are the indices from the previous step)
plot(X(I_x), Y_CA(I_y), X(I_x), Y_NY(I_y));
\end{verbatim}
}

% \textbf{\zv is novel with real world applications}: All the participants described \zv as a novel tool and mentioned that they have not seen the declarative pattern searching functionality in any other tool.

\paraem{Finding 8: \zv can be improved.} While the participants looked forward to using the custom query builder in their own workflow, a few of them were interested in directly exposing the commonly-used trends/patterns such as outliers, through the drag and drop interface. Some of them were interested in knowing how they could integrate custom functional primitives (we could not cover it in the tutorial due to time constraints). In order to improve the user experience, participants suggested adding instructions and guidance for new users as part of the interface. Participants also commented on the unrefined look and feel of the tool, as well as the lack of the diverse set of usability related features, such as bookmarking and search history, that are offered in existing systems.




