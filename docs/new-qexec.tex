%!TEX root=zenvisageql.tex

\vspace{-4pt}
\section{Query Execution}
\label{subsec:query_execution}
\vspace{-2pt}

In \zv, \zq queries are automatically parsed and executed by the back-end.
The \zq compiler translates \zq queries into a combination of \sql
queries to fetch the visualization collections and processing tasks to
operate on them.
We present a basic graph-based translation for \zq
 and then provide 
several optimizations to the graph which reduce the
overall runtime considerably.
%
%retrieve data and a series of processing tasks which performs
%that are issued to a relational database and performs
%the processing computation on the results to arrive at the final results.
%By translating to \sql, we do not tie ourselves down to any specific
%  relational database and can seamlessly leverage benefits from improvements to
%databases.
\vspace{4pt}
\subsection{Basic Translation}
% \alkim{If a specification is invalid, skip}
% \alkim{We translate into each \zq query into a graph-based query plan}
Every valid \zq query can be transformed into a query plan 
in the form of a directed
acyclic graph (DAG). 
The DAG contains \cnode[s] (or \emph{collection nodes})
to represent the collections of visualizations in the \zq query and \pnode[s]
(or \emph{process nodes}) to represent the optimizations (or
\emph{processes}) in the \zqproc column. 
Directed edges are drawn between nodes
that have a dependency relationship. 
Using this query plan,
the \zq engine can determine at each step which visualization collection to
fetch from the database or which process to execute.
The full steps to build a query plan for any \zq query is as follows: 
\begin{inparaenum}[(i)]
\item
  Create a \cnode[] or \emph{collection node} for every collection of
  visualizations (including singleton collections).
\item
  Create a \pnode[] or \emph{processor node} for every optimization (or
  \emph{process}) in the \zqproc column.
\item
  For each \cnode[], if any of its \axvar[s] are derived as a result of a
  process, connect a directed edge from the corresponding \pnode[].
\item
  For each \pnode[], connect a directed edge from the \cnode[] of each
  collection which appears in the process.
\end{inparaenum}
\later{
\begingroup
\plitemsep=.2em
\pltopsep=.2em
\begin{compactenum}
\item
  Create a \cnode[] or \emph{collection node} for every collection of
  visualizations (including singleton collections).
\item
  Create a \pnode[] or \emph{processor node} for every optimization (or
  \emph{process}) in the \zqproc column.
\item
  For each \cnode[], if any of its \axvar[s] are derived as a result of a
  process, connect a directed edge from the corresponding \pnode[].
\item
  For each \pnode[], connect a directed edge from the \cnode[] of each
  collection which appears in the process.
\end{compactenum}
\endgroup
}
%These steps split a \zq query into its collection components (\cnode[s]) and
%its process components (\pnode[s]), and the directed edges between the nodes
%represent the dependencies between these components.
%
Following these steps, we can translate our realistic query example in
Table~\ref{tab:real2} to its query plan presented in
Figure~\ref{fig:real2-plan}. The \cnode[s] are annotated with {\tt f\#},
and the \pnode[s] are annotated with {\tt p\#} (the $i$th \pnode[] refers to the
process in the $i$th row of the table). Here, {\tt f1} is a root node
with no dependencies since it does not depend on any process, whereas {\tt f5}
depends on the results of both {\tt p3} and {\tt p4} and have edges coming from
both of them.
\begin{figure}
\vspace{-5pt}
  \centering
  \includegraphics[width=.6\linewidth]{figs/real2-plan}
  \caption{The query plan for the query presented in Table~\ref{tab:real2}.}
  \label{fig:real2-plan}
  \vspace{-15pt}
\end{figure}
Once the query plan has been constructed, 
the \zq engine can execute it using the simple algorithm presented in
in Algorithm~\ref{simplealg:basic}.

  \vspace{-0.75em}
\begin{framed}
  \vspace{-1.5em}
\begin{simplealg}
\small
  %\begin{simplealg}
%\begingroup
%\plitemsep=.2em
%\pltopsep=.2em
Algorithm to execute \zq query plan:
\begin{compactenum}
\item
  Search for a node with either no parents or one whose parents have all been
  marked as done.
\item 
  Run the corresponding task for that node and mark the node as done.
\item
  Repeat steps 1 and 2 until all nodes have been marked as done.
\end{compactenum}
%\endgroup
%\caption{Basic algorithm to execute a \zq query based on its query plan.}
%\end{simplealg}
\label{simplealg:basic}
\end{simplealg}
  \vspace{-1.5em}
\end{framed}
  \vspace{-1em}
For \cnode[s], the corresponding task is to retrieve the data for visualization
collection, while for \pnode[s], the corresponding task is to execute the
process.

\paragraph{\cnode[] translation:}
At a high level, for \cnode[s], the appropriate \sql group-by queries
are issued to the database to compose the data for multiple visualizations
at once. 
Specifically,
for the simplest setting where there are no collections specified
for X or Y, a \sql query in the form of:
\begin{quoting}\small
\begin{verbatim}
      SELECT X, A(Y), Z, Z2, ...  WHERE C(X, Y, Z, Z2, ...)
        GROUP BY X, Z, Z2, ...  ORDER BY X, Z, Z2, ...
\end{verbatim}
\end{quoting}
is issued to the database, where {\tt X} is the X column attribute, {\tt Y} is
the Y column attribute, {\tt A(Y)} is the aggregation function on Y (specified
in the Viz column), {\tt Z, Z2, ...} are the attributes/dimensions we are
iterating over in the Z columns, 
while {\tt C(X, Y, Z, Z2, ...)} refers to any additional constraints
specified in the {\tt Z} columns. The {\tt ORDER BY} is inserted to ensure
that all rows corresponding to a visualization are grouped together, in order. 
As an example, the \sql
query for the \cnode[] for {\tt f1} in Table~\ref{tab:outlier} would have the form:
\begin{quoting}\small
\begin{verbatim}
      SELECT year, SUM(sales), product
        GROUP BY year, product  ORDER BY year, product
\end{verbatim}
\end{quoting}
If a collection is specified for the y-axis, each attribute in the collection
is appended to the {\tt SELECT} clause. If a collection is specified for the
x-axis, a separate query must be issued for every X attribute in the collection.
The results of the \sql query are then packed into a $m$-dimensional array
(each dimension in the array corresponding to a dimension in the collection)
and labeled with its {\tt f\#} tag. 
%\alkim{Do I need to explain the process of
%going from the resultset to the $m$-dimensional array more?}

\paragraph{\pnode[] translation:} 
At a high level, for \pnode[s], depending on the structure
of the expression within the process, 
the appropriate pseudocode is generated
to operate on the visualizations.
To illustrate, say our process is trying to find the
top-10 values for which a trend is maximized/minimized 
with respect to various
dimensions (using $T$), and the process has the form:
\vspace{-5pt}
\begin{equation}
  \langle argopt \rangle_{v0}[k=k']
  \; \bigg[ \langle op_1 \rangle_{v1}
    \bigg[
      \langle op_2 \rangle_{v2}
      \cdots
      \bigg[
        \langle op_m \rangle_{vm}
        T(f1)
      \bigg]
    \bigg]
  \bigg]
  \label{eq:trend}
  \vspace{-5pt}
\end{equation}
where $\langle argopt \rangle$ is one of {\tt argmax} or {\tt argmin}, and
$\langle op \rangle$ refers to one of 
$(\max|\min|\sum|\prod)$. 
Given this, the pseudocode which optimizes this
process can automatically be generated based on the actual values of $\langle
argopt \rangle$, $\langle op \rangle$, and the number of operations. 
In short, for each $\langle op \rangle$ or
dimension traversed over, the \zq engine generates a new nested for loop.
Within each for loop, we iterate over all values of that dimension,
evaluate the inner expression, and then eventually apply the overall 
operation (e.g., max, $\sum$). 
\iftechreportcode
\vspace{10pt}
For Equation~\ref{eq:trend}, 
the generated pseudocode would look like the one given by
Listing~\ref{lst:pseudocode}. Here, {\tt f} refers to the visualization
collection being operated on by the \pnode[], which the parent \cnode[] should
have already retrieved.  
% At the outermost level, instead of returning raw values,
% the generated code returns the top-$k$ dimensional values which most optimize
% the overall expression.

\vspace{-5pt}
\begin{lstlisting}[columns=fullflexible,basicstyle=\ttfamily\scriptsize,
    caption={Pseudocode for a process in the form of
  Equation~\ref{eq:trend}.}, label={lst:pseudocode}, captionpos=b]
  f = make_ndarray(SQL(...))
  tmp0 = make_array(size=len(v0))
  for i0 in [1 .. len(v0)]:
    tmp1 = make_array(size=len(v1))
    for i1 in [1 .. len(v1)]:
      tmp2 = make_array(size=len(v2))
      for i2 in [1 .. len(v2)]:
        ...
          tmpm = make_array(size=len(vn))
          for im in [1 .. len(vn)]:
            tmpm[im] = T(f[0, i1, i2, ..., im])
          tmpm-1[im-1] = opm(tmpm)
        ...
      tmp1[i1] = op1(tmp2)
    tmp0[i0] = op0(tmp1)
  return argopt(tmp0)[:k']
\end{lstlisting}
\vspace{-5pt}

\fi

%\begin{quoting}
%\begin{verbatim}
%f = make_ndarray(SQL(...))
%results = make_array(size=k')
%\end{verbatim}
%\end{quoting}

\techreport{Although this is the translation for one specific type of process, it is easy
to see how the code generation would generalize to other patterns.}

\vspace{-3pt}
\subsection{Optimizations}
We now present several optimizations to the previously introduced basic
translator. 
In preliminary experiments, we found that the \sql queries for the \cnode[s] 
took the majority of the runtime for \zq queries, 
so we concentrate our efforts on
reducing the cost of computing \cnode[s]. 
However, we do present one \pnode[]-based
optimization for process-intensive \zq queries.
We start with the simplest optimization schemes, and add more sophisticated
variations later.
%\alkim{Do we want to use the word I/O-bound?}

\vspace{-4pt}
\subsubsection{Parallelization}\label{sec:para}
\vspace{-3pt}
One natural way to optimize the graph-based query plan is to take advantage of
the multi-query optimization (MQO)~\cite{DBLP:journals/tods/Sellis88} present
in databases and issue in parallel the \sql queries for independent \cnode[s]---the \cnode[s]
for which there is no dependency between them.
With MQO, the database can receive multiple \sql queries at the same time and
share the scans for those queries, thereby reducing the number of
times the data needs to be read from disk.
% Note, however, that naively applying this 
% can lead to performance penalties, as we will discuss
% later on.

To integrate this optimization, we make two simple
modifications to Algorithm~\ref{simplealg:basic}. 
In the first step, instead of searching for a
single node whose parents have all been marked done, search for \emph{all}
nodes whose parents have been marked as done. Then in step 2, issue the
\sql queries for all \cnode[s] which were found in step 1 in parallel at the
same time. For example, the \sql queries for {\tt f1} and {\tt f2}
could be issued at the same time in Figure~\ref{fig:real2-plan}, and once {\tt
p1} and {\tt p2} are executed, \sql queries for {\tt f3} and {\tt f4} can
be issued in parallel.

\vspace{-6pt}
\subsubsection{Speculation}\label{sec:spec}
\vspace{-3pt}

While parallelization gives the \zq engine a substantial increase in
performance, we found that many realistic \zq queries intrinsically 
have a high level
of interdependence between the nodes in their query plans. 
% As a result, the \sql query runtimes
% still dominated the overall runtimes for \zq queries.
To further optimize the performance, we use \emph{speculation}:
the \zq engine pre-emptively issues \sql
queries to retrieve the superset of visualizations for each \cnode[],
considering all possible outcomes for the \axvar[s]. 
Specifically, by tracing the provenance of each \axvar[] 
back to the root, we can determine the superset of all values for each \axvar[];
then, by considering the Cartesian products of these sets, 
we can determine a superset of the relevant visualization collection for a \cnode[].
After the \sql queries have returned, the \zq
engine proceeds through the graph as before, and
once all parent \pnode[s] for a \cnode[] have been
evaluated, the \zq engine isolates the correct subset of data for that
\cnode[] from the pre-fetched data.

% Specifically,
% when composing a visualization collection,
% some columns are fixed by the user, while others
% are the results of processes by way of \axvar[s].
% It is because we do not know the results of these processes that we must wait
% until the corresponding \pnode[s] have been executed.
% However, as mentioned before, 
% a strict rule
% for processes is that the resulting collection of values must be a subset of
% the input collection of values.
% \alkim{Make sure to mention this in limitations.}
% Furthermore, the input collection of values must be either defined directly in
% a visualization collection or be the resulting values of a previous process.
% Therefore, by tracing the provenance of the unresolved \axvar[s] back to the
% root, we can determine the set of all possible values contained by an \axvar[].
% The \zq engine can take the Cartesian product of these sets to determine
% the superset of visualizations for a \cnode[].
For example, in the query in Table~\ref{tab:real2}, {\tt f3} depends on
the results of {\tt p1} and {\tt p2} since it has constraints based on {\tt v2}
and {\tt v4}; specifically {\tt v2} and {\tt v4} should be locations and
categories for which {\tt f1} and {\tt f2} have a negative trend. However, we
note that {\tt v2} and {\tt v4} are derived as a result of {\tt v1} and {\tt
v3}, specified to take on all locations and categories in rows 1 and
2. So, a
superset of {\tt f3}, the set of profit over year
visualizations for various products for all locations and categories 
(as opposed to just those that satisfy {\tt p1} and {\tt p2}), could be
retrieved pre-emptively.
Later, when the \zq engine executes {\tt p1} and {\tt p2}, this superset
can be filtered down correctly.

% \alkim{Note that this requires a change in the \sql code as well, as we have to
%   group the data for the {\tt f3} node by `origin' and `dest' as well, so they
% can be correctly filtered later. (should we add this line?)}
% In practice, this optimization allows the \zq engine to first concurrently
% issue the \sql queries for all \cnode[s] in the query plan, and after executing
% the \pnode[s], correctly filter down the supersets into their corresponding
% collections. 
One downside of speculation is that a lot more data must be
retrieved from the database, but we found that blocking on the retrieval of
data was more expensive in runtime than retrieving extra data. Thus,
speculation ends up being a powerful optimization which compounds the positive
effects of parallelization.
\vspace{-6pt}
\subsubsection{Query Combination}\label{sec:comb}
\vspace{-3pt}
From extensive modeling of relational databases,
we found that the overall runtime of concurrently 
running issuing \sql queries 
is heavily dependent on the number of queries being run in parallel.
Each additional query constituted a $T_q$ increase in
the overall runtime (e.g., for our settings of PostgreSQL, we found $T_q$ =
\texttildelow900ms). 
To reduce the total number of running queries, we use \emph{query combination}; that is,
given two \sql queries $Q_1$ and $Q_2$, we
combine these two queries into a new $Q_3$ 
which returns the data for both
$Q_1$ and $Q_2$. 
In general, if we have $Q_1$ (and $Q_2$) in the form of:
\begin{quoting}\small
\begin{verbatim}
SELECT X1, A(Y1), Z1    WHERE C1(X1, Y1, Z1)
       GROUP BY X, Z1    ORDER BY X, Z1
\end{verbatim}
\end{quoting}
we can produce a combined $Q_3$ which has the
form:
\begin{quoting}\small
\begin{verbatim}
SELECT X1, A(Y1), Z1, C1, X2, A(Y2), Z2, C2
    WHERE C1 or C2
    GROUP BY X1, Z1, C1, X2, Z2, C2
    ORDER BY X1, Z1, C1, X2, Z2, C2
\end{verbatim}
\end{quoting}
where {\tt C1 = C1(X1, Y1, Z1)} and {\tt C2} is defined similarly.
From the combined query $Q_3$, it is possible to regenerate the data which would
have been retrieved using queries $Q_1$ and $Q_2$ by aggregating over the
non-related groups for each query. For $Q_1$, we would select the data for which
{\tt C1} holds, and for each {\tt (X1, Z1)} pair, we would aggregate over the
{\tt X2}, {\tt Z2}, and {\tt C2} groups.
% \alkim{Btw, if we want to do AVG, we also need to return COUNTs, but this is a
% super minor detail. Probably can omit?}

While query combining is an effective optimization, there are limitations. We found
that the overall runtime also depends on the number of unique group-by values
per query, and the number of unique group-by values for a combined query is the
product of the number of unique group-by values of the constituent queries.
Thus, the number of average group-by values per query grows super-linearly with
respect to the number of combinations. However, we found that as long as the
combined query had less than $M_G$ unique group-by values, it was more
advantageous to combine than not (e.g., for our settings of PostgreSQL, we
found $M_G$ = 100k).

\paragraph{Formulation.} Given the above findings, we can now formulate the problem of deciding which
queries to combine as an optimization problem:
{\em Find the best combination of \sql queries that 
minimizes: $\alpha \times$(total number
of combined queries) + $\sum_i$ (number of unique group-by values in combined query $i$),
such that no single combination has more than $M_G$ unique group-by
values.} 

\papertext{
As we show in the technical report~\cite{techreport}, this optimization
problem is {\sc NP-Hard} via a reduction from the {\sc Partition Problem}.
}
%An interesting detail is that the choice of the queries to be combined is
%significant. Since the overall runtime depends on the average number of unique
%group-by values across the queries, it
%is more advantageous to combine queries with smaller numbers of unique group-by
%values.
\input{nphardnessproof}


\paragraph{Wrinkle and Solution.} However, a wrinkle to the above formulation is that it assumes no two \sql
queries share a group-by attribute.
If two queries have a shared group-by attribute, it may be
more beneficial to combine those two, since the number of group-by values does
not increase upon combining them. 
Overall, we developed the metric $EFGV$
or the effective increase in the number of group-by values to determine the
utility of combining query $Q'$ to query $Q$:
$EFGV_{Q}(Q') = \prod_{g \in G(Q')} \#(g)^{[[g \notin G(Q)]]}$
where $G(Q)$ is the set of group-by values in $Q$, $\#(g)$ calculates the
number of unique group-by values in $g$,  and $[[g \notin G(Q)]]$
returns 1 if $g \notin G(Q)$ and 0 otherwise. In other words, this calculates
the product of group-by values of the attributes which are in $Q'$ but not in $Q$.
Using the $EFGV$ metric, we then apply a variant of agglomerative
clustering~\cite{anderberg2014cluster} to decide the best choice of
queries to combine. 
As we show in the experiments section, this technique 
leads to very good performance. 

% \alkim{Do we want to say the full algorithm is omitted due to lack of space but
% present in the tech report here?}

\vspace{-4pt}
\subsubsection{Cache-Aware Execution}\label{sec:cache}
\vspace{-2pt}
Although the previous optimizations were all I/O-based optimizations for \zq,
there are cases in which optimizing the execution of \pnode[s] is important as
well. In particular, when a process has multiple nested for loops, the cost of
the \pnode[] may start to dominate the overall runtime. To address this
problem, we adapt techniques developed in high-performance computing---specifically,
cache-based optimizations similar to those used in matrix
multiplication~\cite{goto2008anatomy}. With cache-aware execution, the \zq
engine partitions the iterated values in the for loops into blocks of data
which fit into the L3 cache. Then, the \zq engine reorders the order of
iteration in the for loops to maximize the time that each block of data remains
in the L3 cache. This allows the system to reduce the amount of data transfer between
the cache and main memory, minimizing the time taken by the
\pnode[s].
%This allows the system to minimize the amount of data the cache needs to eject and thus the amount of data that needs to be copied
% from main memory to the cache, minimizing the time taken by the \pnode[s].
% This is also akin to the techniques for speeding up the nested-loops join algorithm.

%\alkim{Do we like ``{Block-Based Iteartion}'' better for the title?}
