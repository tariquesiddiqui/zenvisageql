import matplotlib.pyplot as plt

title1="Total time"
# x0=[1000,5000,10000,50000,100000]   
# x11=[322,359,319,2769,9207]
# x12=[279, 229, 268,385,931]
# x13=[401,443, 664,3231,12149]

x0=[1000,10000,50000,100000]   
x11=[322,319,2769,9207]
x12=[279, 268,385,931]
x13=[401,664,3231,12149]

title2="Computation Time"
# x0=[1000,5000,10000,50000,10000]   
# x21=40,106,89,2329,8868
# x22=51,35,45,103,165
# x23=42,126,187,2718,11479

x0=[1000,10000,50000,10000]   
x21=40,89,2329,8868
x22=51,45,103,165
x23=42,187,2718,11479

title3="Query Execution Time"
# x0=[1000,5000,10000,50000,10000]   
# x31=[280,253,230,440,339]
# x32=[220,194,223,282,766]
# x33=[360,317,478,513,671]

x0=[1000,10000,50000,10000]   
x31=[280,230,440,339]
x32=[220,223,282,766]
x33=[360,478,513,671]
                  
                  
index = [0,1,2,3]

figure=plt.figure()
plt.subplot(131)
plt.xticks(index, x0)
#plt.xticks(rotation=)
plt.xticks(fontsize=4, rotation=-8)
plt.plot(index, x11, color='green', label='Representative',  lw=1.5, marker='o',mec='black', markevery=1,ms=10)
plt.plot(index, x12, color='red', label='Similarity',  lw=1.5, marker='x',mec='black', markevery=1,ms=10)
plt.plot(index, x13, color='blue', label='Outlier',  lw=1.5, marker='s',mec='black', markevery=1,ms=10)
plt.xlabel('No .of Groups',fontsize=6)
plt.ylabel('time(ms)',fontsize=7)
plt.xticks(fontsize=6)  
plt.yticks(fontsize=7) 
plt.subplot(131).yaxis.labelpad = 0

plt.title(title1,y=0.8,fontsize=6)
plt.annotate('(a)', (0,0), (35, -25), xycoords='axes fraction', textcoords='offset points', va='top')


plt.subplot(132)
plt.xticks(index, x0)
#plt.xticks(rotation=)
plt.xticks(fontsize=4, rotation=-8)
plt.plot(index, x21, color='green', label='Representative',  lw=1.5, marker='o',mec='black', markevery=1,ms=2)
plt.plot(index, x22, color='red', label='Similarity',  lw=1.5, marker='x',mec='black', markevery=1,ms=2)
plt.plot(index, x23, color='blue', label='Outlier',  lw=1.5, marker='s',mec='black', markevery=1,ms=2)
plt.xlabel('No .of Groups',fontsize=6)
plt.ylabel('time(ms)',fontsize=7)
plt.xticks(fontsize=6)  
plt.yticks(fontsize=7) 
plt.subplot(132).yaxis.labelpad = 0


plt.title(title2,y=0.8,fontsize=6)
plt.annotate('(b)', (0,0), (35, -25), xycoords='axes fraction', textcoords='offset points', va='top')


plt.subplot(133)
plt.xticks(index, x0)
#plt.xticks(rotation=)
plt.xticks(fontsize=4, rotation=-8)
Representative,=plt.plot(index, x31, color='green', label='Representative',  lw=.5, marker='o',mec='black', markevery=2,ms=5)
Similarity,=plt.plot(index, x32, color='red', label='Similarity',  lw=.5, marker='x',mec='black', markevery=2,ms=5)
Outlier,=plt.plot(index, x33, color='blue', label='Outlier',  lw=.5, marker='s',mec='black', markevery=2,ms=5)
plt.xlabel('No .of Groups',fontsize=6)
plt.ylabel('time(ms)',fontsize=7)
plt.xticks(fontsize=6)  
plt.yticks(fontsize=7) 
#plt.yaxis.labelpad = 0
plt.subplot(133).yaxis.labelpad = 0


plt.title(title3,y=0.8,fontsize=6)
plt.annotate('(c)', (0,0), (35, -25), xycoords='axes fraction', textcoords='offset points', va='top')

plt.figlegend((Representative,Similarity,Outlier), ('Representative','Similarity','Outlier'),'upper left',prop={'size':6})

    
# plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
# plt.tight_layout()
fig = plt.gcf()
fig.subplots_adjust(hspace=.25, wspace=0.5)
fig.set_facecolor("white")
fig.set_size_inches(5,2)
plt.savefig("profiling.pdf",bbox_inches='tight')



plt.show()


