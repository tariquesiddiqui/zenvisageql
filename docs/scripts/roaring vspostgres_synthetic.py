import matplotlib.pyplot as plt
import numpy as np

title1="selectivity=100%"
x0=[20,100,10000,50000,100000]  
x11=[406,374,892,1357,1775]
x12=[232,626,1244,2407,3051]

title2="selectivity=10%"
x21=[115,126,151,258,282]
x22=[24,22,98,102,133]
                  
index = [0,1,2,3,4]

figure=plt.figure()
plt.subplot(131)
plt.xticks(index, x0)
#plt.xticks(rotation=)
plt.xticks(fontsize=4, rotation=-8)
plt.plot(index, x11, color='green', label='Representative',  lw=.5, marker='o',mec='black', markevery=2,ms=5)
plt.plot(index, x12, color='red', label='Similarity',  lw=.5, marker='x',mec='black', markevery=2,ms=5)
plt.xlabel('No .of Groups',fontsize=6)
plt.ylabel('time(ms)',fontsize=7)
plt.xticks(fontsize=6)  
plt.yticks(fontsize=7) 

plt.subplot(131).yaxis.labelpad = 0
plt.title(title1,y=0.8,fontsize=6)
plt.annotate('(a)', (0,0), (35, -24),fontsize=8, xycoords='axes fraction', textcoords='offset points', va='top')
#plt.legend(loc=0,prop={'size':8})
#plt.ylim(0,1);


plt.subplot(132)
plt.xticks(index, x0)
#plt.xticks(rotation=)
plt.xticks(fontsize=4, rotation=-8)
Postgresql,=plt.plot(index, x21, color='green', label='Representative',  lw=.5, marker='o',mec='black', markevery=2,ms=5)
Roaring,=plt.plot(index, x22, color='red', label='Similarity',  lw=.5, marker='x',mec='black', markevery=2,ms=5)
plt.xlabel('No .of Groups',fontsize=6)
plt.ylabel('time(ms)',fontsize=7)
plt.xticks(fontsize=6)  
plt.yticks(fontsize=7) 
plt.subplot(132).yaxis.labelpad = 0
plt.title(title1,y=0.8,fontsize=6)
plt.annotate('(b)', (0,0), (35, -24),fontsize=8, xycoords='axes fraction', textcoords='offset points', va='top')



ax=plt.subplot(133)
N = 2
ind = np.arange(N)  # the x locations for the groups
width = 0.27       # the width of the bars
roaring = [964,85]
rects1 = ax.bar(ind, roaring, width, color='g',align='center')
postgres= [730,124]
rects2 = ax.bar(ind+width,postgres, width, color='r',align='center')
ax.set_ylabel('time(ms)',fontsize=5)
ax.yaxis.labelpad = 0
ax.set_xticks(ind+width)
ax.set_xticklabels( ('100% select.', '10% select.') ,fontsize=8)
plt.annotate('Census data', (0,0), (38, -20), fontsize=8,xycoords='axes fraction', textcoords='offset points', va='top')
plt.annotate('(c)', (0,0), (20, -20), fontsize=8,xycoords='axes fraction', textcoords='offset points', va='top')
plt.xticks(fontsize=8)  
plt.yticks(fontsize=6) 



ax.legend( (rects1[0], rects2[0]), ('roaring','postgresql'),'upper right',prop={'size':6} )

plt.figlegend((Postgresql,Roaring), ('Postgresq','Roaring'),'upper left',prop={'size':6})

# plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
# plt.tight_layout()
fig = plt.gcf()
fig.subplots_adjust(hspace=.25, wspace=0.4)
fig.set_facecolor("white")
fig.set_size_inches(6,2)
plt.savefig("roaringvspostgres.pdf",bbox_inches='tight')



plt.show()


