import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(111)
# the width of the bars

xnaive=[103.3333333,90,91.66666667,60,66.66666667,60,68.33333333, 83.33333333,80,63.33333333,55,73.33333333]
xcql=[263.3333333,125,133.3333333,83.33333333,101.6666667,86.66666667,111.6666667,116.6666667,88.33333333,63.33333333,81.66666667,133.3333333]
xbaseline=[165,255,127.5,172.5,157.5,97.5,240,135,157.5,127.5,240,195]
ynaive=[94.44444444,94.4,91.06666667,91.06666667,91.06666667, 77.73333333, 84.4,82.13333333, 79.93333333,84.4,68.8,84.4]
ycql=[99.2,79.2,99.2,99.2,94.2, 99.2,99.2,99.2,99.2,99.2,94.2,94.2]
ybaseline=[86.63333333,49.3,83.26666667,66.6,53.3,76.6,76.6,53.3,69.9,66.6,89.9,66.6]




naive=plt.scatter(xnaive,ynaive,color='g')
cql=plt.scatter(xcql,ycql,color='b')
baseline=plt.scatter(xbaseline,ybaseline,color='r')

ax.set_xlabel('time (ms)',fontsize=10)
ax.set_ylabel('accuracy%',fontsize=10)
ax.yaxis.labelpad = 0
plt.xticks(fontsize=10)  
plt.yticks(fontsize=10)
plt.ylim(0,100)
ax.legend((naive,cql,baseline), ('zenvisage drag and drop interface','zenvisage custom query builder', 'Baseline'), loc = "lower right",prop={'size':8} )
fig = plt.gcf()
fig.subplots_adjust(hspace=.25, wspace=0.3)
fig.set_facecolor("white")
fig.set_size_inches(4,2)
plt.savefig("../figs/accuracyvstime.pdf",bbox_inches='tight')
plt.show()
