%!TEX root=zenvisageql.tex
\vspace{-6pt}
\section{Query Language}
\label{sec:querylanguage}
\vspace{-2pt}
\zv's query language, \zq, provides users with a powerful mechanism to operate 
on collections of visualizations. 
In fact, \zq treats visualizations as a {\em first-class citizen}, 
enabling users to operate at a high level on collections of visualizations
much like one would operate on relational data with \sql.
For example, a user may want to filter out all visualizations 
where the visualization shows a roughly decreasing trend from a collection,
or 
a user may want to create a collection of visualizations which are most
similar to a visualization of interest. 
Regardless of the query, \zq
provides an intuitive, yet flexible specification mechanism for
users to express the desired patterns of interest (in other words, their {\em exploration needs})
using a small number
of \zq lines. 
Overall, \zq provides users the ability to compose collections of visualizations,
filter them, and sort and compare them in various ways. 

\zq draws heavy inspiration from the
Query by Example (QBE) language~\cite{qbe} and uses a similar table-based
specification interface. 
Although \zq components are not fundamentally
tied to the tabular interface, we found that our end-users felt
more at home with it; many of them are
non-programmers who are used to spreadsheet tools like Microsoft
Excel.
Users may either directly write \zq, or they may use the \zv front-end, which
supports interactions that are transformed internally into \zq.



We now provide a formal introduction to \zq in the rest of this section. We introduce
many sample queries to make it easy to follow along, and we use a relatable
fictitious product sales-based dataset throughout this paper in our
query examples---we will reveal attributes of
this dataset as we go along.


\vspace{-4pt}
\subsection{Formalization}
\label{subsec:formalization}
\vspace{-3pt}
For describing \zq, we assume that we are operating on a single relation
or a star schema where the attributes are unique
(barring key-foreign key joins), allowing \zq to seamlessly support
  natural joins.
In general, \zq could be applied to arbitrary collections
of relations by letting the user precede an attribute $A$
with the relation name $R$, e.g., $R.A$.
For ease of exposition, we focus on the single relation case.

\vspace{-2pt}
\subsubsection{Overview}
\vspace{-3pt}
\begin{figure}
\vspace{-10pt}
  \centering
  \begin{tikzpicture}[scale=0.6]
    \begin{axis}[
        %cycle list name = color list,
        %axis lines=left,
       % xlabel=Year,
        ylabel=Sales (million \$),
        ylabel near ticks,
        yscale=0.3,
        ybar,
        /pgf/number format/1000 sep={},
        xtick pos =left,
        ytick pos=left
      ]
      \addplot
        coordinates {
          (2012, 50)
          (2013, 33)
          (2014, 40)
          (2015, 50)
          (2016, 70)
        };
    \end{axis}
  \end{tikzpicture}
  \vspace{-12pt}
  \caption{Sales over year visualization for the product chair.}
  \vspace{-18pt}
  \label{fig:sample-viz}
\end{figure}

\paragraph{The concept of visualizations.} We start by defining the notion of a visualization.
We use a sample visualization in Figure~\ref{fig:sample-viz} to guide our discussion.
Of course, different visual
analysis tasks may require different types of visualizations (instead of bar charts, 
we may want scatter plots or trend lines), but across all types,
a visualization is defined by the following five main components:
\begin{inparaenum}[(i)]
\item the x-axis attribute,
\item the y-axis attribute,
\item the subset of data used,
\item the type of visualization (e.g., bar chart, scatter plot), and
\item the binning and aggregation functions for the x- and y- axes.
\end{inparaenum}


\begin{table}[!]
\vspace{-6pt}
  \centering\scriptsize
  {\tt
    \begin{tabular}{|r|l|l|l|l|}
      \hline
      \zqname & X & Y & Z & Viz \\ \hline
      *f1 & `year' & `sales' & `product'.`chair' & bar.(y=agg(`sum')) \\ \hline
    \end{tabular}
  }
  \vspace{-7pt}
  \caption{Query for the bar chart of sales over year for the
    product chair.}
  \label{tab:overview-ex}
  \vspace{-5pt}
\end{table}

\begin{table}[!]
\vspace{-6pt}
  \centering\scriptsize
  {\tt
    \begin{tabular}{|r|l|l|l|l|}
      \hline
      \zqname & X & Y & Z & Viz \\ \hline
      *f1 & `year' & `sales' & `product'.* & bar.(y=agg(`sum')) \\ \hline
    \end{tabular}
  }
  \vspace{-6pt}
  \caption{Query for the bar chart of sales over year for
  each product.}
  \label{tab:overview-ex2}
  \vspace{-18pt}
\end{table}

\paragraph{Visualization collections in \zq:} \zq has four columns to support the specification of
visualizations that the five aforementioned components map into:
\begin{inparaenum}[(i)]
\item {\em \textbf{X}},
\item {\em \textbf{Y}},
\item {\em \textbf{Z}}, and
\item {\em \textbf{Viz}}.
\end{inparaenum}


Table~\ref{tab:overview-ex} gives an example of a valid \zq query that uses
these columns to specify a bar chart visualization of overall sales
over the years for the product chair 
(i.e., the visualization in
Figure~\ref{fig:sample-viz})---ignore the Name column for now. 
The details for each of these columns 
are presented subsequently.
In short, the x-axis (\texttt{X}) is the attribute \texttt{year},
the y-axis (\texttt{Y}) is the attribute \texttt{sales},
and the subset of data (\texttt{Z}) is the product chair,
while the type of visualization is a bar chart (\texttt{bar}),
and the binning and aggregation functions indicate that the y axis is an aggregate (\texttt{agg}) --- the sum of sales.  


In addition to specifying a single visualization, users may often want to
retrieve multiple visualizations. \zq supports this in two ways. Users
may use multiple rows, and specify one visualization per row. The user may also
specify a \emph{collection} of visualizations in a single row by iterating over
a collection of values for one of the X, Y, Z, and Viz columns.
Table~\ref{tab:overview-ex2} gives an example of how one may iterate over all products
(using the notation * to indicate that the attribute product can take
on all possible values),
returning a separate sales bar chart for each product.


\paragraph{High-level structure of \zq.} Starting from these two examples, we can now move onto
the general structure of \zq queries.
Overall, each \zq query consists of multiple rows, where each row operates on collections of visualizations.
Each row contains three sets of columns, as depicted in Table~\ref{tab:zq-breakdown}:
(i) the first column corresponds to an identifier for the visualization collection,
(ii) the second set of columns defines the visualization collection,
while (iii) the last column corresponds to some operation on 
the visualization collection.
All columns can be left empty if needed
(in such cases, to save space, for convenience, we do not display these columns in our paper). 
For example, the last column may be empty if no operation is to be 
performed, like it was in Table~\ref{tab:overview-ex} and~\ref{tab:overview-ex2}.
We have already discussed (ii); now we will briefly discuss (i) and (iii), corresponding
to {\em \textbf{\zqname}} and {\em \textbf{\zqproc}} respectively.

\paragraph{Identifiers and operations in \zq.} 
The {\em \textbf{\zqproc}} column allows the user to operate on the defined collections of
visualizations, applying high-level filtering, sorting, and comparison. 
The {\em \textbf{\zqname}} column provides a way to label and combine
specified
collections of
visualizations, so users may refer to them in the
\zqproc column. 
Thus, by repeatedly using the X, Y, Z,
and Viz columns to compose visualizations and the \zqproc column to process
those visualizations, the user is able derive the exact set of visualizations
she is looking for.
Note that the result of a \zq query is the \emph{data} used to
generate visualizations. The \zv front-end then uses this data to render the
visualizations for the user to peruse.
\vspace{-2pt}
\begin{table}
\vspace{-15pt}
  \centering\scriptsize
  {\tt
    \begin{tabular}{|r|l|l|l|l|r|}
      \hline
      \zqname & X & Y & Z & Viz & \zqproc \\ \hline
      \vspace{10pt}
      $\overbrace{\textrm{Identifier}}$ & \multicolumn{4}{l|}{$\overbrace{\textrm{Visualization Collection}}$} & $\overbrace{\textrm{Operation}}$ \\
    \end{tabular}
  }
  \vspace{-15pt}
  \caption{\zq query structure.}
  \label{tab:zq-breakdown}
  \vspace{-15pt}
\end{table}

\vspace{-5pt}
\subsubsection{X, Y, and Z}
The X and Y columns specify the attributes used for the x- and y- axes. 
For example, Table~\ref{tab:overview-ex} dictates that the returned
visualization should
have `year' for its x-axis and `sales' for its y-axis.
As mentioned, the user
may also specify a collection of values for the X and Y columns if they wish to
refer to a collection of visualizations in one \zq row. Table~\ref{tab:xy-coll}
refers the collection of both sales-over-years and profit-over-years bar
charts for the chair---the missing values in this query (``...'') are the same as 
Table~\ref{tab:overview-ex}.
As we can see, a collection is constructed using \{\}. If the user wishes to denote
all possible values, the shorthand * symbol may be used, as is shown by
Table~\ref{tab:overview-ex2}. In the case that multiple columns contain
collections, a Cartesian product is performed, and visualizations for every
combination of values is returned. For example, Table~\ref{tab:xy-coll2} would
return the collection of visualizations with specifications:
{\tt \{\mbox{(X: `year', Y: `sales')}, (X: `year', Y: `profit'), (X: `month', Y:
`sales'), (X: `month', Y: `profit')\}}. \techreport{Additionally, \zq allows composing 
multiple attributes in the X and Y columns by supporting Polaris table 
algebra [4] over the operators: +, x, / (Appendix
\ref{querylang-appendix}).} 


\begin{table}[!t]
  \vspace{-8pt}
  \centering\scriptsize
  \begin{adjustbox}{center}
    {\tt
      \begin{tabular}{|r|l|l|l|l|}
        \hline
        \zqname & X & Y & Z & Viz \\ \hline
        ... & ... & \{`sales', `profit'\} & ... & ... \\ \hline
      \end{tabular}
    }
  \end{adjustbox}
  \vspace{-8pt}
  \caption{Query for the sales and profit bar charts
  for the product chair (missing values are the same as that in Table~\ref{tab:overview-ex})}
  \label{tab:xy-coll}
\end{table}

\begin{table}[!t]
\vspace{-6pt}
  \centering\scriptsize
  \begin{adjustbox}{center}
    {\tt
      \begin{tabular}{|r|l|l|l|l|}
        \hline
        \zqname & X & Y & Z & Viz \\ \hline
        ... & \{`year', `month'\} & \{`sales', `profit'\} & ... & ... \\ \hline
      \end{tabular}
    }
  \end{adjustbox}
  \vspace{-8pt}
  \caption{Query for the sales and profit bar charts
    over years and months for chairs (missing values are the same as in Table~\ref{tab:overview-ex}).}
  \label{tab:xy-coll2}
  \vspace{-7pt}
\end{table}

\begin{table}[!t]
  \centering\scriptsize
  \begin{adjustbox}{center}
    {\tt
      \begin{tabular}{|r|l|l|l|l|l|}
        \hline
        \zqname & X & Y & Z & Z2 & Viz \\ \hline
        ... & ... & ... & ... & `location'.`US' & ... \\ \hline
      \end{tabular}
    }
  \end{adjustbox}
  \vspace{-8pt}
  \caption{Query which returns the overall sales bar chart
  for the chairs in US (all missing values are the same as that in Table~\ref{tab:overview-ex}).}
  \label{tab:z-mul-col}
  \vspace{-14pt}
  \end{table}



With the Z column, the user can select which subset of the data they wish to
construct their visualizations from.
\zq uses the
\mbox{\emph{$\langle$attribute$\rangle$.$\langle$attribute-value$\rangle$}}
notation
to denote
the selection of data.
Consequently,
the query in Table~\ref{tab:overview-ex}
declares that
the user wishes to retrieve the sales bar chart only for the
chair product.
\techreport{Note that unlike the X and Y columns, both the attribute and the
attribute value must be specified for the Z column; otherwise, a proper subset
of the data would not be identified.}%
Collections are allowed for both the attribute and the attribute value in the Z
column. Table~\ref{tab:overview-ex2} shows an example of using the * shorthand
to specify a collection of bar charts, one for each product.
A Z column which has a collection over attributes might look like:
\mbox{\tt \{`location', `product'\}.*}
(i.e., a visualization for every product and a visualization for every
location).
In addition, the Z column allows users to specify predicate constraints
using syntax like \mbox{\tt `weight'.[? < 10]}; this
specifies all items whose weight is less than 10 lbs. To evaluate, the ? is
replaced with the attribute and the resulting expression is passed to \sql's
{\tt WHERE} clause.
\techreport{The predicate constraint syntax has an analogous predicate collection syntax,
which creates a collection of the values which satisfy the condition.
\mbox{\tt `weight'.[? < 10]} specifies that the resulting visualizations must
only contains items with less than 10 lbs,  \mbox{\tt `weight'.\{? < 10\}}
creates a collection of values, one for each item which is less than 10 lbs. }%

\zq supports multiple constraints on different attributes through the use of multiple Z
columns. In addition to the basic Z column, the user may choose to add Z2,
Z3, ... columns depending on how many constraints she requires.
Table~\ref{tab:z-mul-col} gives an example of a query which looks at sales
plots for chairs only in the US. Note that Z columns are combined using
conjunctive semantics.
\vspace{-4pt}
\subsubsection{Viz}
  \vspace{-2pt}
The Viz column decides the visualization type, binning, and aggregation
functions for the row.
Elements in this column have the format:
\mbox{\emph{$\langle$type$\rangle$.$\langle$bin+aggr$\rangle$}}. 
All examples so far have been bar charts with no binning and {\tt SUM}
aggregation for the y-axis, but other variants
are supported.
The visualization types are derived from the Grammar of
Graphics~\cite{grammar-of-graphics}
specification language, so all plots from the geometric transformation layer of
\ggplot~\cite{ggplot} (the tool that implements Grammar of Graphics) are supported.
For instance, scatter plots are
requested with {\tt point} and heat maps with {\tt bin2d}.
As for binning, binning based on bin width ({\tt bin}) and number of bins
({\tt nbin}) are supported for numerical attributes---we may want to use 
binning, for example, when we are plotting the total number of products whose
prices lie within 0-10, 10-20, and so on. 

Finally, \zq supports all
the basic \sql aggregation functions such as {\tt AVG}, {\tt COUNT}, and {\tt
MAX}. Table~\ref{tab:viz} is an example of a query which uses a different
visualization type, heat map, and creates 20 bins for both x- and y- axes.

\begin{table}[!t]
  \centering\scriptsize
  \begin{adjustbox}{center}
    {\tt
      \begin{tabular}{|r|l|l|l|}
        \hline
        \zqname & X & Y & Viz \\ \hline
        *f1 & `weight' & `sales' & bin2d.(x=nbin(20), y=nbin(20)) \\ \hline
      \end{tabular}
    }
  \end{adjustbox}
  \vspace{-8pt}
  \caption{Query which returns the heat map of sales vs.~weights across all transactions.}
  \label{tab:viz}
    \vspace{-10pt}
\end{table}

\begin{table}[!t]
  \centering\scriptsize
  \begin{adjustbox}{center}
    {\tt
      \begin{tabular}{|r|l|l|l|}
        \hline
        \zqname & X & Y & Z \\ \hline
        f1 & `year' & `sales' & `product'.`chair' \\ \hline
        f2 & `year' & `profit' & `location'.`US' \\ \hline
        *f3 \IN f1 + f2 & & & `weight'.[? < 10] \\ \hline
      \end{tabular}
    }
  \end{adjustbox}
  \vspace{-8pt}
  \caption{Query which returns the sales for chairs or profits for US
    visualizations for all items less than 10 lbs.}
  \label{tab:name-plus}
      \vspace{-15pt}
\end{table}


\techreport{Like the earlier columns, the Viz column also allows collections of
values. Similar to the Z column, collections may be specified
for both the visualization type or the binning and aggregation. If the user wants to
view the same data binned at different granularities, she might specify a bar
chart with several different bin widths: \mbox{\tt bar.(x=\{bin(1), bin(5),
bin(10)\}, y=agg(`sum'))}. On the other hand, if the user wishes to view the
same data in different visualizations, she might write:
\mbox{\tt \{bar.(y=agg(`sum')), point.()\}}.}


The Viz column allows users powerful control over the structure of
the rendered visualization. However, there has been work from the visualization community
which automatically tries to determine the most appropriate visualization type,
binning, and aggregation for a
dataset based on the x- and y- axis
attributes~\cite{wongvoyager2015,Mackinlay:1986:ADG:22949.22950}. 
Thus, we can frequently leave the Viz
column blank and \zv will use these rules of thumb to automatically decide the appropriate setting for us. With this in
mind, we omit the Viz column from the remaining examples with the assumption
that \zv will determine the ``{best}'' visualization structure for us.

\vspace{-5pt}
\subsubsection{\zqname}
\vspace{-2pt}
Together, the values in the X, Y, Z, and Viz columns of each row specify a
collection of visualizations.  The \zqname column allows us to label these
collections so that they can be referred to be in the \zqproc column. For
example, {\tt f1} is the label or identifier given to the collection of sales bar charts in
Table~\ref{tab:overview-ex2}. The * in front of {\tt f1} signifies that 
the collection is an output collection; that is, \zq should return this
collection of visualizations to the user. 


However, not all rows need to have a * associated with their \zqname identifier.
A user may define intermediate
collections of visualizations if she wishes to further process them in the \zqproc
column before returning the final results. 
In the case of Table~\ref{tab:name-plus},
{\tt f1} and {\tt f2} are examples of intermediate collections. 

Also in Table~\ref{tab:name-plus}, we have an example of how
the \zqname column allows us to perform high-level set-like operations
to combine visualization collections directly.
For example, {\tt f3 \IN f1 + f2} assigns {\tt f3} to the
collection which includes all visualizations in {\tt f1} and {\tt f2} (similar
to set union). 
This can be useful if the user wishes to combine
variations of values without considering the full Cartesian product.
In our example in Table~\ref{tab:name-plus}, the user is able to combine the sales for chairs plots
with the profits for the US plots without also having to consider the sales for the
US plots or the profits for chairs plots; she would have to do so if she had used
the specification: {\tt \mbox{(Y: \{`sales', `profit'\}}, \mbox{Z: \{`product'.`chair',
`location'.`US'\}})}. 

An interesting aspect of Table~\ref{tab:name-plus}
is that the X and Y columns of the third row are devoid of values, and the Z
column refer to the seemingly unrelated weight attribute. The values in
the X, Y, Z, and Viz columns all help to specify a particular collection of
visualizations from a larger collection. 
When this collection is defined via the \zqname column, 
we no longer need to fill in the values for X, Y, Z, or Viz, except to select from
the collection---here, \zq only selects the items which satisfy the
constraint, {\tt weight < 10}.  

\techreport{Other set-like operators include
{\tt f1 - f2} for set minus and {\tt f1 \^{} f2} for intersection.}
 


\begin{table*}[t]
\vspace{-1pt}
  \centering\scriptsize
  \begin{adjustbox}{center}
    {\tt
      \begin{tabular}{|r|l|l|l|l|}
        \hline
        \zqname & X & Y & Z & \zqproc \\ \hline
        f1 & `year' & `profit' & v1 \IN `product'.* & \\ \hline
        %f2 & `year' & y1 \IN \{`sales', `sold'\} & v1 & v2 \IN argmax$_{v1}[k=10] \sum_{y1} D(f1,f2)$ \\ \hline
        f2 & `year' & `sales' & v1 & v2 \IN argmax$_{v1}[k=10] D(f1,f2)$ \\ \hline
        *f3  & `year' & `profit' & v2 & \\ \hline
      \end{tabular}
    }
   \end{adjustbox}
  \vspace{-10pt}  
  \caption{Query which returns the top 10 profit visualizations for products
    which are most different from their sales visualizations.}
  \label{tab:proc1}
\vspace{-5pt}
\end{table*}

\subsubsection{\zqproc}
%\alkim{
%  \begin{itemize}
%    \item
%      Defined iterators on variables, and how labels from Name column iterates
%      through
%    \item
%      The real power of \zq comes not from its ability to specify collections,
%      but to operate on them.
%    \item
%      Allows things like trend-based filtering, similarity search,
%      representative search, outlier search
%    \item
%      To operate on these levels, naturally we need a way to iterate over the
%      collections in some way: in \zq, we use \axvar[cs] and \nmvar[s] for this.
%    \item
%      There's many different ways to iterate over things based on what we want
%      to do exactly, but we opted for dimension-based iteration because we
%      found that's what served our needs for the majority of the cases.
%    \item
%      We also need to specify a way to iterate over collections because there
%      might be multiple dimensions along which they have multiple values.
%    \item
%      Here is how we assign \axvar[Cs] \axvar[] \axvar[c], \axvar[s]
%    \item
%      Now that we have a way to iterate, we define our process primitives (T,D)
%      and our iterators
%    \item
%      Basic ways to check for identity or one-pair, etc
%    \item
%      We found all the tasks by our visual analysts could be summarized as an
%      optimization function
%    \item
%      Give examples of sim, trend, etc.
%    \item
%      We also allow black-box which is how we implement k-mediodes.
%  \end{itemize}
%}
%
The real power of \zq as a query language comes not from its ability to
effortlessly specify collections of visualizations, but rather from its ability
to operate on these collections somewhat declaratively. 
With \zq's processing
capabilities, users can filter visualizations based on trend, search for 
similar-looking visualizations, identify representative visualizations, and
determine outlier visualizations. 
Naturally, to operate on collections, \zq
must have a way to iterate over them; however, 
since different visual analysis tasks
might require different forms of traversals over the collections, 
we expose the iteration
interface to the user.

\paragraph{Iterations over collections.}
Since collections may be composed of varying values from multiple columns,
iterating over the collections is not straight-forward.
Consider
Table~\ref{tab:proc1}---the goal is to return profit
by year visualizations for the top-10 products whose profit by year visualizations 
look the most different
from the sales by year visualizations.
\techreport{This may indicate a product that deserves special attention. }%
While we will describe this query in 
detail below, at a high level the first row assembles
the visualizations for profit over year for all products ({\tt f1}),
the second row assembles the visualizations for sales over year for all products ({\tt f2}), 
followed by operating (via the \zqproc column) on these two collections 
by finding the top-10 products who
sales over year is most different from profit over year,
while the third row displays the profit over year for those top-10 products.
A array-based representation of the visualization collections {\tt f1} and {\tt f2},
would look like the following:
\vspace{-5pt}
\begin{align*}
\scriptsize
  {\tt f1} = 
  \begin{Bmatrix}
    \texttt{X: `year', Y: `profit'} \\
    \begin{array}{l}
      \hline
      \texttt{Z: `product.chair'} \\
      \texttt{Z: `product.table'} \\
      \texttt{Z: `product.stapler'}
    \end{array} \\
    \vdots
  \end{Bmatrix}
  {\tt f2} = 
  \begin{Bmatrix}
    \texttt{X: `year', Y: `sales'}  \\
    \begin{array}{l}
      \hline
      \texttt{Z: `product.chair'} \\
      \texttt{Z: `product.table'} \\
      \texttt{Z: `product.stapler'}
    \end{array} \\
    \vdots 
  \end{Bmatrix}
\end{align*}
We would like to iterate over the products, the Z dimension values, of both {\tt f1}
and {\tt f2} to make our comparisons. Furthermore, we must
iterate over the products in the \emph{same order} for both {\tt f1} and {\tt
f2} to ensure that a product's profit visualization correctly matches with its
sales visualization. 
Using a single index for this would be
complicated and need to take into account the sizes of each of the columns.
\techreport{While there may be other ways to architect this iteration for
a single attribute, it is virtually impossible to do when there are multiple
attributes that are varying. }%
Instead, \zq opts for a more powerful \emph{dimension-based} iteration, which
assigns each column (or dimension) a separate iterator called an
\emph{\axvar[]}.
This dimension-based iteration is a powerful idea that extends to any number of dimensions. 
%In this example, we would like for each product in {\tt f1} to match their respective
%visualizations in {\tt f2}. So, for each value in the Z (product) dimension,
%we would like to
%iterate over the Y dimension in {\tt f2} to compare
%the profit visualization with the sales and sold visualizations.
%With a single
%index, this iteration would be complicated and need to take
%into account the sizes of each of the dimensions. However, this
%dimension-based iteration is a powerful idea which extends to any number of
%dimensions and is formalized in the algebra in Section~\ref{sec:formal-expr}.
%
%Now that we have established the need for dimension-based iteration, we
%introduce the mechanism to label the dimensions: \axvar[s].
%\alkim{Note that only columns which have collections of
%values are counted as dimensions in this scheme.}
%All dimension labels or \emph{\axvar[s]} use the syntax:
As shown in Table~\ref{tab:proc1}, \axvar[s] are defined and assigned
using the syntax:
\mbox{$\langle variable \rangle $\IN$ \langle collection \rangle$};
\axvar[] {\tt v1} is assigned to
the Z dimension of {\tt f1} and iterates over all product values. 
For cases in which multiple collections must traverse
over a dimension in the same order, an \axvar[] must be shared across those
collections for that
dimension;
%
%\axvar[cs] may be shared across collections if we want
%the \zqproc column to iterate over the collections in a consistent manner with
%respect to a dimension;
in Table~\ref{tab:proc1}, {\tt f1} and {\tt f2}
share {\tt v1} for their Z dimension, since we want
to iterate over the products in lockstep.

\paragraph{Operations on collections.}
With the \axvar[s] defined, the user can then formulate the high-level
operations on collections of visualizations as an optimization function which
maximizes/minimizes for their desired pattern. Given that {\tt
argmax$_{x}[k=10] \; g(x)$} returns the top-10 {\tt x} values which maximizes the
function $g(x)$, and $D(x,y)$ returns the ``{distance}'' between {\tt x} and
{\tt y}, now consider the expression in the \zqproc
column for Table~\ref{tab:proc1}.
Colloquially, the expression says to
find the top-10 {\tt v1} values whose $D(f1,f2)$ values are the
largest.
The $f1$ and $f2$ in $D(f1,f2)$ refer to the collections of
visualizations in the first and second row and are bound to the current value
of the iteration for {\tt v1}. In other words, for each product
{\tt v1'} in {\tt v1}, retrieve the
visualizations {\tt f1[z: v1']} from collection {\tt f1} and {\tt f2[z: v1']} 
from collection {\tt f2} and calculate the
``{distance}'' between these visualizations; then, retrieve the 10 {\tt
v1'} values for which this distance is the largest---these are the products, and assign {\tt v2} to this
collection. 
Subsequently, we can access this set of products, as we do in the Z column of 
the third line of Table~\ref{tab:proc1}.

\begin{table*}[t]
\vspace{0pt}
  \centering\scriptsize
  \begin{adjustbox}{center}
    {\tt
      \begin{tabular}{|r|l|l|l|l|}
        \hline
        \zqname & X & Y & Z & \zqproc \\ \hline
        f1 & `year' & `sales' & v1 \IN `product'.* & v2 \IN argmax$_{v1}[t<0]
        T(f1)$ \\ \hline
        *f2 & `year' & `sales' & v2 &  \\ \hline
      \end{tabular}
    }
  \end{adjustbox}
  \vspace{-8pt}
  \caption{Query which returns the sales visualizations for all products which
  have a negative trend.}
  \label{tab:trend}
\vspace{-15pt}
\end{table*}

\begin{table*}[t]
  \centering\scriptsize
  \begin{adjustbox}{center}
    {\tt
      \begin{tabular}{|r|l|l|l|l|}
        \hline
        \zqname & X & Y & Z & \zqproc \\ \hline
        f1 & `year' & `sales' & `product'.`chair' & \\ \hline
        f2 & `year' & `sales' & v1 \IN `product'.(* -  `chair') &
        v2 \IN argmin$_{v1}[k=10] D(f1,f2)$\\ \hline
        *f3 & `year' & `sales' & v2 & \\ \hline
      \end{tabular}
    }
  \end{adjustbox}
  \vspace{-10pt}
  \caption{Query which returns the sales visualizations for the 10 products
    whose sales visualizations are the most similar to the sales visualization
  for the chair.}
  \label{tab:similarity}
\vspace{-10pt}
\end{table*}

\begin{table*}[t]
  \centering\scriptsize
  \begin{adjustbox}{center}
    {\tt
      \begin{tabular}{|r|l|l|l|l|}
        \hline
        \zqname & X & Y & Z & \zqproc \\ \hline
        f1 & `year' & `sales' & v1 \IN `product'.* & \\ \hline
        f2 & `year' & `sales' & v2 \IN `product'.* & v3 \IN argmax$_{v1}[k=10]
        \sum_{v2} D(f1, f2)$ \\ \hline
        *f3 & `year' & `sales' & v3 & \\ \hline
      \end{tabular}
    }
  \end{adjustbox}
  \vspace{-10pt}
  \caption{Query which returns the sales visualizations for the 10 products
  whose sales visualizations are the most different from the others.}
  \label{tab:outlier}
  \vspace{-8pt}
\end{table*}

\paragraph{Formal structure.}
More generally, the basic structure of the \zqproc column is:
{\small
\vspace{-5pt}
\[
  \langle argopt \rangle_{\langle \axvar[a] \rangle}[\langle limiter \rangle] \langle expr \rangle \textrm{ \ \ \  where}
\]
\vspace{-2em}
\begin{align*}
  \langle expr \rangle \; & \rightarrow \; \left(\max | \min | \sum | \prod \right)_{\langle
  \axvar[a] \rangle} \langle expr \rangle \\
  & \rightarrow \; \langle expr \rangle \; (+|-|\times|\div) \; \langle expr \rangle \\
  & \rightarrow \; T(\langle \nmvar[a] \rangle) \\
  & \rightarrow \; D(\langle \nmvar[a] \rangle, \langle \nmvar[a] \rangle) \\
  \langle argopt \rangle \; & \rightarrow \; \left( \text{argmax} | \text{argmin} |
\text{argany} \right) \\
\langle limiter \rangle \; & \rightarrow  \; \left(k=\mathbb{N} \; | \; t>\mathbb{R}
\; | \; p=\mathbb{R} \right)
\end{align*}
}
%
\noindent where $\langle \axvar[a] \rangle$ refers to the \axvar[s], and $\langle \nmvar[a]
\rangle$ refers to collections of visualizations. $\langle argopt \rangle$ may
be one of argmax, argmin, or argany, which returns the values which have the largest, smallest, and
any expressions respectively. The $\langle limiter \rangle$ limits the number of results: 
$k = \mathbb{N}$ returns only the top-$k$ values;
$t>\mathbb{R}$ returns only values who are larger than a threshold value $t$
(may also be smaller, greater than equal, etc.); $p=\mathbb{R}$ returns
the top $p$-percentile values.
$T$ and $D$ are
two simple \emph{functional primitives} supported by \zq that can be applied to
visualizations to find desired patterns:
\begin{denselist}
    \item \ [$T(f) \rightarrow \mathbb{R}$]: $T$ is a function which takes a
    visualization $f$ and returns a real number measuring some visual property of the trend of
    $f$. One such property is ``growth'', which returns a positive number if the overall trend is
    ``{upwards}'' and a negative number otherwise; an
    example implementation might be to measure the slope of a linear fit to the
    given input visualization $f$.
    Other properties may measure the skewness, or the number of peaks, or noisiness
    of a visualization. 
  \item \ [$D(f, f') \rightarrow \mathbb{R}$]: $D$ is a function which takes two
    visualizations $f$ and $f'$ and measures the distance (or dissimilarity)
    between these visualizations. Examples of distance functions may include
    a pointwise distance function like 
    Euclidean distance, Earth Mover's Distance, or the Kullback-Leibler Divergence. 
    The distance $D$ could also be measured using 
    the difference in the number of peaks, or slopes, or some
    other property. 
\end{denselist}
\zq supports many different implementations for these two functional
primitives, and the user is free to choose any one. If
the user does not select one, \zv will
automatically detect the ``{best}'' primitive based on the data
characteristics. Furthermore, if \zq does not have an implementation of the
$T$ or $D$ function that the user is looking for, the user may write and use their own
function.

\paragraph{Concrete examples.}
With just dimension-based iteration, the optimization structure of the \zqproc
column, and the functional primitives $T$ and $D$, we found that we were able
to support the majority of the visual analysis tasks required by our users.
Common patterns include filtering based on overall trend
(Table~\ref{tab:trend}), searching for the most similar visualization
(Table~\ref{tab:similarity}), and determining outlier visualizations
(Table~\ref{tab:outlier}).
\techreport{
Table~\ref{tab:trend} describes a query where in the first row, the variable {\tt v2}
selects all products whose trend is decreasing, and the second row visualizes
these product's sales over year.
Table~\ref{tab:similarity} starts with the visualization 
sales over year for chair in the first row, then in the second row
computes the visualizations of sales over year for all products, and in the process
column computes the similarity with chair, assigning the top 10 to {\tt v2},
and the third row visualizes the sales over year for these products.
Table~\ref{tab:outlier} starts with the visualization collection of sales over year
for all products in the first row, followed by another collection of the same
in the second row, and in the process column computes the sum of pairwise distances,
assigning the 10 products whose visualizations are most distant to others to {\tt v3}, 
after which they are visualized.
}%
Table~\ref{tab:real2} features a realistic query inspired by one of our case
studies. The overall goal of the query is to find the
products which have positive sales and profits trends in locations and
categories which have overall negative trends; the user may want to look at
this set of products to see what makes them so special.
Rows 1 and 2 specify the sales and profit visualizations for all locations and
categories respectively, and the processes for these rows filter down to the
locations and categories which have negative trends. Then rows 3 and 4 specify
the sales and profit visualizations for products in these locations and
categories, and the processes filter the visualizations down to the ones that
have positive trends. Finally, row 5 takes the list of output products from the
processes in rows 3 and 4 and takes the intersection of the two returning the
sales and profits visualizations for these products.




\paragraph{Pluggable functions.}
While the general structure of the \zqproc column does cover the majority of
the use cases requested by our users, users may want to write their own
functions to run in a \zq query. To support this, \zq exposes a {\tt Java}-based API
for users to write their own functions. In fact, we use this interface to
implement the $k$-means algorithm for
\zq.
While the pluggable functions do allow virtually any capabilities to be
implemented, it is preferred that users write their queries using the syntax of
the \zqproc column; pluggable functions are considered black-boxes
and cannot be automatically optimized by the \zq compiler.

% \subsection{Realistic Examples}

% \alkim{
%   Now that we have looked at basic examples, to show the true power and
%   flexibility of \zq, we now show some real-world example queries. These are
%   all queries that visual analysts had to do by hand before.
% }

%\input{real-examples}

\begin{table*}
  \centering\scriptsize
  \begin{adjustbox}{center}
    {\tt
      \begin{tabular}{|r|l|l|l|l|l|l|}
        \hline
        \zqname & X & Y & Z & Z2 & Z3 & \zqproc \\ \hline
        f1 & `year' & `sales' & v1 \IN `location'.* & & & v2 \IN
        argany$_{v1}[t<0] T(f1)$\\ \hline
        f2 & `year' & `profit' & v3 \IN `category'.* & & & v4 \IN
        argany$_{v3}[t<0] T(f2)$\\ \hline
        f3 & `year' & `profit' & v5 \IN `product'.* & `location'.[? IN v2] &
        `category'.[? IN v4] & v6 \IN argany$_{v5}[t>0] T(f3)$ \\ \hline
        f4 & `year' & `sales' & v5 & `location'.[? IN v2] &
        `category'.[? IN v4] & v7 \IN argany$_{v5}[t>0] T(f4)$ \\ \hline
        *f5 & `year' & \{`profit', `sales'\} & v6 \^{} v7 & & & \\ \hline
      \end{tabular}
    }
  \end{adjustbox}
  \vspace{-10pt}
  \caption{Query which returns the profit and sales visualizations for products
    which have positive trends in profit and sales in locations and categories
  which have overall negative trends.}
  \label{tab:real2}
  \vspace{-15pt}
\end{table*}

 \vspace{-2pt}
\subsection{Discussion of Capabilities and Limitations}\label{subsec:cap-lim}
Although \zq can capture 
a wide range of visual exploration queries, it is not
limitless. Here, we give a brief description of what \zq can do.
A more formal quantification can be found in
\papertext{\cite{techreport}}\techreport{Section~\ref{sec:formal-expr}}.

\zq's primary goal is to support queries over visualizations---which are themselves
aggregate group-by queries on data. 
Using these queries, \zq can compose a collection of visualizations, 
filter them in various ways, compare them against benchmarks or against
each other, and sort the results. 
The functions $T$ and $D$, while intuitive, support the ability
to perform a range of computations on visualization collections---for example, 
any filter predicate on a single visualization, checking for a specific
visual property, can be captured under $T$.
\techreport{With the pluggable functions, the ability to perform sophisticated computation
on visualization collections is enhanced even further. }%
Then, via the dimension-based iterators, \zq supports the ability to chain
these queries with each other and compose new visualization collections.
These simple set of operations offer unprecedented power in being able to
sift through visualizations to identify desired trends.

Since \zq already operates one layer above the data---on the visualizations---it does
not support the {\em creation of new derived data}:
that is, \zq does not support the generation of derived attributes or values 
not already present in the data. 
The new data that is generated via \zq is limited to those from binning 
and aggregating via the Viz column.
This limits \zq's ability to perform prediction---since feature engineering is
an essential part of prediction; it also limits \zq's ability to 
compose visualizations on combinations of attributes at a time,
e.g., $\frac{A1}{A2}$ on the X axis. 
Among other drawbacks of \zq: \zq does not support 
\begin{inparaenum}[(i)] 
\item recursion; 
\item any data modification; 
\item non-foreign-key joins nor arbitrary nesting; 
\item dimensionality reduction or other changes to the attributes; 
\item other forms of processing visualization collections not expressible via $T$, $D$ or the
black box;
\techreport{\item multiple-dimensional visualizations;}
\techreport{\item intermediate variable definitions;}
\item merging of visualizations (e.g., by aggregating two visualizations);
and 
\item 
statistical tests.
\end{inparaenum}
