%!TEX root=zenvisageql.tex


\begin{table*}[!htbp]
  \centering\scriptsize
  {\tt
    %\resizebox{\linewidth}{!}{%
      \begin{tabular}{|r|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z} & \textbf{Process}
        \\ \hline
        f1 & `year' & `sales' & v1 \IN `product'.* & \\
        f2 & `year' & `profit' & v1 & v2 \IN $argmax_{v1}[k=10]
        D(f1, f2)$ \\
        *f3 & `year' & `sales' & v2  & \\
        *f4 & `year' & `profit' & v2  & \\ \hline
      \end{tabular}
    %}
  }
  \vspace{-10pt}\caption{A \zq query which returns the set sales over years and the profit
    over years visualizations for the top 10 products for which these two
  visualizations are the most dissimilar.}
  \label{tab:vars-1}

  \centering\scriptsize
  {\tt
    %\resizebox{\linewidth}{!}{%
      \begin{tabular}{|r|l|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z} &
        \textbf{Constraints} & \textbf{Process}
        \\ \hline
        f1 & `year' & `sales' & v1 \IN `product'.* & & v2 \IN $argmax_{v1}[k=10]
        T(f1)$ \\
        *f2 & `year' & `profit' & & product IN (v2.range) & \\ \hline
      \end{tabular}
    %}
  }
  \vspace{-10pt}\caption{A \zq query which plots the profit over years for the top 10
  products with the highest sloping trend lines for sales over the years.}
  \label{tab:vars-2}

  \centering\scriptsize
  {\tt
    %\resizebox{\linewidth}{!}{%
      \begin{tabular}{|r|l|l|l|l|} \hline
        \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z} &
        \textbf{Process}
        \\ \hline
        f1 & x1 \IN C & y1 \IN  M & `product'.`chair' & \\
        f2 & x1 & y1 & `product'.`desk' & x2,y2 \IN $argmax_{x1,y1}[k=10] D(f1,f2)$ \\
        *f3 & x2 & y2 & `product'.`chair' & \\
        *f4 & x2 & y2 & `product'.`desk' & \\ \hline
      \end{tabular}
    %}
  }
  \vspace{-10pt}\caption{A \zq query which finds the x- and y- axes which
  differentiate the chair and the desk most.}
  \label{tab:vars-3}
\vspace{-10pt}\end{table*}

\subsubsection{Sets and Variables}
\label{sec:variables}
As we described previously, sets in \zq must
always be accompanied by a variable which iterates over that set, to 
ensure that \zq traverses over sets of visualizations
in a consistent order when making comparisons. 
Consider Table~\ref{tab:vars-1}, which shows
a query that iterates over
the set of products, and for each, compares the sales over
years with the profits over years. Without a
variable enforcing a consistent iteration order over the set of products,
it is possible that the set of visualizations could be
traversed in unintended ways. For example, the sales vs.~year plot for chairs
from the first set could be compared with the profit vs.~year plot for desks in
the second set. By reusing {\tt
v1} in both the first and second rows, the user can force the \zv back-end to
step through the sets in sync.

\noindent {\bf Operations on Sets.} 
Constant sets in \zq must use \{\} to denote that the enclosed elements are
part of a set. As a special case, the user may also use * to represent the set of
all possible values. The union of
sets can be taken using the  sign {\tt |}, set difference can be taken using the
{\tt $\setminus$}
sign, and the intersection can be taken with {\tt \&}. 
\later{Users must take the
explicit difference between * and a set to get the complement.
Set operators have lower precedence than most other operators, so the use of
parentheses is encouraged:
{\tt `product'.(* $\setminus$ \{`stapler'\})}.
As a matter of convenience, all operands of a set operation
which are not sets are automatically converted into singleton sets which
contain that operand. This allows us to simplify the previous expression a
little
further to {\tt `product'.(* $\setminus$ `stapler')}.
The valid elements of sets are literals, the set expansion of variables ({\tt
.range}), and other valid sets. Variables in their raw form cannot be elements
in a set. The \zq, the only a Cartesian products of sets can be specified is
when a period is used (e.g., {\tt \{`product', 'color'\}.*}).}

\later{Labels for sets maybe used with the {\tt @} notation so that the user does not
need to specify the same set over and over again. For example, if the user has
decided that the measure attributes she is interested in are {\tt \{`profit',
`sales', 'revenue'\}}, she could use {\tt M @ \{`profit', `sales', `revenue'\}}
the first time when she defines the set, and {\tt M} every time afterwards.
Once a label has been set, it cannot be redefined.}

\noindent {\bf Ordering.} 
All sets in \zv are ordered, but when defined using the \{\}, the ordering is
defined arbitrarily. Only after a set has passed through a Process
column's ordering mechanism can the user depend on the ordering of the set.
Ordering mechanisms are discussed further in Section~\ref{sec:process}.
However, even for arbitrarily ordered sets, if a variable iterator is defined
for that set and reused, \zq guarantees at least a \emph{consistent} ordering
of traversal, allowing the sets in
Table~\ref{tab:vars-1} to be traversed in the intended order.
\later{
\alkim{
Variable names must start with a letter and can only contain letters, numbers,
and the underscore for the remaining characters. 
To be used, variables must either be defined in the current or a previous row.
}
}

\noindent {\bf Axis Variables.} 
In \zq, there are two types of variables: \emph{axis variables} and \emph{name
variables}.
Axis variables are the common variables used in any of the columns except the
Name column.  The declaration has the form:
\emph{$\langle$variable~name$\rangle$} \IN \emph{$\langle$set$\rangle$}.
The variable then can be used as an iterator in the Process column or reused in
a different table cell to denote that the set be traversed in the same way for
that cell. It is possible to declare multiple axis variables at once,
as we have seen from the Z and Process columns: (e.g., {\tt z.v \IN *.*}).

Sometimes, it is necessary to retrieve the set that an axis variable iterates
over. The {\tt .range} notation allows the user to expand variables to
their corresponding sets and apply arbitrary \zv set
operations on them. For example, {\tt v4 \IN (v2.range |
v3.range)} binds v4 to the union of the sets iterated by {\tt v2}
and {\tt v3}. 
\later{However, the expanded set for a variable may not be the final
value for any table cell in \zq; it must always be either a literal or a
raw variable.}

Axis
variables can be used freely in any column except the Constraints column. In
the Constraints column, only the expanded set form of a variable may be used.
Table~\ref{tab:vars-2} demonstrates how to use an axis variable in the
Constraints column. In this example, the user is trying to plot the overall
profits over years for the top 10 products which have had the most growth in
sales over the years. The user finds these top 10 products in the first row
and declares the variable {\tt v2} to iterate over that set. Afterwards, she
uses the constraint {\tt product \IN (v2.range)} to get the overall profits
across all 10 of these products in the second row.

\noindent {\bf Name Variables.}
Name variables (e.g., \texttt{f1, f2}) are declared only in the Name column and used only in the
Process column.  
Named variables are iterators
over the set of visualizations represented by the visual component. If the
visual component represents only a single visualization, the name variable is
set to that specific visualization. Name variables are \emph{bound} to the axis
variables present in their row. In Table~\ref{tab:vars-2}, {\tt f1} is bound
{\tt v1}, so if {\tt v1} progresses to the next product in the set {\tt f1}
also progresses to the next visualization in the set. This is what allows the
Process column to iterate over the axis variable ({\tt v1}), and still compare
using the name variable ({\tt f1}).
If multiple axis variables are present in the visual component, the name
variable iterates over set of visualizations produced by the Cartesian product
of the axis variables. 
\techreport{If the axis variables have been declared independently
of each other in the visual component, the ordering of the Cartesian product
  follows the ordered bag semantics described in
  Section~\ref{subsec:ordered-bag} in order of the columns laid out in \zq.
  However, the user may also superscript variables to control the order in
  which Cartesian product is done as well.
  However, if the variables were declared together in the Process 
column as is the case
with {\tt x2} and {\tt
y2} in Table~\ref{tab:vars-3}, the ordering is the same as the set in the
declaration.}
