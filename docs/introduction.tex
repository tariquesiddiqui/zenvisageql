%!TEX root=zenvisageql.tex

\section{Introduction}
\label{sec:intro}
Interactive visualization tools, such as Tableau~\cite{tableau}
and Spotfire~\cite{spotfire}, have paved the way for the 
democratization of data exploration and data science. 
These tools have witnessed an ever-expanding user base---as a concrete example, 
Tableau's revenues last year were in the hundreds of millions of US Dollars and is 
expected to reach tens of billions soon~\cite{tableau-increases}.
Using such tools, or even tools like Microsoft Excel, the standard 
data analysis recipe is as follows:
the data scientists load a dataset into the tool,  
select visualizations to examine, 
study the results, and then repeat the process
until they find ones that match
their desired pattern or need. 
Thus, using this repeated process of manual examination, or {\em trial-and-error},
data scientists are able to formulate and test hypothesis, 
and derive insights. 
The key premise of this work is that 
 to find
desired patterns in datasets, 
{\bf \em manual examination of each visualization in a collection  is 
simply unsustainable}, especially
on large, complex datasets.
Even on moderately sized datasets, a data scientist may need to examine as many as 
tens of thousands of visualizations, all to test a single hypothesis, 
a severe impediment to data exploration.

To illustrate, we describe the challenges of 
several collaborator groups who have been hobbled by the ineffectiveness of current 
data exploration tools: 


\paraem{Case Study 1: Engineering Data Analysis.}
Battery scientists at Carnegie Mellon University
perform visual exploration of datasets of solvent
properties to design better batteries.
A specific task may involve finding solvents
with desired behavior: e.g.,
those whose 
solvation energy
of Li$^+$ vs.~the boiling point is a 
{\em roughly increasing trend}. 
To do this using current tools, 
these scientists manually examine
the plot of Li$^+$ solvation energy vs.~boiling point for each of the thousands of solvents, to find those that match 
the desired pattern of a roughly increasing trend.



\paraem{Case Study 2: Advertising Data Analysis.}
Advertisers at ad analytics
firm Turn, Inc., 
often examine their portfolio of advertisements to see
if their campaigns are performing as expected.
For instance, an advertiser may be interested in seeing if there are any keywords
that are {\em behaving unusually} with respect to other keywords in Asia---for example,
maybe most keywords have a specific trend for 
click-through rates (CTR) over time, 
while a small number of them
have a different trend.
To do this using the current tools available at Turn, 
the advertiser needs to manually examine the plots of 
CTR over time for each 
keyword (thousands of such plots),
and remember what are the typical trends. 

\paraem{Case Study 3: Genomic Data Analysis.}
Clinical researchers at the NIH-funded genomics center
at UIUC and Mayo Clinic 
are interested in studying 
data from clinical trials.
One such task involves finding pairs of genes
that {\em visually explain the differences} 
in clinical trial outcomes (positive vs.~negative)---visualized 
via a scatterplot with the x- and y- axes each referring
to a gene, and each outcome depicted as a point 
in the scatterplot---with the positive outcomes
depicted in one color, and the negative ones as another.
Current tools require the researchers to
generate and manually evaluate tens of thousands of scatter plots of pairs of
genes to determine whether
the outcomes can be clearly
distinguished in the scatter plot. 




\techreport{
\paraem{Case Study 4: Environmental Data Analysis.}
Climate scientists at the National Center for
Supercomputing Applications at Illinois are interested
in studying the nutrient and water property readings on sensors within buoys
at various locations in the Great Lakes. 
Often, they find that a sensor is displaying unusual behavior
for a specific property, and want
to figure out {\em what is different} about this sensor relative to others,
and if other properties for this sensor are {\em showing similar behavior}.
In either case, the scientists would need to separately examine each
property for each sensor (in total 100s of thousands of visualizations)
to identify explanations or similarities.

\paraem{Case Study 5: Server Monitoring Analysis.}
 The server monitoring team at Facebook has noticed a spike in the
per-query response time for Image Search in Russia on August 15, after which the response time flattened out.
The team would like to identify if there are {\em other attributes 
that have a similar behavior} with
per-query response time, which may indicate the reason for the spike and subsequent flattening.
To do this, the server monitoring team generates visualizations for
different metrics as a function of the date, and assess if any of them has similar behavior
to the response time for Image Search. Given that the number of metrics is likely 
in the thousands, this takes a very long time.

\paraem{Case Study 6: Mobile App Analysis.} 
The complaints section of the Google mobile platform team 
have noticed that a certain mobile app has 
received many complaints.
They would like to figure out {\em what is different} about this app relative to others.
To do this, they need to plot various metrics for this app to figure out 
why it is behaving anomalously. 
For instance, they may look at network traffic generated by this app over time,
or at the distribution of energy consumption across different users.
In all of these cases, the team would need to generate several visualizations manually 
and browse through all of them in the hope of finding what could be the issues with the app.
}

\vspace{2pt}
\noindent 
Thus, in these examples, the recurring theme is 
the manual examination of a large number of generated visualizations for a specific visual pattern.
Indeed, we have found that in these scenarios\papertext{, as well as others that we have encountered 
via other collaborators---in {\em climate science, server monitoring, and mobile app analysis}}---data exploration 
can be a tedious and time-consuming process with current
visualization tools.

\paragraph{Key Insight.} The goal of this paper is to develop \zv, a visual analytics
system that can {\bf \em automate the search for desired
visual patterns}.
Our key insight in developing \zv is that the data exploration 
needs in all of these scenarios 
can be captured within a common set of operations
on collections of visualizations.
These operations include:
{\em composing}  
collections of visualizations,
{\em filtering} visualizations
based on some conditions,
{\em comparing} visualizations,
and {\em sorting} them based on some condition.
The conditions include 
similarity or dissimilarity to a specific pattern, 
``typical'' or anomalous behavior,
or the ability to provide 
explanatory or discriminatory power.
These operations and conditions form the kernel of a 
new data exploration language, \zq ("zee-quel"), that forms
the foundation upon which  \zv is built.

\paragraph{Key Challenges.} 
We encountered many challenges in building the \zv visual analytics platform, 
a substantial advancement over manually-intensive visualization
tools like Tableau and Spotfire; these tools
enable the examination of {\em one visualization at a time},
without the ability to automatically identify
relevant visualizations from a collection of visualizations.

First, there were many challenges in developing \zq,
the underlying query language for \zv.
Unlike relational query languages that operate
directly on data, \zq operates on collections of visualizations,
which are themselves aggregate queries on data. 
\techreport{Thus, in a sense \zq is a query language that operates
on other queries as a first class citizen.}
This leads to a number of challenges that are not addressed
in a relational query language context. For example,
we had to develop a natural way to users to specify
a collection of visualizations to operate on, without
having to explicitly list them;
even though the criteria on which the visualizations
were compared varied widely, we had to develop a 
small number of general mechanisms that capture all of these
criteria.
Often, the visualizations that we operated on had
to be modified in various ways---e.g., we
might be interested in visualizing the sales of
a product whose profits have been dropping---composing
these visualizations from existing ones is not straightforward.
Lastly, drilling down into specific visualizations
from a collection also required special care.
Our \zq language is a synthesis of desiderata
after discussions with data scientists
from a variety of domains, and has been under
development for the past two years. 
To further show that \zq is {\em complete} under
a new \vzalg that we develop involved additional challenges.

Second, in terms of front-end development, 
\zv, as an interactive analytics tool,
needs to support the ability for users to 
interactively specify \zq queries---specifically, 
interactive short-cuts for commonly used \zq queries, 
as well as the ability to pose extended \zq queries
for more complex needs.
Identifying common interaction ``idioms'' for these
needs took many months. 

Third,  
an important challenge in building \zv is
the back-end that supports the execution of \zq.
A single \zq query can lead to the generation of 10,000s of 
visualizations---executing each one 
independently as an aggregate query,
would take several hours, rendering the 
tool somewhat useless.
\techreport{(As it turns out, this time would be what an analyst aiming
to discover the same pattern would have to spend
with present visualization tools,
so the naive automation may still help reducing the amount of
manual effort.) }%
\zv's query optimizer 
operates as a wrapper
over any traditional relational database system. 
This query optimizer compiles \zq queries
down to a directed acyclic graph of 
operations on collections of visualizations,
followed with the optimizer
using 
a combination of intelligent speculation and combination, 
to issue queries to the underlying database. 
We also demonstrate that the underlying problem
is {\sc NP-Hard}.
Our query optimizer leads
to substantial improvements over the naive
schemes adopted within relational 
database systems for multi-query optimization.


\techreport{
\paragraph{Related Work.}
There are a number of
tools one could use for interactive analysis;
here, we briefly describe why those tools are inadequate 
for the important need of 
automating the search
for desired visual insights.
We describe related work in detail in Section~\ref{sec:related}.
To start, visualization tools like
Tableau and Spotfire only generate and provide one visualization
at a time, 
while \zv analyzes collections of visualizations at a time,
and identifies relevant ones from that collection---making it
substantially more powerful.
While we do use relational database systems
as a computation layer,
it is 
cumbersome to near-impossible to express
these user needs in \sql. 
As an example, finding visualizations of 
solvents for whom a given property
follows a roughly increasing trend is impossible to write
within native SQL, and would require custom UDFs---these UDFs 
would need to be hand-written for every \zq query.
\techreport{Similarly, finding visualizations of 
keywords where CTR over time in Asia is behaving
unusually with respect to other keywords 
is challenging to write within SQL. }%
For the small space of queries 
where it is possible to write
the queries within \sql 
these queries require non-standard constructs,
and are both complex and cumbersome to write,
even for expert \sql users, 
and are optimized very poorly
(see Section~\ref{sec:related}).
\techreport{It is also much more natural 
for end-users to operate directly on visualizations
than on data. 
Indeed, users who have never programmed or written
SQL before find it easy to understand and write 
a subset of \zq queries, as we
will show subsequently.} 
Statistical, 
data mining, and machine learning
 certainly provide functionality beyond \zv
in supporting prediction
and statistics; 
these functionalities are exposed as ``one-click'' algorithms
that can be applied on data.
However, no functionality is provided for searching for desired
patterns; no querying functionality beyond the one-click algorithms,
and no optimization. To use such tools for \zq, many
lines of code and hand-optimization is needed.
\techreport{As such, these tools are beyond the reach of
novice data scientists who simply want to explore 
and visualize their datasets.}
}%metatechreport

\paragraph{Outline.}
We first describe our
query language for \zv, \zq (Section~\ref{sec:querylanguage})%
\techreport{, and formalize the notion of a {\em \vzalg}, an analog of relational
algebra, describing a core set of capabilities for any 
language that supports visual data exploration and
demonstrate that \zq is {\em complete} in that
it subsumes these capabilities (Section~\ref{sec:formal-expr})}.
We then describe the graph-based query translator and optimizer for \zq
(Section~\ref{subsec:query_execution}).
Next, our initial prototype of \zv is presented (Section~\ref{sec:architecture}).
We also describe our performance experiments
(Section~\ref{sec:experiments}), and
present a user survey and study focused on evaluating the 
effectiveness and usability of \zv (Section~\ref{sec:userstudy}).
\techreport{In the appendix, we present additional details of 
our query language,
along with complete examples, 
and additional details on user study.}%
\papertext{%
Lastly, we describe how \zv differs from related work (Section~\ref{sec:related}).

In our extended technical report~\cite{techreport}, we provide
additional details that we weren't able to fit into the paper.
In particular, we formalize
the notion of a {\em \vzalg}, an analog of relational
algebra, describing a core set of capabilities for any language that supports visual
 data exploration, and demonstrate that \zq is {\em complete} in that
it subsumes these capabilities.
 We also provide additional details of our query language.}
% In the appendix, we present additional details of our query language,
% with complete examples (Appendix~\ref{subsec:examples}) and describe the capabilities of our language in some detail (Appendix~\ref{subsec:expressivenesss}).



