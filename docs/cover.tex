%!TEX root=zenvisageql.tex


\section*{Summary for Revised Paper ``Effortless Data Exploration with zenvisage: An Expressive and Interactive Visual Analytics System''}

We thank the reviewers and meta-reviewer for their insightful 
and very detailed feedback, 
and for giving a chance to address these comments---we have taken this feedback to 
heart in preparing an {\em almost entire
rewrite} of the present paper---{\bf \em each and every section was substantially modified; 
new sophisticated optimized \zq engine,
\fuse,
(using speculation,
parallel, and query combining) along with cache-aware optimization
techniques (and an {\sc NP-Hardness} proof) were developed; 
an extensive performance evaluation on multiple datasets and parameters was added
and a new Upwork user survey was conducted. }
The major changes to each section of the paper include:
\begin{enumerate}
\item {\em A complete rewrite and repositioning of the introduction.}
We rewrote most of the introduction to (i) position our system correctly
as a system that facilitates the search for desired patterns as opposed to a
visualization recommendation system; (ii) describe how \zv solves
some of the articulated problems alluded to in the examples; and (iii) 
describe the challenges in building \zv.

\item {\em A complete rewrite of the query language section.}
To improve readability and make
the paper more self-contained, we rewrote this section to have fewer references
to the technical report. We removed the list of initial examples,
and made this section read less like a user manual, with more intuition 
built in. We also added more details on the task processors (now called
processes),
and clearly defined the interfaces of our exploration functions.
At the same time, we trimmed the description of features that were
not essential to grasp the core contributions of \zq.
% \alkim{Opened up task processors, and made them seem less like a black box,
% both in language section and execution section. Also better defined our
% exploration functions so the interface is more clear}

\item {\em Full-fledged modeling and optimization, 
hardness proof, intelligent optimization schemes, and 
complete rewrite of the query execution section.}
To extend and articulate clearly the novelty of the query optimizer,
we added 
a thorough system modeling and exploration of solution alternatives.
We extended our previous naive graph-based optimization 
technique to a sophisticated technique, called \fuse, reliant on speculation,
caching, and
query combination---a particularly tricky aspect
was that query combination led to non-monotonically increasing gains, so
determining the optimal set of combinations was non-trivial.
%was trading off
%query combination and batching,
%and optimization across task processors (now called processors).
% \alkim{To be fair, our speculation scheme is not really intelligent... we just
% speculation everything. But we do do both batching AND query combining, and the
% decisions for when to combine/batch are based on our complex algorithms that we
% talk aboutl ater.}
We additionally proved that the problem of identifying the optimal set of
combinations 
is {\sc NP-Hard}
and developed an approach to solve the problem. 
In our revision, we now also
spend a lot of room articulating clearly how a \zq query gets
translated, with detailed descriptions of processors and functions $T$ and $D$.
Overall, this section has been entirely rewritten and enhanced.


\item {\em Reimplementation of the \zq Engine.}
  One of the main concerns of the reviewers was the large execution times of \zq
  queries. To address this point, in addition to the optimizations
  mentioned before, we reimplemented the \zq engine to use the much faster
  array-based indexing (as opposed to hash-based indexing) 
  which provided a 50$\times$ speedup in processing time
  (and explained the poor performance earlier). 
  We also showed in the experimental section that if we swap out
  PostgreSQL for a column-store
  database with a large in-memory cache, we can reach interactive response
  times for large datasets.


\item {\em Completely redone performance experiments and rewritten experimental
section.}
We completely redid the performance experiments to better
focus on parameters and aspects that the reviewers suggested.
Specifically, we compared the performance of our query optimizer 
against three algorithms (we had just one baseline in prior work), 
including one which leverages
the standard multi-query optimization within traditional 
database systems---our algorithm performs much better than this baseline.
We compared the algorithms on a range of real-world and 
synthetic datasets, varying parameters,
including the complexity of the queries and the number of
visualizations processed. We also report 
the breakdown of
runtime
%our algorithm
across retrieving visualizations
and processing them. 

\item {\em Entirely new analyst interview section, and 
extensive rewrites to user study section.}
To enable us to judge whether the tasks used in the previous user study
capture the typical tasks used in practice, 
we conducted an entirely new set of user interviews of analysts from Upwork, 
focused on the data exploration tasks from a comprehensive list 
provided by Amar et al.~\cite{amar2005low}. 
Additionally, we rewrote most of the user study section to clearly articulate
the proposed benefits of \zv and focus on 
the utility of \zv and \zq relative to other tools,
as opposed to general usability aspects. 
We specifically revamped and expanded the portion of the study
that compares \zv to other database and data mining tools, along with 
participant quotes and code examples. 


\item {\em Trimmed down system architecture section.}
Since reviewers felt that the paper was busy enough as it is,
so
we have considerably trimmed the system architecture 
%section---especially
%the front-end technologies,
section---only leaving what is absolutely
necessary to support the user study.%, and the experimental sections.

\item {\em Modified related work section.}
We edited this section to emphasize technical differences
as opposed to philosophical differences between \zv and other
systems.
%(these differences were also summarized in the introduction). 
%We briefly also mentioned these differences in the
%introduction.
Also mirroring the positioning of \zv in the introduction,
we focused on comparing with systems that support similar
searches for desired patterns, as opposed to recommendation systems.
For comparison with raw relational databases and \sql,
%Within the related work section, for the relational databases
%comparison,
we additionally added a short experiment to illustrate
how \sql is unsuitable for expressing \zq queries.

\item {\em Removed the \vzalg section.}
Since the \vzalg section could be removed without affecting
the readability or impact of other sections---especially
now that the optimization section has been expanded---we
decided to remove this section entirely, and link to it
via the technical report.


%\tar{Since multiple reviewers were concerned about the timings of task-processors, we may want to highlight we reimplemented the task-processors using arrays and used cache-based optimizations to gain upto 50X speedup in processing, making task-processors 
%take tiny fraction of the overall execution time}
\end{enumerate}
In addition, we have addressed all the questions raised by the reviewers, as detailed below. 
We begin with the meta-review questions and then address the questions raised by individual reviewers. 



\subsection*{Metareview}

\com{1. Rework the structure of the paper so that it is comprehensible, including removing many of the references to the technical report. Remove (at least) the UI description and save it for another paper/demo.}

Based on this comment, we have tried to rewrite most of the paper to make it self-contained.
We removed the entirety of \vzalg to make room for the other content---while interesting,
this didn't take away from and was not a prerequisite for the other contributions of the paper. 
The \vzalg can be found in the technical report.
We have additionally trimmed most of the system architecture and 
front-end description (Section~\ref{sec:architecture})
in order to make more room for the other content. 
We have substantially revised all sections in making it more intuitive and self-contained:
In Section~\ref{sec:querylanguage}, we have modified the description to
read less like a user manual and more like a paper, like the reviewers suggest.
The key concepts are conveyed along with intuition, and the entire description is self-contained.
In Section~\ref{subsec:query_execution}, which has been completely revamped with 
new optimizations, we have spent a lot of time with the basic translation and
each of the optimizations in order to convey the key intuitions---in the past this section
had a lot of references to the technical report even for basic notions.
Even for the other sections, we have tried to opt for having the sections self-contained,
with minimal references to the technical report.

\com{2. Improve the related work comparison}

We have completely revamped the related work comparison, 
focusing primarily on differences
in functionality as opposed to differences in the UI,
with details in the related work section.
We now spend an entire column of text on comparing against visual analytics tools,
relational databases, data mining and statistics tools, and programming libraries.
In addition, we have expanded our description of the user study
to talk about these other tools (in addition to the visual analytics baselines).

In short, for relational databases, we talk about how relational databases
are not able to support certain types of processing;
and even for the small subset of \zq that relational databases can support---we provide
a \sql translation for one such query---it requires complex constructs 
not supported and optimized for in many relational databases.
In addition, we show through one experiment in the related work section that
%We show through one experim
\zq along with our cache-aware \fuse optimization
%Our \zq along with \fuse \tar{and cache-optimized in memory processing}
gives up to 10$\times$ better performance than this \sql query.
%(we've added one experiment in the related work section).
For most other queries, even we, the authors, were unable to write the query.
Most user study participants who knew \sql said that they would not use \sql
for \zq-like queries.

%didn't suggest it as an option
%when asked if they had tools that could replace \zv. 

Then, for data mining and statistics tools, we describe how 
they do not support any expressive querying for patterns or
optimization across such queries. In essence, these tools support built-in machine learning
or statistics algorithms, and only support changes in the parameters used to invoke 
these algorithms. 

Last, we describe how visual analytics tools generate one visualization at a time, 
while \zv is a query language over collections of visualizations. This increased capabilities
also leads to additional challenges in optimization.


\com{3. The user study needs to be at the very least rewritten, and probably redone, to find a good baseline to compare to and to describe what claims it is validating.}
%\agpyell{this isn't great}
To address these concerns, we reanalyzed collected data 
and rewrote large parts of the user study section; and we conducted 
an additional set of 7 analyst interviews recruited via Upwork. 

In particular, Reviewer 1 and 2 raised concerns about the design of the user study, 
and the task selection.
To remedy this, we conducted a survey with active data analysts
to augment the decisions behind our task selection.
We recruited them via Upwork, an online freelancing platform,
and probed them on the types of tasks they perform on a day-to-day basis.
We used a taxonomy of exploration tasks prescribed
by Amar et al.~\cite{amar2005low} to categorize these tasks.
We then cross-referenced this list with the actions completed in the tasks for the user
study to examine the breadth of activities the user study tasks covered.
We demonstrated how the selected user study tasks encompass most of the task
used in data exploration; however, we acknowledge that \zv does not offer
improvements for certain exploration tasks, and we explicitly state this in the
user study section.
%\tar{above paragraph looks much better now}

We then substantially rewrote the user study section to focus primarily on the comparison
with visual analytics tools, and other data analytics tools. 
We have also clarified the claims made by this user study and made the findings and contributions more precise.
%The user survey can be found in Section~\label{sec:interviews},
%while the new user study can be found in Section~\label{sec:userstudy}


\com{4. Improve the description of the novelty of the approach. In particular, the authors must provide a more detailed description of the execution engine that convinces the reviewers that this paper makes sufficient contributions... In particular, ...
(a) the idea of introducing processors and filter visualization is not explored in detail, and no optimizations are provided: these are implemented as black-box functions over visualizations, and no special optimization technique is described; in fact, based on the experiments some of them seem to require quite a long time to be executed;
(b) the translation of zenvisage queries into SQL is based on a shallow form of batching, and again does not look very insightful.}

We would like to emphasize that the primary contribution of the paper is not the query execution engine, but
in fact the \zq query language and the overall vision of the system. 
The auxiliary contributions include the query execution, the \vzalg and completeness proof, and the interface aspects. 
However, we took the reviewer's comments to heart to pursue a ``deep-dive'' exploration of the query execution part,
adding a host of new optimization techniques---employing speculation, query
combination, and cache-aware execution---touching both the optimization of the visualization retrieval as well as the processing of visualizations. 
We prove that the optimization problem for finding the optimal query
combination is {\sc NP-Hard},
and identify an effective solution---called \fuse.
As it turns out, vanilla parallelism, a simple form of multi-query optimization adopted by traditional relational databases
performs much more poorly than \fuse.
%We additionally add two other baselines to demonstrate the benefits of our optimization.

To address the other concerns: 
(a) We now fully explore optimizations that affect both the processing and the
retrieval of visualization data. 
The overall execution of \zq queries is now interactive with moderately sized
%datasetss
%Both the processing and generation is now interactive time on moderately sized
datasets---on large datasets,
we simply have no way of getting interactive time without approximation. In fact,
we show that our optimization schemes now overall lead to runtimes which are
virtually the same as the time it takes to run 
{\em a single group-by query} on the dataset.
This means that the bottleneck is not \zq, but in fact is the basic cost of scanning the entire dataset.
To test this further, we tested our optimized solution on our largest datasets in Vertica, and
we found that the techniques once again led to interactive times.
Thus, the bottleneck is not our code, but is in fact the basic database system bottleneck.
Note that even when the solution is NOT interactive---perhaps because the database is large---it is still
valuable, because it automates a lot of manual work of generating individual visualizations
and perusing them. 
(b) Indeed, in our previous version, the translation was rather shallow. 
In this version, we have shown that the optimal translation of \zq into \sql
%optimally for a simpler setting
%(when all the group-bys are distinct)
is itself {\sc NP-Hard}. We have developed a new
technique, called \fuse which finds a close approximation to the optimal
solution using a metric called $EFGV$.%trading off combining and batching that optimizes a new metric called $EFGV$. 



% \alkim{We do get interactive times when either dataset is small
%   enough or if we use in-memory database as our analytics systems (such as
%   Tableau do). However, there is no way to get interactive times if there is
%   too much data, other than to do sampling/estimation. Also, it should be
%   stressed that even if the system is not interactive, it provides a lot of use
%   because it automates a lot of the work being done manually right now (0 man
% hours).}


\subsection*{Reviewer 1}
\com{5. Organization - the paper tries to squeeze too much content within the page limit of the submission. As a result, many of the sections feel rather superficial and somehow rushed on. ... In this respect, this submission does not look sufficiently self-contained.}

See response to MetaReview point 1. 

\com{6. Contributions - The main intuition is to introduce processors to compare and filter visualizations. This is an interesting idea, and it is indeed an advancement wrt other visual analytics tools. However, this extension is not particularly deep from the technical viewpoint: the authors essentially introduce a number of black-box functions to process visualizations -- but besides this they do not offer significant insights related to the execution of the functions over visualizations. In fact, it can be seen from the experiments that on complex data, processing visualizations in main memory may take a significant time, and it is unclear how this would be compatible with the expectations of users in terms of response times. Other technical contributions in the paper are related to the translation of zenvisage queries into SQL queries, and their execution. These optimizations, however, amount to a rather straigtforward batch execution strategy, and essentially rely on the DBMS to do most of the actual optimization work.}

See response to MetaReview point 4.

\com{7. The language - the language is very expressive ... this raises a concern about the overall usability of the language, in two directions. First, the language itself does not look easy to use. Second, I think it is easy to write zenvisage scripts that take considerable time to be executed.}

%\tar{maybe we should add that we provide more details on the syntax/structure of process column, and participants' could understand it pretty easily. Moreover, we help users with drop-down menus to build the  process column.}

From our user study, we found that not only are users able to learn how to write
effective, correct \zq in a short amount of time (a 15 minute tutorial),
all of them were able to answer the tasks, i.e., formulate the \zq queries, 
within two minutes, and with over 95\% accuracy.
(In contrast, the users ended up having very poor accuracy of 70\% when they used 
a vanilla visualization tool AND took longer.)
This means that users are indeed able to understand and use \zq effectively. 
We have considerably expanded our user study section to clarify such issues.
Regarding the second point --- indeed, it is possible to write \zq scripts
that take a long time to be executed, however, that is a problem with any
declarative query language. That said, one benefit of our new optimization algorithm \fuse 
is that it can analyze a query and provide an estimate of the time, preventing 
users from running queries that take a long while to be executed.
%We have added a note about this to the paper \agp{we should.}



\com{8. Experimental Evaluation - experimental results do not look particularly convincing.... In terms of scalability, the study of the query optimization techniques simply that... batching queries improves performance. With respect to the performance of the task processors, the paper does not really discusses any optimizations ... and, as mentioned above, times may increase significantly when the number of groups increase. The user study also looks somehow flawed: it is completely unclear what is the baseline .... I can see that the problem of identifying the baseline in this case is tricky, since there is no system capable of performing a form of processing of visualizations that is similar to the one of zenvisage, but it is known that user studies, even more than other quantitative studies, are very sensitive to design choices like this.}

Our new experimental results are much more exhaustive and study sophisticated
optimizations 
that go beyond simple batching.
%In particular, we evaluate four algorithms. 
In fact, We provide experimental evidence that our \fuse optimization
does not take much longer than simply running a group-by query on the entire dataset---a powerful result,
indicating that we are able to extract as much mileage as possible from the database.
In addition, please see response to MetaReviewer's point 4.

Regarding the user study, indeed, finding a good baseline for \zv is tricky.
However, we have now revamped the user study to first justify the set of tasks and baselines we compare against
via user survey,
and further presented the comparison with visual analytics tools, followed by other analytics tools.
Our visual analytics tool baseline is modeled in such a way such that it replicates
the look and feel of \zv, while providing functionality similar to the likes of Tableau or Spotfire.
In addition, please see response to MetaReviewer's point 3.

% We have rewritten the user study section to better articulate the results and design decisions behind the user study. Finding a good baseline for \zv is indeed tricky. We now explicitly compare \zv with existing visual analytical tools and analytical tools to demonstrate the benefits of \zv, and reference the taxonomy of low-level analytical tasks by Amar et al. \cite{amar2005low} to clearly describe the tasks where \zv excels over existing tools (identifying correlations, anomalies, and clustering similar attributes). We do acknowledge that \zv does not offer improvements over certain tasks and explicitly state this fact in the user study section.



% \com{
% 1. I believe the paper would greatly benefit from splitting the presentation of the language and the study of its formal properties, from the discussion of the technical aspects related to the architecture and optimizations. Separating the content in two different papers would give proper space to both treatments, and improve the overall readability.}

% \com{
% 2. with respect to the language, I suggest that the authors concentrate on the discussion of how such an expressive and ambitious language can be made practical for users, especially the ones without a strong technical background. It would be interesting, for example, to discuss how the syntax can be restricted to improve the usability, or techniques to estimate the execution time of scripts in advance, in order to warn the users about possible slowdowns.
% }

% \com{
% 3. as far as the technical development is concerned, I think the authors may want to investigate more the implementation of the task processors (as opposed to the SQL optimization algorithm, that looks quite straightforward). In fact, designing optimized implementations of the very expressive processors allowed within the language seems an interesting challenge.
% }


\smallskip
\noindent
We have also addressed the other typographical errors identified by the reviewer.
%\agp{we should...}

\subsection*{Reviewer 2}


\com{9. Introduction and Motivation: The introduction is a bit confusing 
regarding the main contribution of the paper.... I believe that the contribution of this paper 
is the query visualization language that allows the user to declaratively express 
the desired visualizations, and the authors need to clearly say that 
avoiding the visualization recommendation hints.}

We agree. We have eliminated any mention of the visualization recommendation notion,
and focus on the fact that \zq is a language that supports querying over visualizations. 
We have completely rewritten the introduction and the related work sections 
to emphasize these notions. 


\com{10. Contribution and Positioning to Other Works/tools: databases that have query languages, data mining tools, and visual analytics tools. The paper makes comparisons to all three but these comparisons are more at the level of "user-friendliness".  What is needed is also a comparison at the level of expressivity and functionality.
}

See response to MetaReview point 2.


\com{11. Paper structure:
The paper organization is rather unorthodox and confusing. 
(i) Section 2 starts with examples of the Query Language using a table syntax and then explains the table syntax. I think we could skip the examples or move them after explaining the syntax. ... It would be best to focus on a few that ... are also representative of the query capabilities
(ii) Then Section 3 is query execution followed by the system architecture followed by experiments and finally followed by the relational algebra. why not finishing with the query language, then presenting an overview of the system, then explaining the particular components of the system around query execution...? 
}
We agree with the reviewer: we have made significant changes to the organization and presentation.
(i) We have completely rewritten Section~\ref{sec:querylanguage} 
and eliminated the early examples
and focused on the intuition behind the key language functionalities
with a minimal number of examples.
(ii) We have eliminated the algebra from the main body of the paper---the rest of the paper
did not depend on this. Since we significantly shortened the architecture section---focused 
primarily on describing a screenshot---we believe it is
best placed before the user study, where it is most relevant. The query execution section 
is self-contained, and does not require any concepts from the architecture section. 
Also see response to MetaReview point 1 for overall organizational changes. 


\com{12. Query Language:
\begin{inparaenum}[(i)]
\item The paper should add a summary of the capabilities and limitations of the language. 
\item How are joins expressed in ZQL? How are they mapped to SQL? 
\item ZQL queries can be supported in Java through the Java client library. Why would one use ZQL in Java vs any visualization language (e.g. D3) coupled with SQL (assuming that he is already programming in Java?
\item The Constraints and Viz sections are quite short. What type of operations does it support? It supports filtering but does it support any functions over column values, such as taking the average of a column or concatenating strings or adding values, etc, before filtering them? What type of processing on the values can be done before they mapped, what type of functions are supported, e.g. take the log of X values?
\item Please discuss how the (process) functions cover the breadth and diversity of visual analytics tasks. What kind of variants of these functions can the user define?
\item Section 2 provides one view of the language while section 7 describes the relational algebra behind it. First of all, everything about the language should be in one place. Second, section 7 is confusing because ... the two sections seem disconnected to large degree.
\end{inparaenum}
}

We thank the reviewer for the suggestions. 
Overall, we have rewritten most of  
the query language section, taking the reviewer's comments into account.
Specifically, we have eliminated the upfront examples, and focused instead
on the key concepts, and the examples to illustrate them. 
We have tried to be as formal and careful about describing features of the language,
while also providing intuition about the utility.
Specifically, the process column discussion has expanded, 
and we have added a detailed discussion on capabilities and limitations.

We answer point-by-point below:
\begin{inparaenum}[(i)]
\item We agree. We have added a detailed discussion on capabilities and limitations 
in Section~\ref{subsec:cap-lim}.
We have articulated how \zq's primary contribution is as a 
query language over collections of visualizations---composing, filtering, sorting,
and comparing visualizations iteratively.
On the other hand \zq is not appropriate for any sort of data modification
or data transformation, among other functionalities that \sql can support,
such as recursion and nesting.
\item We have added a short line about how joins are supported to the paper.
%\agpyell{Add.}
\item This is already addressed in the related work comparison---see response to MetaReview point 2.
In short, \sql is very cumbersome to near impossible to use, and inefficient for the queries \zq is good at.
The benefit of the {\tt Java} library is that some users may want to develop domain specific 
visualization tools that compile down to \zq queries issued via {\tt Java}. 
\item We have folded the Constraints column into the discussion of the Z column, and we are 
now spending more time clearly explaining the capabilities of these columns.
In short, while the Viz column does support certain fixed types of aggregations
and binning, it doesn't support arbitrary functions.
Further, none of the other columns support data transformations. This is
one of the main limitations of \zq as it stands, and is described in Section~\ref{subsec:cap-lim}.
%\agpyell{alkim check}
\item We have simplified the presentation of the process functions as well, and we
now clarify the various ways the process functions can be used, and
how it can be extended.
%\agpyell{alkim check}
\item We have removed Section 7 (former \vzalg discussion) from the main body of the paper,
reducing this concern. In the technical report, we have tried to be more consistent when
rewriting the section.
\end{inparaenum}


\com{13. Query Execution: The translation is described very briefly making it hard to understand the mechanism ... furthermore, how are the Viz, X, Y parts translated to? Obviously, they are not translated to SQL, are they? Provide an example of a ZQL and its translation ... use this example to illustrate the optimizations of Section 3.2 too. The language supports three simple functions ... How are these functions translated into the executable SQL code?
}

(also raised by Reviewer 3)

We thank the reviewers for the excellent suggestions. 
We agree that our previous query execution section described the translation and optimization strategies
in an excessively terse manner.
To remedy this (and also because we now have a host of additional optimizations),
we have rewritten the query execution and optimization section (Section~\ref{subsec:query_execution}) in its entirety.
For the query execution, we have added 
a clear step-by-step description of the entire translation process. 
This includes the mapping for all of the columns except Process, which are mapped
I%\agpyell{alkim check Viz}
to portions of the \sql queries, and for the processes. We have also considerably
clarified the presentation of the processes in Section~\ref{sec:querylanguage}).
We also made sure that the same examples were used for the optimization
discussion.

%, to this section by using the same example for the optimization discussion, 
%which was covered in our response to MetaReview point 4. 


\com{14. System:
\begin{inparaenum}[(i)]
\item Figures 3 and 4 are almost invisible ... 
\item  The front-end description does not add much... 
\item I would first show the system and then focus on its main components.
\item Section 3 needs to clearly refer to the main system components of Figure 2 (maybe as separate subsections). 
\end{inparaenum}
}
Responses below:
\begin{inparaenum}[(i)]
\item While it is still hard to read the font of the screenshot, 
we have now added a large font overlay --- only this text is meant to be read.
We have eliminated one of the two figures.
\item We have eliminated most of the description of the front-end and only kept the essential parts.
\item We now show the system screenshot as the first step.
\item We have eliminated the system components diagram (Figure 2), and kept
  Section 3 self-contained.
The architecture section is now a short section primarily focused on what is needed to understand
the user study and the experimental evaluation.
\end{inparaenum}

\com{15. Experiments:
I have hard time understanding the effect of optimizations ... 
I think a more comprehensive study would be more helpful and insightful. 
For example, show performance based on the number of visualizations, ....
}

We thank the reviewer for this suggestion --- based on the suggestion 
we have thoroughly revamped the experimental section by conducting performance
evaluation studies on varying all of the parameters the reviewer suggests,
among others. The results can be found in Section~\ref{sec:experiments}.

\com{16. Experiments: For the user study, the authors prepared the tasks ... Were they tasks better suited for ZenVisage? ... It would be fairer ... if the tasks were actually diverse and the study would show where the current system excels and where not. In fact, one could organize two-level study. first, a variety of analytics tasks are chosen. The second level could focus on specific tasks (for which Zenvisage is appropriate) and show how well it performs.
}

We agree with the reviewer's point --- it's not clear if the 
tasks are indeed diverse enough, and we really appreciated
the suggestion of performing a two-stage study 
where we first identify a variety of analytics tasks
and test their relevance, and then focus on specific tasks
for \zv. 
We have done precisely that. 

Please see response to MetaReview point 3. 

% We conducted an additional user interview with data analysts to augment the decisions behind our task selection. We probed the different type of tasks analysts work on during their daily activities, and we reference the taxonomy of low-level analytical tasks described by Amar et al. \cite{amar2005low} to categorize the tasks. We cross-referenced the low-level task list with the actions completed by the user study participants to examine the breath of activities our tasks covered. We rewrote the user study section to more accurately describe where \zv excels over existing tools (identifying correlations, anomalies, and clustering similar attributes) and where \zv does not offer improvements.



%\com{Too much breadth and many dependencies to the technical report. Needs focus.}



\subsection*{Reviewer 3}
\com{17. ...the proposed ZQL language is quite like breaking the visual analytics task step by step and describing each step through a sub-query, which could also be described using SQL sub-query. How much user effort could be reduced by using ZQL? ...
}

See response to MetaReview point 2.


% The reviewer brings up an important question: can \sql replace \zq in functionality? 
% As it turns out, the answer is yes for 
% certain types of \zq queries, but not all queries. 
% We now provide a \sql translation for a \zq query in Table~\agp{xxx} in our
% paper---this translation assumes 
% that the distance function $D$ is Euclidean.
% For any other distance functions, 
% we are not aware of any way of issuing a \sql
% query to replace \zq. 
% Furthermore, there are other types of processors 
% which similarly cannot be replaced
% directly with \sql.
% However, the more problematic aspect is that the \sql query thus written is
% fairly complicated and hard to understand and debug;
% it also necessarily involves some complex \sql constructs,
% that are not supported universally across all database systems,
% and are poorly optimized. 
% Our user study participants, for example, despite knowing and using \sql,
% did not think \sql is an appropriate language to issue these types of requests.
% We have added a detailed description to the related work section to describe
% the comparison of \zq with \sql. 
% We have additionally added experiments that demonstrate that the
% \sql queries of this type take a very long time to be executed (in Section~\agp{xxx})



\com{18. ...the query execution section presents the translation approach and optimization strategies too briefly. More algorithmic details and examples would be good for the readers.}

See response to Reviewer 2 point 13.


\com{...zenvisage aims to provide an interactive visual analytics system, but the overall performance takes tens of seconds as shown in Figure 6, it is a bit too long for interactive analytics. For the user, they only care about the overall performance, I am wondering what the major part of the time consumed in the system.}

We thank the reviewer for the suggestion 
--- we now have a breakdown in the paper for the time consumed by our
optimization algorithms.
Furthermore, we reimplemented the \zq engine to use a much faster array-based
index (as opposed to a hash-based index, which along with our optimizations, provided us with a 50$\times$
speedup in processing.
%each step of \fuse, our new optimization algorithm.
%\tar{we may want to highlight we reimplemented the task-processors using arrays and used cache-based optimizations to gain upto 50X speedup in processing, making task-processors 
%take tiny fraction of the overall execution time}
In short, our algorithm is only non-interactive for practical queries when the time it takes
to run a single group-by query on the dataset (essentially the cost of a single
scan) is non-interactive. 
%Essentially, the time for \fuse is a small multiple of the time to take a run single group-by query.
We describe in detail the changes to the optimization, and the explanation for how we investigated
where the bottlenecks to interactivity are --- along with an additional experiment on Vertica --- in the response to the MetaReview point 4.



% In our previous version, we were using relatively naive optimization schemes, as a result of which the overall
% performance took tens of seconds---after developing additional optimization techniques, capturing 
% combination, speculation, batching, and caching, practical queries on real datasets now take orders of XXX\agp{xxx} seconds. 
% Also to address the issue raised by the reviewer, we have added an experimental breakdown
% in Figure~\ref{xxx}\agp{xxx} that displays where the time is being spent in the system.
% In short, we find that the bulk of the time is spent in the \sql queries that are issued,
% as opposed to the task processing. \agp{xxx}.
% Note however, we are fundamentally limited by the size of the dataset---if a 
% single sequential scan takes \agp{xxx} seconds, then it is impossible for us to 
% achieve better times than that, without adding on additional
% optimizations such as sampling and precomputation. 
% We plan to explore these additional optimizations
% in future work. 
% We have additionally added an experiment to demonstrate that as well---in Figure~\agp{x}, we show
% that our optimizations lead to a moderate complexity \zq query taking not more
% than \agp{xx}x of the time taken for a single group-by query on the entire dataset
% on PostgreSQL.

