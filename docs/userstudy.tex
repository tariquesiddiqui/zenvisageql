%!TEX root=zenvisageql.tex

\section{User Study}
\label{sec:userstudy}
We conducted a mixed methods study~\cite{creswell2007designing} 
in order to assess the efficacy of \zv. 
We wanted to: 
a) examine and compare the usability of the drag and drop 
interface and the custom query builder interface of \zv; 
b) compare the performance and usefulness of \zv 
with existing tools;
\papertext{and}
c) identify if, when, and how people would use \zv in their workflow\techreport{;
and d) receive general feedback for future improvements}.

\subsection{User Study Methodology}
\paragraph{Participants.} We recruited 12 graduate students 
as participants with varying degrees of expertise in data analytics. 
\papertext{Details of their prior experience can be found in our 
technical report~\cite{techreport}.}
\techreport{Table \ref{tab:toolsrep} depicts the participants' experience with different categories of tools.
\begin{table}[H]
\vspace{-10pt}
 \centering\scriptsize
  {\tt    
  \begin{tabular}{|l|l|} \hline
      \textbf{Tools} & \textbf{Count} \\ \hline
      Excel, Google spreadsheet, Google Charts  & 8 \\ \hline
      Tableau & 4 \\ \hline
      SQL, Databases & 6 \\ \hline
      Matlab,R,Python,Java & 8 \\ \hline
      Data mining tools such as weka, JNP & 2 \\ \hline
      Other tools like D3 & 2 \\ \hline
     \end{tabular}
  }
  \vspace{-10pt}\caption{Participants' prior experience with data analytic tools}
  \label{tab:toolsrep}
\vspace{-10pt}\end{table}
}

\paragraph{Dataset.} 
We used a housing dataset from Zillow.com~\cite{housing_data} consisting of
housing sales data for different cities, counties, and states from 2004--15,
with over 245K rows, and 15 attributes.
\papertext{Since looking for housing is commonplace, the participants could relate to the dataset and understand the
usefulness of the tasks.}
\techreport{
We selected this dataset for two
reasons: 
First, housing data is often explored by data analysts using existing
visualization tools~\cite{elmqvist2008datameadow}. Second, looking for housing
is commonplace for graduate students while in school and upon
graduation; the participants could relate to the dataset and understand the
usefulness of the tasks.
}
  
\paragraph{Comparison Points.} 
There are no tools that offer the same functionalities as
\zv. Visual analytics tools do not offer the ability
to search for specific patterns, or issue complex visual exploration queries; 
data mining toolkits do not offer the ability to search for visual patterns 
and are instead tailored for general machine learning and prediction. 
\papertext{We decided to implement our baseline tool by replicating
the basic visualization specification capabilities of tools like Tableau,
augmented with additional filtering capabilities. 
The styling scheme was similar to \zv to control for external factors;
details can be found in \cite{techreport}.}\techreport{Since visual analytics tools are closer in spirit and functionality 
to \zv, we decided to implement a visual analytics tool as our baseline.
Thus, our baseline tool replicated the basic query specification and output visualization
capabilities of existing tools such as Tableau.  
We augmented the baseline tool with the ability to specify an arbitrary number of filters, allowing
users to use filters to drill-down on specific visualizations.
This baseline visualization tool was implemented with a styling scheme similar to \zv to control for external factors. 
As depicted in Figure \ref{fig:baseline}, the baseline allowed users to visualize data by allowing them to specify the x-axis, y-axis, category, and filters. The baseline tool would populate all the visualizations, which fit the user specifications, using an alpha-numeric sort order.} In addition to task-based comparisons with this baseline, we
also explicitly asked participants to compare \zv with existing data mining and visual analytics 
tools that they use in their workflow.
 
\paragraph{Study Protocol.} The user study was 
conducted using a within-subjects study design~\cite{bordens2002research}. 
The study consisted of three phases. 
In the first phase, participants described 
their previous experience with data analytics and visualization tools. 
In the second phase, participants performed visual analytics 
tasks using each of the tools. 
\techreport{\zv was introduced as Tool A and the baseline tool was introduced as Tool B.}
This phase began with a 15-minute tutorial that included an overview of the interface and two practice questions. 
The order of the baseline tool and the \zv interface was randomized to reduce order effects.
To compare the effectiveness of the drag and drop interface 
and the custom query builder interface of \zv, 
participants performed two separate sets of tasks on \zv, one for each component. Finally, in the third phase, participants completed a survey that measured 
their satisfaction levels and preferences, followed by open-ended interview questions\techreport{ assessing the strengths and weaknesses 
of the tools, and whether they would use the two tools in their workflow}. 
\techreport{
The average study session lasted for 75 minutes on average. 
Participants were paid ten dollars per hour for their participation.}

\paragraph{Tasks.}
We prepared three sets of tasks for the three interfaces (baseline, drag and
drop, and custom query builder interfaces). The tasks differed only in the
selection attributes so that the performance of the interfaces could be
measured objectively (Section~\ref{sec:frontend} lists a few example queries
that were used in the study \papertext{; others can be found
in the techreport~\cite{techreport}}).
\techreport{We carefully selected tasks which can surface during a
search for a house.
}
To demonstrate the efficacy of \zv, the tasks focused on
identifying patterns, trends or anomalies in housing data; these are
non-trivial tasks to complete with a large dataset with existing tools. 
\techreport{The tasks were targeted to test the core features of \zv and solicit responses from participants on how \zv could complement existing data analytics workflows.}

%The tasks that the participants performed on the two tools were designed to be similar in nature (differing only in selection attributes) so that we could compare the performance more objectively. The tasks focused on identifying visualizations with specific insights. Section~\ref{sec:frontend} lists a few example queries that were used in the study. 

\paragraph{Metrics.} 
We manually recorded how participants used the tool and their answers for each task. We further collected browser activity using screen capture software, system interaction logs, audio of discussions, and asked the participants to complete a survey. 
\techreport{Using this data, we collected the following metrics: task completion time, task accuracy, the number of visualizations browsed for each task, the number of edits made on the drawing panel and the query table, and the usability ratings and satisfaction level from the survey results.}

\paragraph{Ground Truth.} 
Two expert data analysts prepared the ground truth for each the tasks in the form of ranked answers. Their inter-rater agreement, measured using Kendall\textquotesingle s Tau rank correlation coefficient, was 0.854. 
% The experts further discussed and resolved the conflicting answers to produce a final single ranked list of answers for each task. 
Then, each expert independently rated the answers on their ranked list on a 0 to 5 scale (5 highest). We took the average of the two scores to rate the participants\textquotesingle~answers.

\techreport{
\begin{figure}
\vspace{-5pt}
\begin{center}
\fbox{\includegraphics[width=0.6\columnwidth]{figs/baseline.png}}
\end{center}
\vspace{-20pt}
\caption{The baseline interface implemented for the user study.}
\label{fig:baseline}
\vspace{-10pt}
\end{figure}
}


\subsection{Key Findings}
Six key findings emerged from the study.  We describe them below.
We use $\mu$, $\sigma$, $\chi^2$ to denote average, standard deviation, and Chi-square test scores, respectively.

\paraem{Finding 1: \zv enabled over 50\% and 100\% faster task completion times than the baseline for the custom query builder and drag and drop interfaces, respectively.} 
%We timed the task completion time for all participants,
%and 
We found that the drag and drop interface
had the smallest, and most predictable completion time ($\mu=74s$, $\sigma=15.1$),
the custom query builder had a higher completion time and a higher variance ($\mu=115s$, $\sigma=51.6$),
while the baseline had a significantly higher completion time ($\mu=172.5s$, $\sigma=50.5$), more than twice the completion time of the drag and drop interface,
and almost 50\% higher than the custom query builder interface.
This is not surprising, given that the baseline tool requires more
manual exploration compared to the other two interfaces.
%We tested for statistical significance on the difference between the three interfaces 
Using 
a one-way between-subjects ANOVA,
followed by a post-hoc Tukey's test~\cite{tukey1949comparing},
we found that the
drag and drop interface and the custom query builder interface
had statistically significant
faster task completion times compared to the baseline interface,
with $p$ values of 0.0010 and 0.0069, respectively.
The difference between the drag and drop interface
and the custom query builder interface was not statistically significant,
with a $p$ value of $0.06$.
\papertext{Detailed statistics can be found in ~\cite{techreport}.}


% One-way between subject ANOVA~\cite{tukey1949comparing} on the completion time of the tasks revealed that the differences between the three interfaces were significant F(2, 33) = 16.02, p < 0.001. On further applying the post hoc Tukey's test\cite{tukey1949comparing} for comparing the pairwise task completion times among the three interfaces (Table \ref{tab:turkeytest}), we found that both the drag and drop interface and the custom query builder had significantly faster task completion times compared to the baseline tool (p < 0.01), whereas the difference between the drag and drop interface, and the custom query builder was not significantly different.  
\techreport{
\begin{table}[H]
 \centering \scriptsize
  {\tt    
  \scalebox{0.75}{
  \begin{tabular}{|l|l|l|l|} \hline
      \textbf{Treatments} & \textbf{Q statistic} & \textbf{p-value} & \textbf{inference} \\ \hline
	Drag and drop interface vs. Custom query builder & 3.3463 & 0.0605331 & insignificant \\ \hline
	Drag and drop interface vs. Baseline tool & 7.9701 & 0.0010053 & significant (p<0.01) \\ \hline
	Custom query builder vs. Baseline tool & 4.6238 & 0.0069276 & significant (p<0.01) \\ \hline    
     \end{tabular}
  }
  }
\vspace{-10pt}
\caption{Tukey's test on task completion time}
\label{tab:turkeytest}
\vspace{-10pt}
\end{table}
}

\begin{figure}
\begin{center}
\includegraphics[width=0.6\columnwidth]{figs/accuracyvstime.pdf}
\end{center}
\vspace{-25pt}
\caption{Accuracy over time results for zenvisage and baseline}
\label{fig:accuracyvstime}
\vspace{-20pt}
\end{figure}


\paraem{Finding 2: \zv helped retrieve 20\% and 35\% more accurate results than the baseline for drag and drop and custom query builder interfaces, respectively.} 
Comparing the participants' responses against the ground truth responses, 
we found that participants achieved the lowest accuracy with the baseline tool ($\mu=69.9\%$, $\sigma=13.3$). 
\techreport{Further, any tasks that required comparing multiple
pairs of visualizations or trends ended up having lower accuracy than those that looked
for a pattern, such as a peak or an increasing trend, in a single visualization.}
The low accuracy could be attributed to the fact that the participants selected suboptimal answers before browsing through the entire list of results for better answers. Conversely, \zv is able to
accept more fine-grained user input, and
rank output visualizations. With \zv,
participants were able to retrieve accurate answers
with less effort.
Between the two interfaces in \zv,
while the drag and drop interface had a faster task completion time,
it also had a lower accuracy ($\mu=85.3\%$, $\sigma=7.61$).
The custom query builder interface had the
highest accuracy ($\mu=96.3\%$, $\sigma=5.82$).
The accuracy was greater than the baseline by over 20\% (for drag and drop)
and over 35\% (for custom builder).
Finally, looking at both accuracy and task completion times together in Figure~\ref{fig:accuracyvstime}, we see that \zv \emph{helps in ``fast-forwarding to desired visualizations'' with high accuracy.} 

% In the baseline, comparing multiple pairs of visualizations or trends revealed to be more difficult than looking for a visual property, such as a peak, or an increasing pattern, in a single visualization. on the other hand, since \zv returns a ranked list of filtered answers based on the user specification, which allowed the participants to browse through a smaller number of visualizations compared to the baseline tool. Among the two interfaces in \zv, participants completed the tasks faster ($\mu=74s$, $\sigma=15.1$) on the drag and drop interface, but at the price of accuracy ($\mu=85.3\%$, $\sigma=7.61$). Participants using the custom query builder had the highest accuracy ($\mu=96.3\%$, $\sigma=5.82$) while maintaining comparable speeds to the drag and drop interface ($\mu=115s$, $\sigma=51.6$). Finally, looking at both accuracy and task completion times together (as depicted in Figure~\ref{fig:accuracyvstime}), we can conclude that \zv \textbf{helps in fast-forwarding to desired visualizations with a high accuracy.}

% Figure \ref{fig:accuracyvstime} depicts the average accuracy over time results of each of the participants on each of the interfaces. \agp{move this later.} 

%\textbf{\zv helps retrieve accurate results}: We rated the participant's answers based on the expert's gold set answers. The ratings revealed that participants performed poorly with the baseline, with the lowest accuracy ($\mu=69.9\%$, $\sigma=13.3$) and longest task completion time. In the baseline, comparing multiple pairs of trends or graphs revealed to be more difficult than looking for a single trend such as a peak, or an increasing pattern. \zv, on other hand, returns a ranked list of results based on the user specification, which allowed the participants to browse through a smaller number of visualizations compared to the baseline tool. Among the two interfaces in \zv, participants completed the tasks fastest ($\mu=74s$, $\sigma=15.1$) on the drag and drop interface, but at the price of accuracy ($\mu=85.3\%$, $\sigma=7.61$). Participants using the custom query builder had the highest accuracy ($\mu=96.3\%$, $\sigma=5.82$) while maintaining comparable speeds to the drag and drop interface ($\mu=115s$, $\sigma=51.6$). \textbf{\zv, thus, helps in fast-forwarding to desired visualizations with a high accuracy}.


\paraem{Finding 3: The drag and drop interface and the custom query builder complemented each other.
The drag and drop interface was intuitive, while the custom builder was useful for complex queries.} 
The majority of participants (8/12) 
mentioned that the drag and drop 
and custom query builder interfaces complemented each other \techreport{in making \zv powerful}
when searching for specific insights.\techreport{ 25 percent (3/12) of the participants (all with backgrounds in machine learning and data mining) 
preferred using only the custom query builder interface, while just one participant (with no prior experience with query languages or programming languages) found only the drag and drop interface as useful.}
Participants stated that they would use drag and drop for exploration and simple pattern search and custom query builder for specific / complex queries. One stated: \textit{``When I know what I am looking for is exact, I want to use the query table. The other one [drag and drop] is for exploration when you are not so sure.'' (P9).} 
Participants liked the simplicity and intuitiveness of the drag and drop interface. 
One stated: \textit{``[The drawing feature] is very easy and intuitive, I can draw... and automatically get the results.''} (P2).
On a five-point Likert (5 is strongly agree), participants
found the drawing feature easy to use ($\mu=4.45$, $\sigma=0.674$). 
Despite the positive feedback, participants did identify limitations,
specifically, that the functionality was restricted to identifying trends
similar to a single hand-drawn trend.

The majority of the participants (11/12) 
stated that the custom query builder 
allowed them to search for complex insights that might be 
difficult to answer via drag and drop. 
When asked if the custom query builder was difficult to use compared to
the drag-and-drop interface, participants tended to agree ($\mu=3.73$, $\sigma=0.866$ on a five-point Likert scale where five is strongly agree).
At the same time, participants acknowledged 
the flexibility and the efficacy supported by the custom query builder interface. 
One participant explicitly stated why they 
preferred the custom query builder over the drag and drop interface: 
\textit{``When the query is very complicated, [the drag and drop] has its limitations, 
so I think I will use the query table most of the times unless I want to get a brief impression.''} (P6).
Another participant stated that the custom query builder interface \textit{``has more functionality and it lets me express what I want to do clearly more than the sketch''} (P4). This was consistent with other responses. 
Participants with a background in machine learning, 
data mining, and statistics (3/12) preferred
using the custom query builder over the drag and drop interface.

\paraem{Finding 4: \zv was more effective (4.27 vs. 2.67 on a five-point Likert scale)
than the baseline tool at visual data exploration, and would be
a valuable addition to the participants' analytics workflow.} 
In response to the survey question \emph{``I found the tool to be effective in visualizing the data I want to see''}, the participants rated \zv (the drag and drop interface and the custom query builder interface) higher ($\mu=4.27$, $\sigma=0.452$) than the baseline tool ($\mu=2.67$, $\sigma=0.890$) on a five-point Likert scale.
A participant experienced in Tableau commented: \textit{``In Tableau, there is no pattern searching. If I 
see some pattern in Tableau, such as a decreasing pattern, and I want to see if any other variable is 
decreasing in that month, I have to go one by one to find this trend. But here I can find this through the query table.''} (P10).

\paraem{Finding 5: \zv is a valuable addition to the participants' analytics workflow, and allows a quicker initial data exploration before performing complex analysis:}
When asked about using the two tools in their current workflow, 9 of the 12 participants stated that they would use \zv in their workflow, whereas two participants stated that they would use our baseline tool ($\chi^2=8.22$, p<0.01). When the participants were asked how they would use \zv in their workflow, one participant provided a specific scenario: \textit{``If I am doing my social science study, and I want to see some specific behavior among users, then I can use tool A [\zv] since I can find the trend I am looking for and easily see what users fit into the pattern.''} (P7). Other participants mentioned that they would use this tool for finding outliers during data cleaning, common patterns search, and initial understanding of data distributions before running a machine learning task. 
Participants could not articulate specific reasons for using the baseline tool. As one participant put it, \textit{``the existing solutions can already do what tool B [the baseline interface] can do.''} (P10).
Even those with considerable experience with programming languages such as Python and Matlab 
argued that \zv would take less time and fewer lines of code for basic data exploration tasks. 
Participants (2/12) who had considerable experience with data mining libraries were asked a follow-up 
question to write code for a task similar to Table 11 in their favorite language;
this task took them multiple orders of magnitudes more time and lines of code compared to \zq. 
\papertext{Example code can be found in~\cite{techreport}.} \techreport{Example code can be found in the appendix \ref{sec:userstudy-appendix}.} 



% \textbf{\zv is novel with real world applications}: All the participants described \zv as a novel tool and mentioned that they have not seen the declarative pattern searching functionality in any other tool.

\paraem{Finding 6: \zv can be improved.}  
\papertext{Participants also outlined some areas for improvement; details can be found in~\cite{techreport}.
In brief, the participants requested drag-and-drop interactions to support additional operations, such as outlier finding.
Others commented on how the interface could be more polished, and suggested that we add bookmarking and search history capabilities.}
\techreport{
 While the participants looked forward to using custom query builder in their own workflow, a few of them were interested in directly exposing the commonly-used trends/patterns such as outliers, through the drag and drop interface. Some were interested in knowing how they could integrate custom functional primitives (we could not cover it in the tutorial due to time constraints). In order to improve the user experience, participants suggested adding instructions and guidance for new users as part of the interface. Participants also commented on the unrefined look and feel of the tool, as well as the lack of a diverse set of usability related features, such as bookmarking and search history, that are offered in existing systems.
}



