import numpy as np
import matplotlib.pyplot as plt

plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True


fig = plt.figure()
ax = fig.add_subplot(111)
N = 3
ind = np.arange(N)  # the x locations for the groups
ind

width = 0.15       # the width of the bars
singlegby = [0.9,4.2,150]
query1 =[1.2,5.4,162]
query2 =[1.3,5.5,180]
query3 =[1.7,6.2,190]
#BitmapRandom = np.power(10, [4.847455108,5.8348052,6.538560419,6.83966943,6.953612782])

rects1 = ax.bar(ind,singlegby , width, color='red',align='center')
rects2 = ax.bar(ind+width,query1, width, color='yellow',align='center')
rects3 = ax.bar(ind+2*width,query2, width, color='b',align='center')
rects4 = ax.bar(ind+3*width,query3, width, color='green',align='center')
#rects5 = ax.bar(ind+4*width,BitmapRandom, width, color='r',align='center')
ax.set_ylabel('time (s)',fontsize=14)
ax.set_xlabel('#rows',fontsize=14)
ax.set_yscale('log')
ax.yaxis.labelpad = 0
plt.xticks(fontsize=14)  
plt.yticks(fontsize=14)
ax.set_xticks(ind+width)
ax.set_xticklabels( ('2M','20M','120M' ) )
#ax.legend( (rects1[0], rects2[0],rects3[0],rects4[0],rects5[0]), ('no-opt', 'Two-Prong','Bitmap-Scan','Disk-Scan','Bitmap-Random '), loc = "upper left",prop={'size':8} )
ax.legend( (rects1[0], rects2[0],rects3[0],rects4[0]), ('singlegby', 'query1','query2','query3'), loc = "upper left",prop={'size':12} )
fig = plt.gcf()
fig.subplots_adjust(hspace=.25, wspace=0.3)
fig.set_facecolor("white")
fig.set_size_inches(5,3.8)
plt.savefig("../figs/optrealrowdataset.pdf",bbox_inches='tight')
#plt.show()

