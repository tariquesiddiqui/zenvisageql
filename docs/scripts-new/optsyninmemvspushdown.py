import numpy as np
import matplotlib.pyplot as plt

plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True


fig = plt.figure()
ax = fig.add_subplot(111)
N = 4
ind = np.arange(N)  # the x locations for the groups
ind

width = 0.15       # the width of the bars
zql = [3.247,3.807,5.305,5.889]
sql =[3.562,4.009,5.368,6.153]



#BitmapRandom = np.power(10, [4.847455108,5.8348052,6.538560419,6.83966943,6.953612782])
index=['10','100','1000','10000']
#rects1 = ax.bar(ind,noopt , width, color='y',align='center')
#rects2 = ax.bar(ind+width,speculate, width, color='blue',align='center')
#rects3 = ax.bar(ind+2*width,smartfuse, width, color='g',align='center')

rects1=ax.plot(index, zql, color='g',  lw=2, marker='s',mec='black', markevery=1,ms=5)
rects2=ax.plot(index, sql, color='r', lw=2, marker='x',mec='black', markevery=1,ms=5)


#rects5 = ax.bar(ind+4*width,BitmapRandom, width, color='r',align='center')
ax.set_ylabel('time (s)',fontsize=12)
ax.set_xlabel('#visualizations',fontsize=12)
#ax.set_yscale('log')
ax.yaxis.labelpad = 0
plt.xticks(fontsize=11)  
plt.yticks(fontsize=11)
ax.set_xticks(ind+width)
ax.set_xticklabels( ('10','100','1000','10000' ) )
ax.set_xscale('log')
#ax.legend( (rects1[0], rects2[0],rects3[0],rects4[0],rects5[0]), ('no-opt', 'Two-Prong','Bitmap-Scan','Disk-Scan','Bitmap-Random '), loc = "upper left",prop={'size':8} )
ax.legend( (rects2[0],rects1[0], ), ('sql','zql'), loc = "upper left",prop={'size':8} )
fig = plt.gcf()
fig.subplots_adjust(hspace=.25, wspace=0.3)
fig.set_facecolor("white")
fig.set_size_inches(5,3.8)
plt.savefig("../figs/optsynzqlvsql.pdf",bbox_inches='tight')
#plt.show()

