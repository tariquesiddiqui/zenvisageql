import numpy as np
import matplotlib.pyplot as plt

plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True


N = 3
ind = np.arange(N)  # the x locations for the groups
width = .35    # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(121)
values = [245, 39, 21]
ind = np.arange(len(values))
rects1 = ax.bar(ind, values)
rects1[0].set_color('lightblue')
rects1[1].set_color('blue')
rects1[2].set_color('darkgreen')
ax.set_ylabel('time(s)',fontsize=12)
ax.yaxis.labelpad = 0
plt.xticks(fontsize=12)  
plt.yticks(fontsize=12) 
#ax.set_xticks(ind+width)
#ax.set_xticklabels( ('NoOpT', 'IntraLineOpt','InterProcessOpt'),rotation=-8,fontsize=6,weight='bold')
plt.setp( ax.get_xticklabels(), visible=False)



ax = fig.add_subplot(122)
values = [40,2,1]
ind = np.arange(len(values))
rects1 = ax.bar(ind, values,)
rects1[0].set_color('lightblue')
rects1[1].set_color('blue')
rects1[2].set_color('darkgreen')
ax.set_ylabel('Count(SQL queries)',fontsize=12)
ax.yaxis.labelpad = 0
plt.xticks(fontsize=12)  
plt.yticks(fontsize=12) 
#ax.set_xticks(ind+width)
#ax.set_xticklabels( ('NoOpT', 'IntraLineOpt','InterProcessOpt'),rotation=-8,fontsize=6,weight='bold')

plt.figlegend((rects1[0],rects1[1],rects1[2]), ('NoOpT','Intra-Line','Inter-Task'),'upper right',prop={'size':10})
plt.setp( ax.get_xticklabels(), visible=False)

fig = plt.gcf()
fig.subplots_adjust(hspace=.25, wspace=0.3)
fig.set_facecolor("white")
fig.set_size_inches(5,2)
plt.savefig("opt_real1.pdf",bbox_inches='tight')


plt.show()