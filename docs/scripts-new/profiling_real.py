import numpy as np
import matplotlib.pyplot as plt

plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

fig = plt.figure()
ax = fig.add_subplot(111)
N = 2
ind = np.arange(N)  # the x locations for the groups
ind
width = 0.27       # the width of the bars
Similarity = [.4,9]
rects1 = ax.bar(ind,Similarity , width, color='r',align='center')
Representative= [1.5,45]
rects2 = ax.bar(ind+width,Representative, width, color='g',align='center')
Outlier= [2.2,58]
rects3 = ax.bar(ind+2*width,Outlier, width, color='b',align='center')
ax.set_ylabel('time(s)')
ax.set_xticks(ind+width)
ax.set_xticklabels( ('census-data', 'airline') )
ax.legend( (rects1[0], rects2[0],rects3[0]), ('Similarity', 'Representative', 'Outlier'),"upper left" )

fig = plt.gcf()
fig.subplots_adjust(hspace=.25, wspace=0.3)
fig.set_facecolor("white")
fig.set_size_inches(10,6)
plt.savefig("profiling_real.pdf",bbox_inches='tight')

plt.show()
