import numpy as np
import matplotlib.pyplot as plt

plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True


fig = plt.figure()
ax = fig.add_subplot(111)
N = 5
ind = np.arange(N)+1  # the x locations for the groups
ind

width = 0.15       # the width of the bars
noopt = [11.5,19,26,33,39]
parallel = [11.5,12.9,13.2,15.8,17.4]
speculate =[4.8,7.4,8.7,9.5,12.4]
smartfuse =[4.14,5.3,6.1,7.4,8.1]
#BitmapRandom = np.power(10, [4.847455108,5.8348052,6.538560419,6.83966943,6.953612782])
index=['1','2','3','4','5']

#rects1 = ax.bar(ind,noopt , width, color='y',align='center')
#rects2 = ax.bar(ind+width,speculate, width, color='blue',align='center')
#rects3 = ax.bar(ind+2*width,smartfuse, width, color='g',align='center')

rects1=ax.plot(index, noopt, color='r',  lw=2, marker='s',mec='black', markevery=1,ms=5)
rects2=ax.plot(index, parallel, color='y', lw=2, marker='x',mec='black', markevery=1,ms=5)
rects3=ax.plot(index, speculate, color='b', lw=2, marker='x',mec='black', markevery=1,ms=5)
rects4=ax.plot(index, smartfuse, color='green', lw=2, marker='o',mec='black', markevery=1,ms=5)

#rects5 = ax.bar(ind+4*width,BitmapRandom, width, color='r',align='center')
ax.set_ylabel('time (s)',fontsize=14)
ax.set_xlabel('# chains of c-nodes and p-nodes',fontsize=13)
#ax.set_yscale('log')
ax.yaxis.labelpad = 0
plt.xticks(fontsize=14)  
plt.yticks(fontsize=14)
ax.set_xticks(ind)
#ax.set_xticklabels( index )
#ax.set_xscale('log')
#ax.legend( (rects1[0], rects2[0],rects3[0],rects4[0],rects5[0]), ('no-opt', 'Two-Prong','Bitmap-Scan','Disk-Scan','Bitmap-Random '), loc = "upper left",prop={'size':8} )
ax.legend( (rects1[0], rects2[0],rects3[0],rects4[0]), ('no-opt','parallel','speculate','smartfuse'), loc = "upper left",prop={'size':12} )
fig = plt.gcf()
fig.subplots_adjust(hspace=.25, wspace=0.3)
fig.set_facecolor("white")
fig.set_size_inches(5,3.8)
plt.savefig("../figs/optsynparamchains.pdf",bbox_inches='tight')
#plt.show()

