import numpy as np
import matplotlib.pyplot as plt
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True


fig = plt.figure()
ax = fig.add_subplot(111)
N = 3
ind = np.arange(N)  # the x locations for the groups
ind

width = 0.15       # the width of the bars
noopt = [17.4,11.5,17.6]
parallel = [10.3,8.5,11.5]
speculate =[7.1,6.9,8.5]
smartfuse =[5.4,5.5,6.2]
#BitmapRandom = np.power(10, [4.847455108,5.8348052,6.538560419,6.83966943,6.953612782])

rects1 = ax.bar(ind,noopt , width, color='red',align='center')
rects2 = ax.bar(ind+width,parallel, width, color='yellow',align='center')
rects3 = ax.bar(ind+2*width,speculate, width, color='b',align='center')
rects4 = ax.bar(ind+3*width,smartfuse, width, color='green',align='center')
#rects5 = ax.bar(ind+4*width,BitmapRandom, width, color='r',align='center')
ax.set_ylabel('time (s)',fontsize=13)
ax.set_xlabel(' Queries',fontsize=13)
#ax.set_yscale('log')
ax.yaxis.labelpad = 0
plt.xticks(fontsize=13)  
plt.yticks(fontsize=13)
ax.set_xticks(ind+width)
ax.set_xticklabels( ('Q1','Q2','Q3' ) )
#ax.legend( (rects1[0], rects2[0],rects3[0],rects4[0],rects5[0]), ('no-opt', 'Two-Prong','Bitmap-Scan','Disk-Scan','Bitmap-Random '), loc = "upper left",prop={'size':8} )
ax.legend( (rects1[0], rects2[0],rects3[0],rects4[0]), ('no-opt', 'parallel','speculate','smartfuse'), loc = "upper center",prop={'size':11} )
fig = plt.gcf()
fig.subplots_adjust(hspace=.25, wspace=0.3)
fig.set_facecolor("white")
fig.set_size_inches(5,3.8)
plt.savefig("../figs/optreal20m.pdf",bbox_inches='tight')
#plt.show()

