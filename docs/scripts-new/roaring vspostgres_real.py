import matplotlib.pyplot as plt
import numpy as np

plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

ax=plt.subplot(121)
N = 2
ind = np.arange(N)  # the x locations for the groups
width = 0.27       # the width of the bars
roaring = [18,2]
rects1 = ax.bar(ind, roaring, width, color='g',align='center')
postgres= [14,6]
rects2 = ax.bar(ind+width,postgres, width, color='r',align='center')
ax.set_ylabel('time(s)',fontsize=12)
plt.yticks(fontsize=6) 
ax.set_xticks(ind+width)
ax.set_xticklabels( ('100% selectivity', '10% selectivity'),fontsize=7 )
plt.annotate('Airline', (0,0), (50, -20),fontsize=8, xycoords='axes fraction', textcoords='offset points', va='top')




ax=plt.subplot(122)
N = 2
ind = np.arange(N)  # the x locations for the groups
width = 0.27       # the width of the bars
roaring = [964,85]
rects1 = ax.bar(ind, roaring, width, color='g',align='center')
postgres= [730,124]
rects2 = ax.bar(ind+width,postgres, width, color='r',align='center')
ax.set_ylabel('time(ms)',fontsize=12)
ax.yaxis.labelpad = 0
plt.xticks(fontsize=8)  
plt.yticks(fontsize=6) 
ax.set_xticks(ind+width)
ax.set_xticklabels( ('100% selectivity', '10% selectivity'),fontsize=7  )
plt.annotate('CensusIncome', (0,0), (50, -20), fontsize=8 ,xycoords='axes fraction', textcoords='offset points', va='top')



ax.legend( (rects1[0], rects2[0]), ('roaring','postgresql'),fontsize=8 )

# plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
# plt.tight_layout()
fig = plt.gcf()
fig.subplots_adjust(hspace=.25, wspace=0.3)
fig.set_facecolor("white")
fig.set_size_inches(5,2)
plt.savefig("roaringvspostgres_real.pdf",bbox_inches='tight')



plt.show()


