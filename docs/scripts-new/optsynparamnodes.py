import numpy as np
import matplotlib.pyplot as plt

plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True


fig = plt.figure()
ax = fig.add_subplot(111)
N = 7
ind = np.arange(N)  # the x locations for the groups
ind

width = 0.15       # the width of the bars
noopt = [2.85,6.74,8.4,10.6,11.5,13.7,15.4]
speculate =[2.85,3.64,3.93,4.26,4.8,6.3,7.9]
smartfuse =[2.85,3.17,3.22,3.95,4.14,4.9,5.2]
#BitmapRandom = np.power(10, [4.847455108,5.8348052,6.538560419,6.83966943,6.953612782])
index=['1','2','3','4','5','8','10']



#rects1 = ax.bar(ind,noopt , width, color='y',align='center')
#rects2 = ax.bar(ind+width,speculate, width, color='blue',align='center')
#rects3 = ax.bar(ind+2*width,smartfuse, width, color='g',align='center')

rects1=ax.plot(index, noopt, color='r',  lw=2, marker='s',mec='black', markevery=2,ms=5)
rects2=ax.plot(index, speculate, color='b', lw=2, marker='x',mec='black', markevery=2,ms=5)
rects3=ax.plot(index, smartfuse, color='green', lw=2, marker='o',mec='black', markevery=2,ms=5)

#rects5 = ax.bar(ind+4*width,BitmapRandom, width, color='r',align='center')
ax.set_ylabel('time (s)',fontsize=13)
ax.set_xlabel('#c-nodes and p-nodes in one chain',fontsize=13)
#ax.set_yscale('log')
ax.yaxis.labelpad = 0
plt.xticks(fontsize=14)  
plt.yticks(fontsize=14)
#ax.set_xticks(ind+width)
#ax.set_xticklabels( index )
#ax.set_xscale('log')
#ax.legend( (rects1[0], rects2[0],rects3[0],rects4[0],rects5[0]), ('no-opt', 'Two-Prong','Bitmap-Scan','Disk-Scan','Bitmap-Random '), loc = "upper left",prop={'size':8} )
ax.legend( (rects1[0], rects2[0],rects3[0]), ('no-opt, parallel','speculate','smartfuse'), loc = "upper left",prop={'size':12} )
fig = plt.gcf()
fig.subplots_adjust(hspace=.25, wspace=0.3)
fig.set_facecolor("white")
fig.set_size_inches(5,3.8)
plt.savefig("../figs/optsynparamnodes.pdf",bbox_inches='tight')
#plt.show()

