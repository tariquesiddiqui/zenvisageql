%!TEX root=zenvisageql.tex

\begin{table*}[!htbp]
%\vspace{-5pt}
  \centering\scriptsize
 {\tt
  %\resizebox{\linewidth}{!}{%
    \begin{tabular}{|r|l|l|l|l|l|l|} \hline
      \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z} &
      \textbf{Constraints}
      & \textbf{Viz} & \textbf{Process} \\ \hline
      f1 & `year' & `sales' & v1 \IN P & location=`US' & bar.(y=agg('sum')) &
      v2 \IN
      $argany_{v1}[t>0] T(f1)$ \\
      f2 & `year' & `sales' & v1 & location=`UK' & bar.(y=agg('sum')) & v3 \IN $argany_{v1}[t<0] T(f2)$ \\
      *f3 & `year' & `profit' & v4 \IN (v2.range | v3.range) &
      & bar.(y=agg('sum')) & \\ \hline
    \end{tabular}
  %}
  %\end{alltt}
  }
    \vspace{-10pt}
  \caption{
    A \zq query which returns the profit over time visualizations for products
    in user-specified set {\tt P}
    that have positive sales over time trends for the US but have negative sales
    over time trends for the UK.
  }
  \label{tab:exec-1}
  {\tt
  %\resizebox{\linewidth}{!}{%
    \begin{tabular}{|r|l|l|l|l|l|l|} \hline
      \textbf{Name} & \textbf{X} & \textbf{Y} & \textbf{Z} &
      \textbf{Constraints} & Viz
      & \textbf{Process} \\ \hline
      f1 & `location' & `sales' & v1 \IN P & year=`2010' & bar.(y=agg('sum')) & \\
      f2 & `location' & `sales' & v1 & year=`2015' & bar.(y=agg('sum')) & v2 \IN $argmax_{v1}[k=10] D(f1,f2)$ \\
      *f3 & `location' & `profit' & v2 & year=`2010' & bar.(y=agg('sum')) & \\
      *f4 & `location' & `profit' & v2 & year=`2015' & bar.(y=agg('sum')) & \\ \hline
	  	
    \end{tabular}
  %}
  %\end{alltt}
  }
  \vspace{-10pt}
  \caption{
    A \zq query which returns the profit over location visualizations for
    products in user-specified set {\tt P}
    that have the most different sales over location trends between 2010 and 2015.
  }
  \label{tab:exec-2}
    \vspace{-10pt}
\end{table*}

\section{Query Execution}
\label{subsec:query_execution}
In \zv, \zq queries are automatically parsed and executed by the backend.
The \zq compiler translates \zq queries into a collection of \sql queries
that are issued to a relational database and performs
post-processing computation on the results
of the \sql queries. 
By translating to \sql, we do not tie ourselves down to any specific
  relational database and can seamlessly leverage benefits from improvements to
databases.
%\new{By operating outside standard relational databases, we can seamlessly
%leverage benefits from improvements to the database; further, we can replace
%one relational database for another. 
%That said, we may be able to leverage benefits from moving computation
%``closer to'' the data; we plan to explore this in future work.}

\subsection{Translation to Database Queries}
Each row of a \zq query can be broken up into two parts: the visual component and the
the Process column. The visual components 
determine the set of data \zv needs to retrieve from the database, and the
Process column translates into the post-processing computation to be done on
the returned data. \techreport {\zv's naive \zq compiler performs the following operations for each row of a 
\zq query.}
For each row, the \zq compiler issues a \sql query 
corresponding to each visualization in the set specified in the visual component.
\techreport{Notice that this approach is akin to what a visual analyst manually
generating each visualization of interest and perusing it would do;
here, we are ignoring the human perception cost.}
More formally, for each row (i.e., each name variable),
the \zq compiler loops over all combinations of values
for each of the $n$ axis variables in the visual component corresponding
to that row, and for each combination, 
issues a \sql query,
the results of which are stored in the corresponding
location in an $n$ dimensional array.
\techreport{(Essentially, this corresponds to a nested for loop $n$ levels
deep.)}
Each
generated \sql query has the form:
{\tt \small SELECT X, Y FROM R WHERE Z=V AND (CONSTRAINTS) ORDER BY X}.
% \vspace{-5pt}
% \begin{quote} \small
% \begin{verbatim}
% SELECT X, Y 
% WHERE Z=V and (CONSTRAINTS) 
% ORDER BY X
% \end{verbatim}
% \end{quote}
% \vspace{-5pt}
If the summarization is specified, additional clauses
such as {\tt GRO\-UP BY}s and aggregations may be added. 
The results of each visualization are stored 
in an $n$-dimensional array
at the current
index. If $n$ is 0, the name variable points directly to the data.
\techreport{Going forward, we will omit the \texttt{FROM} clause: this is implied
and always fixed.}



Once the name variable array has been filled in with results
from the \sql queries,  
the \zq compiler generates the
post-processing code from the task in the Process
column. 
At a high level, \zq loops through all the visualizations, and applies
the objective function on each one. 
In particular, for each input axis variable in the mechanism, a 
{\tt for} loop is created and nested. The input axis variables are then used to
step through the arrays specified by the name variable, and the objective
function is called at each iteration. Instances of name variables are
updated with the correct index based on the axis variables it depends on. The
functions themselves are considered black boxes to the \zq compiler and are unaltered.
% \agp{This may be precise from a procedural standpoint but this is rather
% opaque. Can we talk about what you're trying to do? seems like all you're doing is to
% pass along each generated visualization to the objective function which has access
% to all the characteristics/attributes/properties of that visualization}
\papertext{We provide the psuedocode of the compiled version of the
query in~\cite{techreport}.}
\techreport{We provide the psuedocode of the compiled version of the
query expressed in Table~\ref{tab:exec-1} in Listing~\ref{lst:exec-1} in
the appendix.}


\subsection{External Optimizations}
Processing each visualization as an independent \sql query can be 
both wasteful and lead to unacceptable latencies.
We now describe how to rewrite and batch the \sql queries
via three levels of optimizations to reduce latency.
These optimizations are reminiscent of multi-query optimization
(MQO)~\cite{DBLP:journals/tods/Sellis88}; however, MQO techniques require
significant changes to the underlying database
engine~\cite{giannikis2013workload,psaroudakis2013sharing,DBLP:journals/pvldb/KementsietsidisNCV08,giannikis2014shared},
whereas our
syntactic rewriting techniques operate completely outside the database and are
tailored to the \zq setting.
\papertext{We omit many details that can be found in~\cite{techreport}.}
%The first two are straightforward and are 
%are reminiscent of multi-query optimization (MQO)~\cite{DBLP:journals/tods/Sellis88}---\new{however,
%MQO requires significant changes to the underlying
%engine, as opposed to our syntactic rewriting techniques
%that operate outside the database.}
%We focus on the third level, tailored to \zq.


% \techreport{While the translation process of the naive \zq compiler is simple and easy to
% follow, there is plenty of room for optimization. In particular, we look at
% three levels of optimization which focus on \emph{batching} \sql queries. 
% We found that the time it took to submit and wait for \sql queries was a
% bottleneck, and reducing the overall number of \sql queries can reduce performance 
% significantly.
% While none of these optimizations may seem particularly surprising,
% it required a substantial amount of effort to identify how these optimizations
% may be applied automatically to any \zq query. 
% \new{Our proposed optimizations are
% reminiscent of multi-query optimization techniques~\cite{DBLP:journals/tods/Sellis88}, but are tailored
% to \zq queries.}
% }

\paragraph{Intra-Line Optimization.}
\papertext{The first optimization level batches the \sql queries from a \zq row into one
query. For example, we can retrieve the data for all products in the first row
Table~\ref{tab:exec-1} in one \sql query. Similarly, if
the axis variable is in either the X or Y columns, we retrieve the data for
the entire set of attributes the axis variable iterates over. 
To ensure that the overhead of computation outside of the database
is minimal, we add appropriate {\tt ORDER BY} clauses to ensure
that additional sorting is not needed.
}
\iftechreportcode
The first level of optimization batches the \sql queries for a row into one
query. For example, we see that for the first row in Table~\ref{tab:exec-1}, that
a separate query is being made for every product in Listing~\ref{lst:exec-1}
in the appendix.
Instead, we can retrieve the data for all products in one \sql query:
\vspace{-5pt}
\begin{quote}\small
\begin{verbatim}
SELECT year, SUM(sales), product
  WHERE product IN P and location=`US'
  GROUP BY product, year
  ORDER BY product, year
\end{verbatim}
\end{quote}
\vspace{-5pt}
If the axis variable is in either the X or Y columns, we retrieve the data for
the entire set of attributes the axis variable iterates over. More concretely,
if we have axis variable {\tt y1 \IN \{`sales', `profit'\}}, our \sql
query would look like:
\vspace{-5pt}
\begin{quote}\small
\begin{verbatim}
SELECT year, SUM(sales), SUM(profit), product
  WHERE product IN P and location=`US'
  GROUP BY product, year
  ORDER BY product, year
\end{verbatim}
\end{quote}
\vspace{-5pt}
This optimization cuts down the number of queries by the sizes of sets the axis
variables range over, and therefore will lead to substantial performance benefits,
as we will see in the experiments.
Note that a side effect of batching queries is that the compiled code must now have an
extra phase to extract the data for different visualizations from the combined
results. However, since the \zq compiler includes an {\tt ORDER BY} clause, the
overhead of this phase is minimal.
\fi



\input{intra-proc-opt}
\input{inter-proc-opt}
