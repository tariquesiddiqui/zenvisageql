%!TEX root=zenvisageql.tex
\vspace{-5pt}
\section{Related Work}
\label{sec:related}
\vspace{-2pt}


We now discuss related prior work in a number of areas.
We begin with analytics tools --- visualization tools,
statistical packages and programming libraries, and relational databases.
Then, we talk about other tools that overlap somewhat with  \zv.
% To aid our discussion, in Figure \ref{fig:steps}, 
% we depict our abstract conceptualization of the three stages of data analytics;
% our specific target, with \zv, is not on the first or third stages, which 
% are better handled by other tools,
% but instead on the middle stage, 
% which is also the focus of current visual analytics tools. 
% Many of these tools target both programmers
% and non-programmers (like we do).
% \begin{figure}[!htb]
% \begin{center}
% \vspace{-5pt}
% \includegraphics[width=0.65\columnwidth]{figs/steps.pdf}
% \end{center}
% \vspace{-20pt}
% \caption{The stages of data analytics: the shaded box
% is our focus}
% \label{fig:steps}
% \vspace{-10pt}
% \end{figure}



\paragraph{Visual Analytics Tools.} 
Visualization tools, such as ShowMe, Spotfire, 
and Tableau~\cite{DBLP:journals/cacm/StolteTH08, DBLP:journals/tvcg/MackinlayHS07,Ahlberg:1996:SIE:245882.245893},
along with similar tools from the 
database community~\cite{DBLP:conf/sigmod/GonzalezHJLMSSG10,DBLP:conf/sigmod/KeyHPA12,DBLP:conf/sigmod/LivnyRBCDLMW97,DBLP:conf/avi/KandelPPHH12}
have recently gained in popularity, catering to 
data scientists who lack programming skills.
Using these tools, these scientists can 
select and view one visualization at a time.
However, these tools do not operate on 
collections of visualizations at a time---and thus
they are much less powerful and the 
optimization challenges are minimal.
\zv, on the other hand, supports queries over collections
of visualizations, returning results not much slower than the
time to execute a single query (See Section~\ref{sec:experiments}). 
Since these systems operate one visualization at a time, users are 
also not able to directly identify desired patterns or needs. 

\begin{table}[t]
\scriptsize
\begin{verbatim}
with ranking    as (
with distances as (
with distance_ product_year  as (
with aggregate_ product_year as (
   select   product, year, avg(profit) as avg_profit 
   from   table group by  product, year) )
   select  s. product as source, d. product as destination, s.year, 
           power(s.avg_profit - d.avg_profit,2) as distance_year 
   from   aggregate_ product_year s, aggregate_ product_year d 
   where  s. product!=d. product and s.year=d.year )
   select  source, destination, sum(distance_year) as distance 
   from   distance_ product_year   groupby  source, destination )
   select  source, destination, distance, 
           rank() over (partition by source order by distance asc) 
           rank from distances )
   select  source, destination, distance 
   from   ranking   where  rank < 10;
\end{verbatim}
\vspace{-15pt}
\caption{Verbose \sql query}\label{tab:verbose}
\vspace{-20pt}
\end{table}

\paragraph{Statistical Packages and Programming Libraries:}
Statistical tools (e.g., KNIME, RapidMiner, SAS, SPSS)
support the easy application of 
data mining and statistical primitives---including
prediction algorithms and statistical tests. 
While these tools support the selection of a prediction
algorithm (e.g., decision trees) 
to apply, and the appropriate parameters,
they offer no querying capabilities, 
and as a result do not need
extensive optimization.
As a result, these tools cannot support user needs like 
those describe in the examples in the introduction. 
Similarly, programming libraries such as 
Weka~\cite{holmes1994weka}
and Scikit-learn~\cite{pedregosa2011scikit} embed machine learning
 within programs.
However, manually translating the user desired patterns
into code that uses these libraries
will require substantial user effort and hand-optimization.
In addition, writing new code and hand-optimization will need
to be performed every time the exploration needs change.
Additionally, for both statistical tools and programming libraries, 
there is a need for programming ability and understanding
of machine learning and statistics to be useful---something 
we cannot expect all data scientists to possess.


\paragraph{Relational Databases.}
Relational databases can certainly support interactive analytics
via \sql. \zv uses relational databases as a backend computational
component, augmented with an engine that uses \fuse to optimize
accesses to the database, along with efficient processing code.
% an in-memory processing component and a sophisticated optimization layer
% that optimizes across this in-memory component and the databases backend.
Thus, one can certainly express some \zq queries 
by writing multiple \sql queries (via procedural \sql), 
using complex constructs only found in some databases, 
such as common table expressions (CTE) 
and window functions.
As we saw in Section~\ref{sec:userstudy}, these \sql queries are too cumbersome to write, and 
are not known to most users of databases---during our user study, we found that
all participants who had experience with \sql were not aware of these constructs; in fact,
they responded that they did not know of any way of issuing \zq queries in \sql, preferring
instead to express these needs in Python.
In Table~\ref{tab:verbose}, we list the verbose \sql query that
computes the following: for each product,
find 10 other products that have most similar profit over year trends. 
The equivalent 
\zq query takes two lines.
Further, we were able to write the \sql query only because the 
function $D$ is Euclidean distance: for other functions, we are unable
to come up with appropriate \sql rewritings.
On the other hand, for \zq, it is effortless to change the function by selecting it from a drop-down menu.
% \begin{wrapfigure}{l}{0.5\columnwidth}
% \vspace{-10pt}
%   \begin{center}
%     \includegraphics[width=0.48\columnwidth]{figs/optsynzqlvsqltwoloop.pdf}
%   \end{center}
% \vspace{-10pt}
% \caption{Performance of \zq vs \sql where we want to find top 10 similar products for every product. 
% We vary the number of products from 10 to 5000.}
% \vspace{-15pt}
% \label{fig:opt_syn_zqlvssql}
% \end{wrapfigure}
Beyond being cumbersome to write, the constructs required 
lead to severe performance penalties on most databases---for instance,
PostgreSQL materializes intermediate results when executing queries with CTEs.
To illustrate, we took the \sql query in Table~\ref{tab:verbose}, 
and compared its execution with the execution of the equivalent \zq. 
As depicted in Figure \ref{fig:opt_syn_zqlvssql}, 
the time taken by PostgreSQL increases sharply as 
the number of visualizations increases, taking up to 
10$\times$ more time as compared to \zq query executor.
This indicates that \zv is still important even for the restricted
cases where we are able to correctly write the queries in \sql.


\begin{figure}[!h]
\vspace{-10pt}
\begin{center}
\includegraphics[width=0.48\columnwidth]{figs/optsynzqlvsqltwoloop.pdf}
\end{center}
\vspace{-15pt}
\caption{\zq vs \sql: we want to find top 10 similar products for every product
on varying the number of products from 10---5000.}
\vspace{-13pt}
\label{fig:opt_syn_zqlvssql}
\end{figure}


% Lastly, these queries are extremely verbose and complex, and are error-prone to write,
% compared to a few lines of \zq.


\paragraph{OLAP Browsing.} There has been some work on 
interactive browsing of data cubes~\cite{DBLP:conf/vldb/Sarawagi99,DBLP:conf/vldb/SatheS01}.
%While we may be able to reuse some of the metrics from that line of work,
The work focuses on suggestions for raw aggregates to examine
that are informative given past browsing, or those that show a generalization 
or explanation of a specific cell---an easier problem meriting simpler techniques---not
addressing the full exploration capabilities provided by \zq.

\paragraph{Data Mining Languages:} 
There has been some limited work in data mining query languages, all from the early 90s, 
on association rule mining (DMQL~\cite{han1996dmql}, MSQL~\cite{imielinski2000query}),
or on storing and retrieving models on data
(OLE DB~\cite{netz2001integrating}),
as opposed to a general-purpose visual data exploration language
aimed at identifying visual trends. 

\paragraph{Visualization Suggestion Tools:}
There has been some recent work on building systems that 
suggest visualizations.
Voyager~\cite{wongvoyager2015} recommends
visualizations based on aesthetic properties of the
visualizations, as opposed to queries.
SeeDB~\cite{Vartak:2015:SED:2831360.2831371} 
recommends visualizations that best display the 
difference between two sets of data. 
SeeDB and Voyager can be seen to be special cases of \zv.
The optimization techniques outlined are a substantial generalization
of the techniques described in SeeDB; 
while the techniques in SeeDB are special-cased to one setting 
(a simple comparison),
here, our goal is to support and optimize all \zq queries.






% but require extensive knowledge of the underlying 
% algorithms and para\-metrization, limiting their use to experts,
% e.g., a user can select to use decision trees, with a certain depth
% and splitting criteria
% on their dataset using any of these tools. 
% Programming libraries such as Weka~\cite{holmes1994weka}
% and Scikit-learn~\cite{pedregosa2011scikit} support embedding machine
% learning and data mining within programs. 
% The statistical analysis and programming libraries falls under
% the third box of Figure~\ref{fig:steps}, representing the
% third stage of data analytics, following data exploration.
% Indeed, complex statistical tasks\techreport{ such as classification,
% regression, and dimensionality reduction}
% are hugely important, but 
% require programming ability or an understanding of machine learning
% and statistics to be useful.
% \zv is instead intended to be a substantial generalization over existing
% data exploration tools (second box in Figure~\ref{fig:steps}),
% aimed at identifying the patterns and trends that hold in the data,
% by non-programmers,
% which can be then used to train a machine learning algorithm later on.



% \tar{One can express a portion of ZQL queries by writing multiple SQL queries (procedural SQL) with complex constructs such as common table expressions (CTE), nested queries, and window (analytic) functions. However:
% --Only a handful of databases support these options. Still, most of the databases don't optimize these constructs, and so execution of these tend to take a lot of time. For instance, postgres materializes intermediate results while executing CTEs.
% --Hacky and cumbersome to write. Only users who are extremely proficient in SQL can write these. Will take many more lines than equivalent ZQL queries. During our user-study, almost all participants who had experience with SQL weren't aware of these options - all clearly said SQL can't express ZQL type of queries. Participants were more comfortable using python than SQL.
% }




% A number of interactive data analytics tools have been developed, such as ShowMe, Spotfire, and Tableau~\cite{DBLP:journals/cacm/StolteTH08, polaris,DBLP:journals/tvcg/MackinlayHS07,Ahlberg:1996:SIE:245882.245893}. While these tools are effective in retrieval, and automatic visualization mapping and encoding of the data the user specifies in a query, these tools do not let the users specify the kind of patterns or insights that they are looking for, and thus the onus is on the users to manually search for interesting visualizations among all possible output visualizations.

%\paragraph{OLAP:} \tar{got rid of the first line}
% Visual specification tools have also been introduced by the database community\cite{DBLP:conf/sigmod/GonzalezHJLMSSG10,DBLP:conf/sigmod/KeyHPA12,DBLP:conf/sigmod/LivnyRBCDLMW97,DBLP:conf/avi/KandelPPHH12}.
% %, including Fusion Tables~\cite{DBLP:conf/sigmod/GonzalezHJLMSSG10},
% VizDeck~\cite{DBLP:conf/sigmod/KeyHPA12}, 
% and Devise~\cite{DBLP:conf/sigmod/LivnyRBCDLMW97}.
% Profiler~\cite{DBLP:conf/avi/KandelPPHH12} and Potter's Wheel~\cite{DBLP:conf/vldb/RamanH01}
% identifies mistakes in datasets. 







% although support a number of complex data mining tasks,
% require extensive knowledge of the algorithms and their parameterizations making them limited only to expert users. Moreover, these tools are loosely coupled with databases, 
% providing limited opportunities for query optimizations, and do not scale well as size of 
% the data increases. \zv, unlike these tools, is not meant for complex data analytics tasks such
% as classification, prediction, regression, or dimensionality reduction, but 
% augments the functionalities of existing visual data exploration tools such as Tableau and ggplot by 
% automatically identifying interesting visualizations that match user specified insights.

% \paragraph{Programming libraries:} Several programming and workflow languages support machine
% learning (e.g, WEKA) and visualization libraries using which users can write imperative code to retreive data, and search for visualizations
% with specific insights. However, most of the users, especially  with less programming experience, only know the insights that they are interested in
% instead of the actual procedure to find them. \zv is more accessible to such users as it exposes a more general purpose declarative interface for describing
% their insights. Moreover, as the path of data exploration is mostly unpredictable and iterative, zenvisage leads to a faster data exploration
% since the users can rapidly modify their queries as their desired insights change.



\techreport{
\paragraph{Multi-Query Optimization:} 
There has been a lot of work on Multi-Query Optimization (MQO), both classic, 
e.g.,~\cite{DBLP:journals/tods/Sellis88,shim1994improvements,roy2000efficient}, and
recent work, e.g.,~\cite{giannikis2013workload,psaroudakis2013sharing,DBLP:journals/pvldb/KementsietsidisNCV08,giannikis2014shared}.
Overall, the approach adopted is to batch queries, decompose into operators,
and build ``meta''-query plans that process multiple queries at once,
with sharing at the level of scans, or at the level of higher level operators
(either via simultaneous pipelining or a true global query plan~\cite{psaroudakis2013sharing}).
Unlike these techniques which require significant modifications
to the underlying database engine---indeed, some of these systems do not even provide
full cost-based optimization and only support hand-tuned plans~\cite{giannikis2013workload}, 
in this paper, we adopted two 
syntactic rewriting techniques that operate outside of any relational database as a backend
without 
requiring any modification, and can thus seamlessly leverage improvements
to the database. 
Our third optimization is tailored to the \zq setting and does not apply more broadly.
}



% \papertext{
% \smallskip
% \noindent In our technical report, we describe other areas of related work,
% including multi-query optimization, anomaly discovery, and time-series similarity indexing~\cite{techreport}.
% }


\techreport{
\paragraph{Anomaly Discovery:}
Anomaly detection is a well-studied topic~\cite{Chandola:2009:ADS:1541880.1541882,Agyemang:2006:CSN:1609942.1609946,Patcha:2007:OAD:1269989.1270131}. Our goal in that
\zv is expected to be interactive, especially on large datasets; most work in anomaly detection focuses on accuracy at the cost of latency and is typically a batch operation. In our case, since interactiveness is of 
the essence, and requests can come at any time, the emphasis is on scalable on-the-fly data processing aspects.
}

\techreport{
\paragraph{Time Series Similarity and Indexing:}
There has been some work on indexing of
of time series data, e.g., \cite{timeseries1,timeseries2,timeseries3,timeseries4,timeseries5,
timeseries6,timeseries7};
for the attributes that are queried frequently, 
we plan to reuse these techniques for similarity search.
For other attributes, indexing and maintaining all trends
is impossible, since the number of trends grows exponentially with the
number of indexed attributes.
}


\later{
\tar{Previous text}
Our work is related to prior work in a number of areas.
\paragraph{Visual Analytics Tools:} A number of 
interactive data analytics tools have been developed, such as ShowMe, Polaris, Spotfire, and
Tableau~\cite{DBLP:journals/cacm/StolteTH08, DBLP:journals/tvcg/MackinlayHS07,Ahlberg:1996:SIE:245882.245893}.
Here, the onus is on the user to specify the visualization to be generated,
and then repeat until the desired visualization is identified. 
Similar specification tools have also been introduced
by the database community\cite{DBLP:conf/sigmod/GonzalezHJLMSSG10,DBLP:conf/sigmod/KeyHPA12,DBLP:conf/sigmod/LivnyRBCDLMW97,DBLP:conf/avi/KandelPPHH12}.
%%, including Fusion Tables~\cite{DBLP:conf/sigmod/GonzalezHJLMSSG10},
%VizDeck~\cite{DBLP:conf/sigmod/KeyHPA12}, 
%and Devise~\cite{DBLP:conf/sigmod/LivnyRBCDLMW97}.
%Profiler~\cite{DBLP:conf/avi/KandelPPHH12} and Potter's Wheel~\cite{DBLP:conf/vldb/RamanH01}
%identifies mistakes in datasets. 
\zv is related to work on browsing data cubes---finding explanations, getting suggestions for cubes to visit,
or identifying generalizations from a single cube, 
e.g., \cite{DBLP:conf/vldb/Sarawagi99, 
DBLP:conf/vldb/SatheS01}. 
While we may be able to reuse the metrics from that line of work,
the same techniques will not directly apply to the identification of
trends, patterns, and anomalies for interactive visualizations.  
Specifically, no OLAP techniques capture the interestingness of slices
of the data cube that can be visualized via two (or more) dimensional
visualizations: instead, the focus is more on 
raw aggregate data, on interestingness of cells of the cube, 
displayed within multidimensional tables.  
Thus, the emphases are different. 
Indeed, we believe that, given the popularity of tools like Tableau,
users prefer to browse and explore data visually rather than in tables.
% \new{Moreover, while the OLAP applications try to model relational data as multidimensional cubes for analysis, our system searches for insights among multiple two-dimensional visualizations that are created from one or more attributes of a relational data.}
There has been some work sketch-based interfaces for visualization~\cite{visual1, visual2,visual3, visual4}: however, none of this work addresses
the deep scalability issues at play if these techniques are to be applied on large
datasets.
There is limited work on visual query interfaces~\cite{gesturedb, dbtouch, qbe, dataplay}, but the goal there is to make it easier to pose SQL queries.

% Furthermore, unlike traditional anomaly detection, our anomaly identification goals are query-driven,
% and not one-shot
% scientists can explore the same dataset or multiple datasets in many different
% ways, and we need to be able to surface interesting insights or anomalies
% in real-time whenever a query is issued.
\paragraph{Time Series Similarity and Indexing:}
There has been some work on indexing of
of time series data, e.g., \cite{timeseries1,timeseries2,timeseries3,timeseries4,timeseries5,
timeseries6,timeseries7};
for the attributes that are queried frequently, 
we plan to reuse these techniques for similarity search.
For other attributes, indexing and maintaining all trends
is impossible, since the number of trends grows exponentially with the
number of indexed attributes.
\paragraph{Visualization Recommendation Tools:}
There has been some recent work on building systems that 
recommend visualizations
to analysts.
Voyager~\cite{wongvoyager2015} recommends
visualizations based on aesthetic properties of the
visualizations, as opposed to user queries.
SeeDB~\cite{tr} 
recommends visualizations that best display the 
difference between two sets of data. 
Both SeeDB and Voyager can be seen to be special cases of \zv,
since \zv can be viewed to be a user-driven visualization
recommendation engine as well.
The optimization techniques outlined are a generalization
of the techniques described in SeeDB; 
while the techniques in SeeDB are special-cased to the 
query considered (a simple comparative query),
here, our goal is to support and optimize all \zq queries.
\paragraph{Data Mining Query Languages:}
There has been some limited work in the space of data mining query languages, all from the early 90s, 
focusing
either on
association rule mining (languages such as DMQL~\cite{han1996dmql} and MSQL~\cite{imielinski2000query}),
or on storing and retrieving models on data
(languages such as OLE DB~\cite{netz2001integrating}),
as opposed to a visual exploration language like \zq.
\paragraph{Multi-Query Optimization:} 
Our proposed primitive query optimization techniques are currently 
tailored to the \zv setting, specifically trying to identify 
opportunities for batching of 
individual visualization generation \sql queries 
so as to ``combine work''
as much as possible, even before they are issued to the database. 
These optimization techniques are external to the database system,
are completely interpreted based on the rows of \zq, and do not 
require any modifications to the underlying database.
Indeed, there are very powerful multi-query optimization techniques
in related work, e.g.,~\cite{DBLP:journals/tods/Sellis88,shim1994improvements,dalvi2003pipelining,roy2000efficient,DBLP:journals/pvldb/KementsietsidisNCV08},
that we plan to leverage as future work to further improve the performance
of \zv, when we attempt to holistically
process \zq queries by making modifications to the underlying database
query processor, rather than simply operating as a ``wrapper'' on top
of a database.
}%techreport

% \new {Our proposed query optimization technqiues fall into the cateogory of Multi-Query Optimization techniques ~\cite{DBLP:journals/tods/Sellis88} with a similar goal of batching multiple SQL queries to reduce the overall number of queries that the system needs to execute. However, our query optimization technqiues are specific to the semantics of \zq query model, and analyze visualisation and process column attributes for optimization opportunities rather than the SQL constructs.}


