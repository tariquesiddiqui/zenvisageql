%!TEX root=zenvisageql.tex




\paragraph{Inter-Task Optimization.}
%Our final optimization is less straightforward than the previous methods.
It is not generally not possible
to batch \sql queries across tasks, since
subsequent tasks may depend on the outputs of previous tasks.
However, for more sophisticated \zq queries, it may be possible 
to identify batching opportunities across tasks.
For instance, a visual component defined after a task may be
independent of the task. 
Formally, if visual component $V$ is defined in a
row later than the row task $T$ is defined in, 
and no column of $V$ depends on the
output of $T$, then $V$ is independent of $T$. The advantage of 
independence is that now we can batch $V$ into an earlier request because we do
not need to wait for the results of $T$.
Thus, in Table~\ref{tab:exec-1}, we can batch the \sql queries for the first and second
rows into a single request since the visual component in the second row is independent of
the task in the first row.


% \papertext{Since the visualizations from subsequent rows may depend on
% the output values of previous tasks, so it is not possible to batch
% queries across tasks}
% \techreport{As mentioned previously, it is generally not possible to batch \sql queries
% across tasks}.
% However, sometimes a visual component defined after a task may be
% independent of the task. 
% \papertext{Nothing prevents the user from combining 
% many non-related visual queries into a single \zq query with many output rows.
% Particularly for the case of exploratory visual analysis, the user may want to submit
% one \zq query which contains many different tasks to explore interesting trends,
% representative sets, and outliers in an unknown dataset.}

% \techreport{More formally, if visual component $V$ is defined in a
% row later than the row task $T$ is defined in, and no column of $V$ depends on the
% output of $T$, then $V$ is independent of $T$. The advantage of 
% independence is that now we can batch $V$ into an earlier request because we do
% not need to wait for the results of $T$.} 
% More concretely, in Table~\ref{tab:exec-1}, we can batch the \sql queries for the first and second
% rows into a single request since the visual component in the second row is independent of
% the task in the first row.

To determine all cases for which this independence can be taken advantage of,
the \zq compiler builds a query tree for the \zq queries it
parses. All axis variables, name variables, and tasks of a \zq query are 
nodes in its query tree. Name variables become the parents of the axis variables
in its visual component. Tasks become the parents of the visualizations it
operates over. Axis variables become the parents over the nodes which are used
in its declaration, which may be either tasks nodes or other axis variable
nodes.
The query tree for Table~\ref{tab:exec-1} is given in
Figure~\ref{fig:query-tree}. Here, the children point to their parents, and the
tasks in rows 1 and 2 are labeled {\tt t1} and {\tt t2} respectively. As
we can clearly see from the tree, the visual component for {\tt f2}
is independent of {\tt t1}.

\begin{figure}[!]
  \centering
  \vspace{-10pt}
  \includegraphics[scale=0.3]{figs/querytree1.pdf}
    \vspace{-10pt}
  \caption{The query tree for the \zq query in Table~\ref{tab:exec-1}.}
  \vspace{-15pt}
  \label{fig:query-tree}
\end{figure}

The \zv execution engine uses the query tree to determine which \sql queries to
batch in which step. At a high level, all \sql queries for name variable nodes
whose children have all been satisfied or completed can be batched into a single request.
More
specifically, the execution engine starts out by coloring all leaf nodes in the
query tree. Then, the engine repeats the following until the entire
query tree has been colored:
\begin{inparaenum}[i)]
  \item Batch the \sql queries for name variable nodes, who have all their
    children colored, into a single request and submit to the database.
  \item Once a response is received for the request, color all name variables
    nodes whose \sql queries were just submitted.
  \item Until no longer possible, color all axis variable and task nodes whose
    children are all colored. If a task is about to be colored, run the task
    first.
\end{inparaenum}
This execution plan ensures the maximal amount of batching is done while still
respecting dependencies. Parts of the execution plan may also be parallelized
(such as simultaneously running two independent, computation-expensive tasks
like representative sets), to improve overall throughput.



\techreport{While this optimization may seem like overkill for the short \zq examples
presented in this paper, nothing prevents the user from combining many non-related visual
queries into a single \zq query with many output rows. Particularly for the
case of exploratory visual analysis, the user may want to submit
one \zq query which contains many different tasks to explore interesting trends,
representative sets, and outliers in an unknown dataset.
}
