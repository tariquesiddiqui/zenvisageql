%!TEX root=zenvisageql.tex

\subsection{\VzAlg Operators}
\label{sec:viz-exp-oper}

Similar to how operators in
ordered bag algebra operate on and result in ordered bags, 
operators in \vzalg operate on and result in \vzgrps. 
Many of the symbols for operators in \vzalg are also derived from
relational algebra, with some differences. 
To differentiate, operators in \vzalg are superscripted
with a $v$ (e.g., $\vzsel$, $\vzsort$).
The unary operators for \vzalg include 
\begin{inparaenum}[\itshape (i)\upshape]
\item $\vzsel$ for selection, \item $\vzsort$ for
sorting a \vzgrp based on the trend-estimating function $T$, \item $\vzlimit$ for
limiting the number of \vzchns in a \vzgrp, and \item $\vzdedup$ for duplicate \vzchn removal.
\end{inparaenum}. 
The binary operators include 
\begin{inparaenum}[\itshape (i)\upshape]
\item  $\vzunion$ for union, \item $\vzdiff$ for
difference, \item $\vzswap$ for replacing the attribute values of the \vzchns in one \vzgrp's with
another's, \item $\vzdist$ to reorder the first \vzgrp based on the \vzchns'
distances to the \vzchns of another \vzgrp based on metric $D$, and \item $\vzfind$
to reorder the \vzchns in a \vzgrp based on their distance to a reference
\vzchn from a singleton \vzgrp based on $D$. 
\end{inparaenum}
These operators are described below, and listed
in Table~\ref{tab:vizalgebopt}.

\begin{table*}[!t]
  \vspace{0pt}
\scriptsize
    \centering
\begin{tabular}{|c|l|l|p{5cm}|l|}      \hline
       Operator & Name & Derived from Bag Algebra  & Meaning & Unary/Binary \\ \hline \hline
       $\vzsel$ & Selection & Yes  & Subselects \vzchns & Unary \\ \hline
       $\vzsort$ & Sort & No & Sorts \vzchns in increasing order & Unary \\ \hline
       $\vzlimit$ & Limit & Yes & Returns first k \vzchns & Unary \\ \hline 
       $\vzdedup$ & Dedup & Yes  & Removes duplicate \vzchns & Unary \\ \hline 
       $\vzunion/\vzdiff/\vzint$ & Union/Diff/Int & Yes & Returns the union
  of/differences between/intersection of two \vzgrps & Binary \\ \hline 
       $\vzswap$ & Swap & No & Returns a \vzgrp in which values of an attribute in  one \vzgrp is  replaced  with values of the same attribute in another \vzgrp & Binary \\ \hline 
       $\vzdist$ & Dist & No & Sorts a \vzgrp based on pairwise distance to another \vzgrp  & Binary \\ \hline 	
       $\vzfind$ & Find & No & Sorts a \vzgrp in increasing order based on their distances to a single reference \vzchn & Binary \\ \hline  
   \end{tabular}
   \caption{Visual Exploration Algebra Operators}
  \label{tab:vizalgebopt}
\end{table*}




\subsubsection{Unary Operators.}
\paragraph{$\vzsel_{\theta}(V)$:}
$\vzsel$ selects a \vzgrp from $V$ based on selection criteria
$\theta$, like ordered bag algebra. 
However, $\vzsel$ has a
more restricted $\theta$; while $\vee$ and $\wedge$ may still be used, 
only the binary comparison operators $=$ and
$\ne$ are allowed. 
As an example, \\
$\vzsel_\theta (\calV)$ where $\theta = ($X=`year' $\land$ Y=`sales' $\land$ year=\wild $\land$ month=\wild $\land$ product $\ne$ \wild $\land$ location=`US' $\land$ sales=\wild $\land$ profit=\wild$)$ from Table~\ref{subtab:vzsel} 
on $\calV$ from
Table~\ref{tab:setup} would result in the \vzgrp of time 
vs. sales visualizations for different products in the US.
% \alkim{The reason we have this restriction is because \zq does not support the
%   other comparisons. Also, we would have to redefine the comparison operators
%   to handle the semantics for what happens with \wild, which seemed annoying
% and long to do.}

In this example, note that the product is
specifically set to not equal \wild so that the resulting \vzgrp will include
all products.
On the other hand, the location is explicitly set to be equal to US. 
The other attributes, e.g., sales, profit, year, month are set to equal \wild: 
this implies that the visual groups are not employing any additional constraints
on those attributes. 
(This may be useful, for example when those attributes are not
relevant for the current visualization or set of visualizations.)
As mentioned before, \vzgrps have the semantics of ordered bags. Thus, $\vzsel$
operates on one tuple at a time in the order they appear in
$V$, and the result is in the same order the tuples are operated on. 

\paragraph{$\vzsort_{F(T)}(V)$:}
$\vzsort$ returns the \vzgrp sorted in an increasing order based on applying $F(T)$ on
each \vzchn in $V$, where $F(T)$ is a procedure that uses  
function $T$.  For example, $\vzsort_{-T}(V)$ might return the
visualizations in $V$ sorted in decreasing order of estimated slope.
This operator is not present in the ordered bag semantics,
but may be relevant when we want to reorder the ordered bag using a different criterion.
The function $F$ may be any higher-order function with no side effects. For a language to
\vzcmp, the language must be able to support any arbitrary $F$.

\paragraph{$\vzlimit_{k}(V)$:}
$\vzlimit$ returns the first $k$ \vzchns of $V$ ordered in the same way they
were in $V$. $\vzlimit$ is equivalent to the \texttt{LIMIT} statement in \sql.
$\vzlimit$ is often used in conjunction with $\vzsort$ to retrieve the top-$k$
visualizations with greatest increasing trends (e.g. $\vzlimit_{k}(\vzsort_{-T}(V))$).
When instead of a number $k$, the subscript to $\vzlimit$ is actually $[a: b]$, 
then the items of $V$ that are between
positions $a$ and $b$ in $V$ are returned.
Thus $\vzlimit$ offers identical functionality to the $[a:b]$ in ordered bag algebra,
with the convenient functionality of getting the top $k$ results by just having
one number as the subscript.
Instead of using $\vzlimit$, \vzalg also supports the use
of the syntax $V[i]$ to refer to the $i$th \vzchn in $V$, and 
$V[a:b]$ to refer to the ordered bag of \vzchns from positions $a$ to $b$.

\paragraph{$\vzdedup(V):$}
$\vzdedup$ returns the \vzchns in $V$ with the duplicates removed, in the order
of their first appearance.
Thus, $\vzdedup$ is defined identically to ordered bag algebra.

\subsubsection{Binary Operators.}
\paragraph{$V \vzunion U$ | $V \vzdiff U$ | $V \vzint U$:}
Returns the union / difference / intersection of $V$ and $U$.
These operations are just like the corresponding operations in
ordered bag algebra.


\paragraph{$\vzswap_{A}(V, U)$:}
$\vzswap$ returns a \vzgrp in which values of attribute
$A$ in $V$ are replaced with
the values of $A$ in $U$. 
Formally, assuming $A_i$ is the $i$th attribute of $V$
and $V$ has $n$ total attributes:
$\vzswap_{A_i}(V, U) = \pi_{A_1,\dots, A_{i-1}, A_{i+1}, \dots, A_{n}}(V) \times \pi_{A_i}(U)$. This can be useful for when the user would like to change
an axis: $\beta_{\text{X}}(V, \vzsel_{\text{X=year}}(\calV))$ will change the \vzchns in $V$
to have year as their x-axis. $\vzswap$ can also be used to combine multiple
dimensions as well. If we assume that $V$ has multiple Y values, we can do
$\vzswap_{\text{X}}(V, \vzsel_{\text{X}\ne*}(\calV))$ to have the \vzchns in
$V$ vary over both X and Y.
\techreport{This operator allows us to start with a set of visualizations and then ``pivot''
to focus on a different attribute, e.g., start with sales over time
visualizations and pivot to look at profit. 
Thus, the operator allows us to transform the space of \vzchns.}


\paragraph{$\vzdist_{F(D),A_1,...,A_j}(V, U)$:}
$\vzdist$ sorts the \vzchns in $V$ in increasing order based on their
distances to the corresponding \vzchns in $U$. More specifically, $\vzdist$
computes $F(D)(\vzsel_{A_1=a_1 \land ... \land A_j=a_j}(V),\\ \vzsel_{A_1=a_1
  \land ... \land A_j=a_j}(U)) \forall a_1,...,a_j \in
  \pi_{A_1,...,A_j}(V)$ and returns an increasingly sorted $V$ based on the
  results. If $\vzsel_{A_1=a_1 \land ... \land A_j=a_j}$ for either $V$ or $U$
  ever returns a non-singleton \vzgrp for any tuple
  $(a_1,...,a_j)$, the result of the operator is undefined.

\paragraph{$\vzfind_{F(D)}(V, U)$:}
$\vzfind$ sorts the \vzchns in $V$ in increasing order based on their distances
to a single reference \vzchn in singleton \vzgrp $U$. 
Thus, $U = [t]$.
$\vzfind$ computes $F(D)(V[i],
U[1]) \quad$ $\forall i \in \{1,\dots,|V|\}$, and returns a reordered $V$ based
on these values, where $F(D)$ is a procedure that uses $D$. 
If $U$ has
more than one \vzchn, the operation is undefined. 
$\vzfind$ is useful for
queries in which the user would like to find the top-$k$ most similar
visualizations to a reference: $\vzlimit_{k}(\vzfind_{D}(V,U))$,
where $V$ is the set of candidates and $U$ contains the reference.
Once again, this operator is similar to $\vzsort$, except that
it operates on the results of the comparison of individual \vzchns to a specific \vzchn.

\begin{table}[!]
  \scriptsize
    \centering
    \begin{tabular}{|c|c|c|c|c|c|c|c|}
      \hline
      X & Y & year & month & product & location & sales & profit \\ \hline
      year & sales & \wild & \wild & chair & US & \wild & \wild \\ \hline
      year & sales & \wild & \wild & table & US & \wild & \wild \\ \hline
      year & sales & \wild & \wild & stapler & US & \wild & \wild \\ \hline
      year & sales & \wild & \wild & printer & US & \wild & \wild \\
      \multicolumn{8}{|c|}{\vdots} \\
    \end{tabular}
  \caption{Results of performing unary operators on $\calV$ from Table~\ref{tab:setup}: $\vzsel_\theta (\calV)$ where $\theta = ($X=`year' $\land$ Y=`sales' $\land$ year=\wild $\land$ month=\wild $\land$ product $\ne$ \wild $\land$location=`US' $\land$ sales=\wild $\land$ profit=\wild$)$}.
  \label{tab:viz-un-opers}    \label{subtab:vzsel}
  \vspace{-10pt}
\end{table}

