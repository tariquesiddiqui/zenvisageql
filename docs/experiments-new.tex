%!TEX root=zenvisageql.tex

\vspace{-4pt}
\section{Experimental Study}
\label{sec:experiments}
\vspace{0pt}

In this section, we evaluate the runtime performance of the \zq engine.
We present the runtimes for executing both synthetic and realistic \zq queries
and show that we gain speedups of up to 3$\times$
with the optimizations from Section~\ref{subsec:query_execution}.
We also varied the characteristics of a synthetic \zq query to observe
their impact on our optimizations. Finally, we show that disk I/O
was a major bottleneck for the \zq engine, and if we switched our back-end
database to a column-oriented database and cache the dataset in memory, we
can
achieve interactive run times for datasets as large as 1.5GB.

%In this section, we evaluate the impact of the query optimizations (described in Section~\ref{subsec:query_execution}) on
%both synthetic and real datasets. We show the impact of following parameters on query optimizations:
%number of visualizations, complexity of \zq queries, complexity of task 
%processors, and the size of the datasets. Orthogonal to our query optimization evaluation, 
%we explain that disk IO is the major bottleneck is making zenvisage interactive as the size 
%of dataseset increases, and show that \zv is capable of providing interactive response times 
%on vertica database when allocated with a large memory for it to cache all rows.  

\paragraph{Setup.}
All experiments were conducted on a 64-bit Linux server
with 8 3.40GHz Intel Xeon E3-1240 4-core processors and 8GB of
1600 MHz DDR3 main memory. We used 
PostgreSQL with working memory size set to 512 MB and shared buffer
size set to 256MB for the majority of the experiments; the last set of 
experiments demonstrating interactive run times additionally 
used Vertica Community Edition with a
working memory size of 7.5GB.

\techreport{
\paragraph{PostgreSQL Modeling.}
For modeling the performance
on issuing multiple parallel queries with varying number of group by values, 
we varied the 
number of parallel queries issued (\#Q) from 1 to 100,
and the group by values per query (\#V) from 10 to 100000, 
and recorded the response times (T).
We observed that the time taken for a batch of queries was 
practically linearly dependent 
to both the number of queries as 
well as the group by values. 
Fitting a linear equation by performing 
multiple regression over the observed data, 
we derived the following cost-model,
$$ T(ms)= 908\times(\#Q) + 1.22\times\frac{(\#V)}{100} + 1635$$
As per the above model, adding a thread leads to the same rise in response time as increasing the number 
of group by values by 75000 over the existing threads in the batch. In other words, it is better to merge queries 
with small number of group by values. Moreover, since there is a fixed cost (1635 ms) associated with
every batch of queries, we tried to minimize the number of batches by packing as many queries as possible 
within the memory constraints.
}




\paragraph{Optimizations.} The four versions of the \zq engine we use are:
\begingroup
\plitemsep=.2em
\pltopsep=.2em
\begin{inparaenum}[(i)]
\item \noopt: The basic translation from Section~\ref{subsec:query_execution}.
\item \para: Concurrent \sql queries for independent nodes from Section~\ref{sec:para}.
\item \spec: Speculating and pre-emptively issuing \sql queries from Section~\ref{sec:spec}.
\item \fuse: Query combination with speculation from Section~\ref{sec:comb}. %\emph{Final version.}
\end{inparaenum}
\endgroup
\noindent In our experiments, we consider \noopt and the MQO-dependent \para to be our
baselines, while \spec and \fuse were considered to be completely novel optimizations.
For certain experiments later on, we also evaluate the performance of the caching optimizations 
from Section~\ref{sec:cache} on \fuse.
 
\begin{table*}
\vspace{-5pt}
\small
%  \vspace{0pt}
%\scriptsize
  \scalebox{0.90}{
%  \begin{adjustbox}{center}
    \begin{tabular}{|C{1pt}|L{220pt}|C{33pt}|C{33pt}|C{30pt}|C{30pt}|C{30pt}|C{40pt}|C{48pt}|}
      \hline
      & Query Description & \# \cnode[s] & \# \pnode[s] & \# $T$ & \# $D$ & \#
      Visualizations & \# \sql Queries: \noopt & \# \sql Queries: \fuse \\
      \hline
      1 & Plot the related visualizations for airports which have a correlation
      between arrival delay and traveled distances for flights arriving there.
     % 
     % average delays and traveled distances for flights going to
     % airports which have a correlation between arrival delay and traveled
     % distance for flights arriving at that airport.
      & 6 & 3 & 670 & 93,000 & 18,642 & 6 & 1 \\ \hline
      2 & Plot the delays for carriers whose delays have gone up at airports
      whose average delays have gone down over the years.
      & 5 & 4 & 1,000 & 0 & 11,608 & 4 & 1 \\ \hline
      3 & Plot the delays for the outlier years, outlier airports, and outlier
      carriers with respect to delays.
      & 12 & 3 & 0 & 94,025 & 4,358 & 8 & 2 \\ \hline
    \end{tabular}
%  \end{adjustbox}
}
%
%\begin{tabular}{|p{1cm}|p{10cm}|p{2cm}|p{2cm}|p{2cm}|}      \hline
%      \# & Query & \#taskprocessors & \# sql queries (avg groupby vals) before smartfuse & \# sql queries (avg groupby vals) after smartfuse \\ \hline	
%       1 & find all destination airports where the increase in average arrival delays over the years is correlated with increase in the distance travelled by fights arriving there, and not because of the increase in flights' departure delays. For these airports, plot average arrival delays, departure delays and distance by flights arriving there over the years.   & 3 (T, D and T) & 6 (3.2k)	& 1 (24k) \\ \hline																			
%       2 & find pairs of origin and destination airports between which the departure and arrival delays of flights have decreased over the years, for these pairs of airports find all flight carriers that show the opposite behaviour, i.e, their delays have gone up over the years, and plot their delays over years.  & 4 (T, T, T and T) & 4 (3k)	& 1 (38k) \\ \hline	
%       3 & a multi-step drill down query where we first find 5 years where the  average delay patterns were most different, over those five years find the top 5 airports with most different average delay patterns, and finally find the carriers that show most different average delay patterns with destination as these airports over the 5 years. We plot the average arrival delays over the months as well as destination airports for these flights over the 5 years  & 3 (D, D and D) & 8 (300) &  2 (32k) \\ \hline		
%				     	
%\end{tabular}
   \vspace{-5pt}
   \caption{Realistic queries for the airline dataset with the \# of
   \cnode[s], \# of \pnode[s], \# of $T$ functions calculated, \# of $D$ functions
 calculated, \# of visualizations explored, \# of \sql queries issued with
 \noopt, and \# of \sql queries issued with \fuse per query.}
  \label{tab:realworldqueries}
  \vspace{-15pt}
\end{table*}

\noindent
%\textbf{Modeling} \tar{Will wait for Alkim to finish optimization section, we just need to say what values we set for constants, do we also need to talk how we did the modeling?}


%For the first two experiments, we load the data in postgresql without any indexing.
\begin{figure}[!h]
\vspace{-10pt}
\begin{center}
  \begin{subfigure}[c]{0.48\columnwidth}
    \includegraphics[width=\textwidth]{figs/optreal20m.pdf}
  \end{subfigure}
  \begin{subfigure}[c]{0.48\columnwidth}
    \includegraphics[width=\textwidth]{figs/chain.pdf}
  \end{subfigure}
\end{center}
\vspace{-20pt}
\caption{Runtimes for queries on real dataset (left) and single chain synthetic query (right)}
\vspace{-8pt}
\label{fig:optrealnviz}
\end{figure}



\begin{figure}[!h]
\vspace{-5pt}
\begin{center}
\includegraphics[width=0.48\columnwidth]{figs/optsynparamviz.pdf}
%\includegraphics[width=0.48\columnwidth]{figs/optsynparamtp.pdf}
\includegraphics[width=0.48\columnwidth]{figs/optsynparamnodes.pdf}
\end{center}
\vspace{-20pt}
\caption{Effect of number of  visualizations (left) and length
of the chain (right) on the overall runtimes.}
\vspace{-15pt}
\label{fig:optsynviz}
\end{figure}

\vspace{2pt}
\subsection{Realistic Queries}
\vspace{-3pt}

For our realistic queries, we used 20M rows of a real 1.5GB airline
dataset~\cite{airlinedata} which contained the details of flights within the
USA from 1987-2008, with 11 attributes. On
this dataset, we performed 3 realistic \zq queries inspired by the case
studies in our introduction. Descriptions of the queries can be found in Table~\ref{tab:realworldqueries}.

Figure~\ref{fig:optrealnviz} (left) depicts the runtime performance of the
three realistic \zq queries, for each of the optimizations. 
For all queries, each level of optimization provided a substantial speedup
in execution time compared to the previous level.
{\em Simply by going from \noopt to \para, we saw a 45\% reduction in runtime. From
\para to \spec and \spec to \fuse, we saw 15-20\% reductions in
runtime}.
A large reason for why the optimizations were so effective was because \zq
runtimes are heavily dominated by the execution time of the issued \sql
queries. In fact, we found that for these three queries, 94-98\% of the overall
runtime could be contributed to the \sql execution time. We can see from
Table~\ref{tab:realworldqueries}, \fuse always managed to lower the number of
\sql queries to 1 or 2 after our optimizations, thereby heavily impacting the
overall runtime performance of these queries.

\vspace{-2pt}
\subsection{Varying Characteristics of {\large \zq} Queries}
\vspace{-2pt}

We were interested in evaluating the efficacy of our optimizations with respect
to four different characteristics of a \zq query:
\begin{inparaenum}[(i)]
\item the number of visualizations to explore,
\item the complexity of the \zq query,
\item the level of interconnectivity within the \zq query, and
\item the complexity of the processes.
\end{inparaenum} 
To control for all variables except these characteristics, we used a synthetic
chain-based \zq query to conduct these experiments. Every row of the
chain-based \zq query specified a collection of visualizations based on the
results of the process from the previous row, and every process was applied on
the collection of visualizations from the same row. Therefore, when we created
the query plan for this \zq query, it had the chain-like structure depicted by
Figure~\ref{fig:optrealnviz} (right).
Using the chain-based \zq query, we could then
\begin{inparaenum}[(i)]
\item vary the number of visualizations explored,
\item use the length of the chain as a measure of complexity,
\item introduce additional independent chains to decrease
  interconnectivity, and
\item increase the number of loops in a \pnode[] to control the complexity of
  processes.
\end{inparaenum}

\smallskip
\noindent To study these characteristics, we used a 
synthetic dataset with 10M rows and 15 
attributes (10 dimensional and 5 measure) 
with cardinalities of dimensional attributes varying from 10 to 10,000. 
By default, we set the input number of visualizations per chain to be 100,
with 10 values for the X attribute, 
number of \cnode[s] per chain as 5, the process as $T$ (with a single for loop)
 with a selectivity of .50, and  number of chains as 1.

\paragraph{Impact of number of visualizations.} 
Figure \ref{fig:optsynviz} (left) shows the performance of \noopt, \spec, and
\fuse on our chain-based \zq query as we increased the number of visualizations
that the query operated on. The number of visualizations was increased by
specifying larger collections of Z column values in the first \cnode[]. We
chose to omit \para here since 
it performs identically to \noopt.
With the increase in visualizations, the overall response time increased for
all versions
because the amount of processing per \sql query increased. \fuse showed better
performance than \spec up to
10k visualizations due to reduction in the total number of \sql queries issued.
However, at 10k visualization, we reached the threshold of the number of unique
group-by values per combined query (100k for PostgreSQL), so it was less optimal
to merge queries. At that point, \fuse behaved similarly to \spec.

\begin{figure}[!h]
\vspace{-10pt}
\begin{center}
%\includegraphics[width=0.48\columnwidth]{figs/optsynparamnodes.pdf}
\includegraphics[width=0.48\columnwidth]{figs/optsynparamchains.pdf}
\includegraphics[width=0.48\columnwidth]{figs/optsynparamtp.pdf}
\end{center}
\vspace{-15pt}
\caption{Effect of number of independent chains (left) and the number of loops
in a \pnode[] (right) on the overall runtimes.}
\vspace{-10pt}
\label{fig:opt_syn_nodes}
\end{figure}



\paragraph{Impact of the length of the chain.}
We varied the length of the chain in the query plan (or the number of rows in
the \zq query) to simulate a change in the complexity of the \zq query and
plotted the results in Figure~\ref{fig:optsynviz} (right). As the number of
nodes in the query plan grew, the overall runtimes for the different
optimizations also grew. However, while the runtimes for both \noopt and \spec
grew at least linearly, the runtime for \fuse grew sublinearly due to its query
combining optimization. While the runtime for \noopt was much greater than for \spec,
since the overall runtime is linearly dependent on the number of
\sql queries run in parallel, we see a linear growth for \spec.

\paragraph{Impact of the number of chains.}
We increased the number of independent chains from 1 to 5 to observe the effect
on runtimes of our optimizations; the results are presented in
Figure~\ref{fig:opt_syn_nodes} (left). While \noopt grew linearly as expected, all
\para, \spec, and \fuse were close to constant with respect to the number of
independent chains. We found that while the overall runtime for concurrent \sql
queries did grow linearly with the number of \sql queries issued, they grew much
slower compared to issuing those queries sequentially, thus leading to an
almost flat line in comparison to \noopt. 

\paragraph{Impact of process complexity.}
We increased the complexity of processes by increasing the number of loops in
the first \pnode[] from 1 to 2. For the single loop, the \pnode[] filtered
based on a positive trend via $T$, while for the double loop, the \pnode[] found
the outlier visualizations.
Then, we varied the number of visualizations to
see how that affected the overall runtimes. Figure~\ref{fig:opt_syn_nodes}
(right) shows the results. For this experiment, we compared regular \fuse with
cache-aware \fuse to see how much of a cache-aware execution made.
We observed that there was not much difference
between cache-aware \fuse and regular \fuse below 5k visualizations when all
data could fit in cache.
After 5k visualizations, not all the visualizations could be
fit into the cache the same time, and thus the cache-aware execution of the
\pnode[] had
an improvement of 30-50\% as the number of visualizations increased from 5k to
25k. However, this improvement, while substantial, is only a minor change in 
the overall runtime. 
% However, this improvement was only in the execution of the process, and
% since \sql queries still heavily dominated,  it was a minor change
% in the overall runtime.


 
%For our experiments, we used the following datasets and queries:
%\begin{inparaenum}[(i)]
%\item We used a synthetic dataset with 10M rows and 15 attributes (10 dimensional and 5 measure) with cardinalities of dimensional attributes varying from 10 to 10000. 
%For evaluationg the impact of different parameters, we created a synthetic \zq query consisting one or more chains of visual component and process nodes \tar{should we add a diagram, or say it looks similar to Figure 2 when set to have two chains?}.
%Each chain starts with a collection of visualizations, on which further filtering or transformations are applied by the subsequent nodes in the chain. By default, we set the input number of visualizations per chain to be 100, number of visual component nodes per chain to be 5, task processor to be T (consisting of a single for loop) with selectivity of .50, and  number of chains to be 1. Each visualization consists of 10 X groups. 
%%For depicting the impact of the number of visualizations, we varied the input number of visualizations from 1 to 10k, for complexity of \zq queries, we varied the number of visual component nodes in a chain from 1 to 5 as well as varied the number of chains from 1 to 5, and for complexity of task processors, we showed the performance of task-processors with 1 and 2 loops over varying number of visualizations.
%
%\item A real airline dataset~\cite{airlinedata} with different number of rows: 2M,20M, and 120M. We used 20M as the default number of rows. \tar{why? need to think of a good reason saying 20M reflects the standard dataset size}. We use three 3 realistic queries as depicted in Table ~\ref{tab:realworldqueries}. 
%\later{
%\begin{inparaenum}[(i)]
%\item Query 1: find all destination airports where the increase in average arrival delays over the years is correlated with increase in the distance travelled by fights arriving there, and not because of the increase in flights' departure delays. For these airports, plot average arrival delays, departure delays and distance by flights arriving there over the years. This query has 5 visual component nodes
%\item Query 2: find pairs of origin and destination airports between which the departure and arrival delays of flights have decreased over the years, for these pairs of airports find all flight carriers that show the opposite behaviour, i.e, their delays have gone up over the years, and plot their delays over years.
%\item Query 3: a multi-step drill down query where we first find 5 years where the  average delay patterns were most different, over those five years find the top 5 airports with most different average delay patterns, and finally find the carriers that show most different average delay patterns with destination as these airports over the 5 years. We plot the average arrival delays over the months as well as destination airports for these flights over the 5 years. 
%\end{inparaenum}
%}

%\end{inparaenum}

\vspace{-2pt}
\subsection{Interactivity}
\vspace{-2pt}

The previous figures showed that the overall execution times of \zq queries took
several seconds, even with \fuse, thus perhaps indicating
\zq is not fit for interactive use with large datasets.
However, 
we found that this was primarily due
to the disk-based I/O bottleneck of \sql queries. In
Figure~\ref{fig:optrowcolumstores} (left), we show the \fuse runtimes of the
3 realistic queries from before on varying size subsets of the airline dataset,
with the time that it takes to do a single group-by scan of the dataset. As we
can see, the runtimes of the queries and scan time are \emph{virtually the
same}, indicating that \fuse comes very close to the optimal I/O runtime
(i.e., a ``fundamental limit'' for the system). 

To further test our hypothesis, we ran our \zq engine with Vertica with a large
working memory size to cache the data in memory to avoid expensive disk I/O. The
results, presented in Figure~\ref{fig:optrowcolumstores} (right), showed that
there was a 50$\times$ speedup in using Vertica over PostgreSQL with these
settings. Even with a large dataset of 1.5GB, we were able to achieve
sub-second response times for many queries. Furthermore, for the dataset
with 120M records (11GB, so only 70\% could be cached), we were able to
reduce the overall response times from 100s of seconds to less than 10 seconds.
Thus, once again, \zv returned results in a small multiple of the time 
it took to execute a single group-by query.
\noindent
Overall, \fuse is interactive on moderate sized datasets on PostgreSQL, or on 
large datasets that can be cached in memory and operated on using a columnar database---which 
is standard practice adopted by visual analytics tools~\cite{tableau-inmem}. 
Improving on interactivity is impossible due to fundamental limits to the system;
in the future, we plan to explore returning approximate answers using samples,
since even reading the entire dataset is prohibitively slow. 


% \alkim{For providing
%   interactivity, existing visual analytics tools either cache all the data
%   inmemory, or make use of large inmemory databases for avoiding expensive disk
%   ios, which are orthogonal to the query optimization technique we propose in
% this paper. This is a good sentence, we should have it somewhere.}

% Finally, note that even with a disk-based PostgreSQL and non-interactive
% response times, \zv is still an overall very useful system, since it automates
% the search for patterns. This drastically reduces number of man hours required
% to find visual insights in a dataset, and it is not unreasonable to think that
% a visual analyst would issue many \zq queries and perform some other task while
% waiting for the results.


%A real airline dataset~\cite{airlinedata} with different number of rows: 2M,20M, and 120M. We used 20M as the default number of rows. \tar{why? need to think of a good reason saying 20M reflects the standard dataset size}. We use three 3 realistic queries as depicted in Table ~\ref{tab:realworldqueries}. 
%\alkim{It's still useful even though not interactive cuz it removes manual
%labor}
%results of query optimizations. For demonstrating interativity of our system, we used Vertica with a 
%large memory size of 7.5GB, (and thus having all rows in memory for 20M flight dataset, and having 
%70\% of the rows in memory for 120M large dataset). 



%\noindent
%\textbf{Effect of Query Optimizations}
%Figure \ref{fig:optrealnviz} (left) depicts the improvement in performance for three \zq queries on the real airline 
%dataset due to query optimizations discussed in Section ~\ref{subsec:query_execution}.
%By running multiple queries in parallel (see parallel vs no-opt), we observe speed-ups upto 50\% in the overall time.
%Further increasing the number of parallel queries by speculating results in improvement in the response time (see speculate).
%As depicted in Table\ref{tab:realworldqueries}, smartfuse merges multiple threads that process small number of group values into lesser number of threads with more number
%of group by values. As clear from our model, this helps in reducing a large overhead of 900ms that is incurred on creating a new thread, resulting in 
%an improvment of 15-20\% in the response time over speculate.

%\noindent
%\textbf{Time for data retrievel vs post-processing}
%As depicted in Figure \ref{fig:optrealnviz} right, the SQL execution dominated the overall runtime with the post-processing time taking much less time(<500ms) 
%for all three queries. There are three reasons why taskprocessors take less time.First, computation time heavily depends on the number of cache line misses or memory access, however we found that since a large number of visualizations (10k visualizations of 10 groups each) can be cached in L caches (~8MB in size), performing even complicated operations over cached data takes less than 500ms (a minor fraction of the overall query execution times). Secondly, we have implemented functional primitives using array-based data structures, which are 50-60X faster than hash based implementations. Moreover, since arrays are stored sequentially in memory, it leads to comparatively lesser number of cache line misses. Finally, we make use of block based computions while applying functional primitives which minimize multiple memory access while iterating multiple times on the same set of data. 
%
%
%\noindent
%\textbf{Impact of number of visualizations}
%Figure \ref{fig:optsynviz} (left) shows the performance of no-opt,speculate and smartfuse algorithms on
%synthetic dataset as we increase the number of visualizations that a \zq query operates on.
%With the increase in visualizations, the overall response time increases for all optimization
%since the amount of processing per thread increases. smartfuse shows better performance than speculate upto
%10k visualizations due to reduction in thread overhead. However, at 10k visualization, we reach the threshold of 100k groups (since each visualization consists 
%10 X values) per thread at which point it is less optimal to merge thread, in that scenario smartfuse becomes similar to speculate. 
%



%\noindent
%\textbf{Impact of complexity of task-processors}
%Figure \ref{fig:optsynviz} (right) depicts that functional primitives such as T
%which iterate once over visualizations take maximum of 50ms even for large
%number of visualizations (25k). For task processors that need to iterate twice
%over the visualizations, such as once that need to find top k outlier or
%representative visualizations, we observed that there are not much difference
%between block based and without block based operations until 5k visualizations
%(all data in cache). After 5k visualizations, not all visualizations could be
%cached in L caches at a time, and thus the block based task processors depict
%an improvement of 30-50\% as the number of visualizations increased from 5k to
%25k, though still a minor improvement as compared to the overall query
%execution time.

%\begin{figure}[!h]
%\vspace{0pt}
%\begin{center}
%\includegraphics[width=0.48\columnwidth]{figs/optsynzqlvsqltwoloop.pdf}
%\end{center}
%\vspace{-10pt}
%\caption{Performance of zql vs sql for a query where we want to find top 10 similar products (using euclidean distance) for every product. We vary the number of products from 10 to 5000.}
%\vspace{-15pt}
%\label{fig:opt_syn_zqlvssql}
%\end{figure}


%\noindent
%\textbf{Impact of complexity of ZQL queries}
%For evaluating the impact of complexity of \zq queries on query optimizations, 
%we tried varying the number of sequential visual component nodes in a chain  from 1 to 10 (Figure \ref{fig:opt_syn_nodes} left)
%as well as the number of independent visual component nodes by varying the number of chains from 1 to 5 (Figure \ref{fig:opt_syn_nodes} right).
%In both cases, as the number of visual component nodes increases, the number of queries and thus the number of 
%threads and amount of processing per thread increases which leads to an increase in overall time. In the first case, no-opt and parallel show
%similar behaviour since the added nodes are dependent on the nodes earlier in the chain, while in the second case parallel optimization results 
%in simultaneous processing of multiple chains  resulting in a substantail improvement over the no-opt. Similarly,in both cases, speculate
%incurs an overhead of creating a thread for every new node while smartfuse avoids creating additional threads if they can be merged into the existing threads,
%and thus shows an improvement of about 20\% over speculate.

\begin{figure}[!h]
\vspace{-10pt}
\begin{center}
\includegraphics[width=0.48\columnwidth]{figs/optrealrowdataset.pdf}
\includegraphics[width=0.48\columnwidth]{figs/optrealcoldataset.pdf}
\end{center}
\vspace{-20pt}
\caption{\fuse  on PostgreSQL (left) and Vertica (right)}
\vspace{-15pt}
\label{fig:optrowcolumstores}
\end{figure}


%\noindent
%\textbf{Interactivity}
%Figure\ref{fig:optrowcolumstores}  depicts the time taken by three real world
%queries when applying swiftfuse optimization based query execution as compared
%to a single group by query on datasets of size 2M, 20M and 120M. As the size of
%dataset increases we observe that it is impossible to provide interative
%response time over disk-based relational databases since the time take to apply
%a single group by itself takes multiple seconds and a typical \zq involves
%multiple group by queries even after optimizations. For providing
%interactivity, existing visual analytics tools either cache all the data
%inmemory, or make use of large inmemory databases for avoiding expensive disk
%ios, which are orthogonal to the query optimization technique we propose in
%this paper. Even for \zv, we find (as depicted in
%Figure\ref{fig:optrowcolumstores}) that if we use vertica with large memory
%(7GB) to cache majority of the rows, there is an improvement upto 50X over
%postgresql. For 20M dataset (1.5GB in size and thus completely cached), the
%sub-second overall reponse times, and for 120M dataset (11 GB, about 70\% rows
%cached), we see a reduction in response times from multiple 100s of seconds to
%a few seconds. 
%   
