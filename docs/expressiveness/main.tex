\documentclass{article}

\usepackage{amsmath}
\usepackage{relsize}
\usepackage{xcolor}
\usepackage{xspace}

\newcommand{\zq}{{\sf ZQL}\xspace}

\newcommand{\IN}{$<$--\xspace}

\newcommand{\alkim}[1]{\noindent{\textcolor{red}{Alkim: #1}}}

\title{Formalization of Expressiveness of \zq}
\author{Albert Kim}

\begin{document}
\maketitle

%A visual source is uniquely defined by a given X and Y attributes and any
%selection predicates for it. An example is:
%\[
%  V(X={\rm year}, Y={\rm sales}, {\rm product=`chair'})
%\]
%
%Assuming $\mathcal{V}$ is the set of all possible visual sources from given
%relation $\mathcal{R}$, to get the set of visual sources for all products where
%year is the X-axis and sales is the Y-axis, we represent the operator as:
%\[
%  \pi_{X={\rm year},Y={\rm sales}} (\sigma_{{\rm product = *}} (\mathcal{V}))
%\]
%
%Given the above, the reasons why $\pi$ and $\sigma$ are not good operators for
%visual expression algebra:
%\begin{itemize}
%  \item
%    We don't allow disjunction for iteration (i.e., {\tt v1 \IN `product'.* OR
%    v2 \IN `location'.*}); maybe a reason we shouldn't use $\sigma$
%  \item
%    Both $\pi$ and $\sigma$ are required for any visual source, so it kind of
%    doesn't make sense to separate them out in two different operators; it
%    might just getting confusing because people might try to map relational
%    algebra.
%  \item
%    Regardless of whether the iteration occurs in X/Y (part of $\pi$) or a
%    different attribute we're selecting on (part of $\sigma$), they both still
%    add the same dimensions in our tensor of visual sources.
%\end{itemize}
%
%Here is how we would write it if was combined:
%\[
%  \phi_{X={\rm year}, Y={\rm sales}, {\rm product = *}} (\mathcal{V})
%\]
%
%This would represent 1-dimensional tensor:
%\[
%  \begin{bmatrix}
%    V(X={\rm year}, Y={\rm sales}, {\rm product=`chair'}) \\
%    V(X={\rm year}, Y={\rm sales}, {\rm product=`table'}) \\
%    \vdots
%  \end{bmatrix}
%\]
%
%If we wanted to find the most similar visual sources given an input, we would
%use:
%\[
%  f \sim \phi_{X={\rm year}, Y={\rm sales}, {\rm product = *}} (\mathcal{V})
%\]
%In tensor world, this would look like:
%\[
%  \begin{bmatrix}
%    f
%  \end{bmatrix}
%  \sim
%  \begin{bmatrix}
%    V(X={\rm year}, Y={\rm sales}, {\rm product=`chair'}) \\
%    V(X={\rm year}, Y={\rm sales}, {\rm product=`table'}) \\
%    \vdots
%  \end{bmatrix}
%\]
%
%We want the visual sources which look most similar or dissimilar, so it would
%be fine if just had:
%\[
%  \begin{bmatrix}
%    V(X={\rm year}, Y={\rm sales}, {\rm product=`chair'}) \\
%    V(X={\rm year}, Y={\rm sales}, {\rm product=`table'}) \\
%    \vdots
%  \end{bmatrix}
%\]
%But it might make sense to have 2-dimensional tensor:
%\[
%  \begin{bmatrix}
%    f & V(X={\rm year}, Y={\rm sales}, {\rm product=`chair'}) \\
%    f & V(X={\rm year}, Y={\rm sales}, {\rm product=`table'}) \\
%    \vdots
%  \end{bmatrix}
%\]
%
%For relation $\mathcal{R}$, we create a separate relation $\mathcal{M}$ with
%only two attributes X and Y, which have all the possible values for the X- and
%Y- axes. Then for each attribute in $\mathcal{R}$, we add an extra symbol *,
%then $\mathcal{V} = \mathcal{M} \times \left( \times_{i=1}^k
%\sigma_i(\mathcal{R}) \cup *\right)$. Each tuple from this relation represents
%a visual source.
%
%Based on the above relation, we can start picking out visual sources using
%basic relational algebra:
%\[
%  \sigma_{X={\rm year},Y={\rm sales}, {\rm product}\ne *, ...}(\mathcal{V})
%\]
%In the $...$ above, every other attribute is set to *.
%
%Note, this does not allow specific subsetting? Rather it just makes it hard to
%do. We can't have subsetting or disjunctions (what about {1,2,*})
%
%\[
%  \sigma_{v} T(\phi)
%\]

\section{Foreword}
I have reformalized things in a more crisp way that more
database people are likely to understand (compared to tensors...). Essentially,
visual expression languages can be thought of as special case relations, and we
extend relational algebra to fit what we want. Since we both borrow and use
operators from relational algebra (e.g., $\sigma$, $\tau$), all operators
specialized for visual expression will have a $v$ superscript (e.g.,
$\sigma^v$, $\tau^v$).

\section{Set-Up}
Assume we are given $k$-ary relation $\mathcal{R}$, which has attributes
$(A_1,A_2,...,A_k)$. Let $\mathcal{X}$ be the unary relation which has
single (meta-)attribute X and whose values include all attributes
from $(A_1,A_2,...,A_k)$ which can be used for the x-axis and the wildcard $*$
(by default, all attributes are included: X = $\{A_1,A_2,...,A_k,*\}$) Let
$\mathcal{Y}$ and Y be
defined similarly for the y-axis. We can define $\mathcal{V}$, the \emph{visual
space}, as follows:
\[
  \mathcal{V} = \mathcal{X} \times \mathcal{Y} \left(
  \mathlarger{\mathlarger{\mathlarger{\times}}}_{i=1}^k \pi_{i}(\mathcal{R})
\cup * \right)
\]
Any subset relation $S \subseteq \mathcal{R}$ is called a \emph{visual group}
and any $k+2$-tuple from $\mathcal{V}$ is called a \emph{visual source}. For
example a visual source might look like:
\[
  (\text{X=`year'},\text{Y=`sales'},\text{product=`chair'},\text{location=}*,...)
\]
Assuming, the remaining attributes all have value $*$, the above $k+2$-tuple
represents the visual source for all visualizations which have `year' as its
x-axis and `sales' as its y-axis for the chair product.

From $\mathcal{V}$, we can quickly get individual visual sources or sets of
visual sources quickly using our specialized $\sigma^v$ operator. The
$\sigma^v$ operator is different from the $\sigma$ operator in the following
ways:
\begin{itemize}
  \item
    $\sigma_{\theta}$ allows a number of binary operators and unary operators
    to appear in the condition, but $\sigma^v_{\theta}$ only allows binary
    operators $=$ and $\ne$ for the condition.
    \alkim{
      In the future, we should allow other binary operators; this may allow for
      iterating over all elements $\{x : x < 10\}$, something currently not
      possible in \zq.
    }
  \item
    Only conjunctions are allowed in the condition for $\sigma^v_{\theta}$ and
    the conjunctions are denoted using the comma.
    \alkim{
      If we allow disjunctions, weird things like all visual sources where we
      iterate over products or locations happen. However, this rule limits our
      ability to create subsets (something which \zq does allow).
    }
  \item
    Any attributes, $A$, which do not appear as part of the condition are
    assumed to be set to the condition that they are equal to the wildcard:
    $A=*$.
  \item
    $\sigma^v$ must have conditions for both X and Y attributes.
  \item
    If X=$*$ or Y=$*$ are part of the condition, the operation is undefined.
    \alkim{
      To make this cleaner, we can also just get rid of the $*$ from the X and
      Y attributes and add the $\in$ boolean operator as part of the condition.
      But then, you can get weird things like
    }
  \item
    If $\ne$ is used on attribute, $A$, then at least one of the constraints
    must be $A \ne *$.
    \alkim{
      Maybe a better way to define this would be to say any visual group cannot
      have both $*$ and non-$*$ values for any attribute. However, this would
      require that visual groups are strict subsets of the visual space. This
      probably makes sense since our later operators probably don't make sense
      if any attribute has both * and non-* values.
    }
    \alkim{
      Otherwise the weird disjunction case from above can be repeated. Maybe
      this is something we should allow in \zq.
    }
\end{itemize}
Given these rules, we can for example quickly select the set of visual sources
where we vary the product:
\[
  \sigma^v_{\text{X=year},\text{Y=sales},\text{product}\ne *}(\mathcal{V})
\]

Note that this also allows ``everything-but'' subsets like $*-\{\text{chair}\}$
via conjunctions of negations \[
  \sigma^v_{\text{X=year},\text{Y=sales},\text{product}\ne *,\text{product}\ne
  \text{chair}}(\mathcal{V})
\]
but does not allow ``ground-up'' subsets like
$\text{product}=\{\text{chair},\text{table}\}$.

In addition to the visual sources from $\mathcal{R}$, every time the user
inputs a visualization $f$, a visual source is added $\mathcal{V}$:
\[
  \mathcal{V} \leftarrow \mathcal{V} \cup (f_X,f_Y,f_1,...f_k)
\]


\section{Visual Expression Operators}
Visual expression operators operate on visual groups.

\subsection{Unary Operators}
The unary operator $\pi$ may be used as is. \alkim{Change of basis can be done
by taking projection and then the Cartesian product again.}
%First, we allow the unary operator $\pi^v$. Unlike, the regular $\pi$ operator,
%numbers refer to the columns 3 to $k+2$; to refer to the first two columns,
%X and Y must be used.

Then we allow sorts $\tau^v_T$ on visual groups. However instead of sorting based
on a specific attribute, $\tau^v_T$ sorts based on given trend-measuring
function $T$. $T$ is applied on every visual source in $\tau^v_T(\mathcal{V})$
and is ordered in increasing order.

We can then trim using the $\mu^v_k(\mathcal{V})$ which only takes the first
$k$ tuples from $\mathcal{V}$, borrowed from \cite{li2005ranksql}. Note that
$\mu^v$ does not follow the same semantics as the $\mu$ from the paper.
\alkim{Maybe we can combine this $\tau$}

We can also take the $k$-most representative visual sources using
$\rho^v_k(\mathcal{V})$.

\alkim{Have to figure out what happens for nested iterations.}

\subsection{Binary Operators}
For visual groups $G$ and $H$ to interact with each other in a binary operator,
the following must be true. For each attribute $i = \{X,Y,A_1,...,A_k\}$,
either
\begin{itemize}
  \item
    $|G_i| = 1$ or $|H_i| = 1$
  \item
    $|G_i| = |H_i|$
\end{itemize}
$\phi^v_D(G,H)$ returns a pair of visual groups, each sorted based on $D$ in
increasing order. \alkim{Have to figure out what happens for nested
iterations.}


\section{Notes}
\begin{itemize}
  \item
    Visual expression algebra must use set of operations to go from visual
    group to visual group, so $\pi^v$ by itself does not work.
  \item
    The Constraints column can also be integrated if we expand our visual
    expression language to allow ourselves to put further constraints on the
    original relation $\mathcal{R}$ before turning it into the visual space
    $\mathcal{V}$.
  \item
    Multiple Z columns need to be in main paper for above formalization to be
    proven true.
  \item
    In the visual expression language, we currently do not allow threshold
    cutoffs; we can add that if we really want.
  \item
    Might want to mention in paper that a value of $\infty$ is allowed for $k$
    for \zq.
  \item
    Come up with better names for visual space, visual group, and visual
    source.
  \item
    The syntax for representative set in \zq kind of makes people assume that
    only one variable can be given at a time.
  \item
    We use the ordered set notation for the algebra.
\end{itemize}



%A visual source is defined
%to be a $k+2$-ary tuple in which the first two elements refer to the attribute 

\bibliographystyle{plain}
\bibliography{main}

\end{document}
