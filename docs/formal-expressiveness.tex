%!TEX root=zenvisageql.tex
\section{Expressiveness}
\label{sec:formal-expr}

In this section, we formally quantify the expressive power of \zq. To this end,
we formulate an algebra, called the \emph{\vzalg}.
Like relational algebra, \vzalg contains a basic
set of operators that we believe all \vzlans should be able to
express.
At a high level, the operators of
our \vzalg operate on sets of visualizations and are not
mired by the data representations of those visualizations, nor the details of how
the visualizations are rendered. Instead, the \vzalg is primarily
concerned with the different ways in which visualizations can be selected,
refined, and compared with each other.

Given a function $T$ that operates on a visualization at a time, 
and a function $D$ that operates on a pair of visualizations at a time, 
both returning real-valued numbers,
a \vzlan $L$ is defined to be
\emph{\vzcmp} $VEC_{T,D}(L)$ with respect to $T$ and $D$ if it supports
all the operators of the \vzalg.
These functions $T$ and $D$ (also defined previously) 
are ``functional primitives'' without which the resulting
algebra would have been unable to manipulate visualizations in the way
we need for data exploration. 
Unlike relational algebra, which does not have any ``black-box''
functions, \vzalg requires these functions for operating on visualizations
effectively. 
That said, these two functions are flexible and configurable and up to the user
to define (or left as system defaults).
Next, we formally define the \vzalg operators and prove
that \zq is  \vzcmp. 

\input{ordered-bag-algebra}

\subsection{Basic Notation}
Assume we are given a $k$-ary relation $\calR$ with attributes
$(A_1,A_2,\dots,A_k)$.
Let $\calX$ be the unary relation with attribute X whose values are the
names of the attributes in $\calR$ that can appear on the x-axis. If the
x-axis attributes are not specified by the user for relation $\calR$, the
default behavior is to include all attributes in $\calR$: $\{A_1,\dots,A_k\}$. Let
$\calY$ be defined similarly with Y for attributes that can appear on the y-axis.
Given $\calR$, $\calX$, and $\calY$, we define $\calV$, the \emph{\vzuni},
as follows: $
  \calV = \nu(\calR) = \calX \times \calY \times
  \left(
    \mathlarger{\mathlarger{\times}}_{i=1}^k \pi_{A_i}(\calR) \cup \{*\}
  \right)$
where $\pi$ is the projection operator from relational algebra and \wild
is a special wildcard symbol, used to denote all values of an attribute.
Table~\ref{tab:setup} shows an example of what a sample $\calR$ and
corresponding $\calX$, $\calY$, and $\calV$ would look like.
\techreport{
    At a high level, the visual universe specifies all subsets of data that may
    be of interest, along with the intended attributes to be visualized. Unlike
    relational algebra, \vzalg mixes schema and data elements, but in a special
    way in order to operate on a collection of visualizations.
}


% \scalebox{0.71}{
%    \begin{tabular}{|c|c|c|c|c|c|}
%       \hline
%       year & month & product & location & sales & profit \\ \hline
%       2016 & 4 & chair & US & 623,000 & 314,000 \\ \hline
%       2016 & 3 & chair & US & 789,000 & 410,000 \\ \hline
%       2016 & 4 & table & US & 258,000 & 169,000 \\ \hline
%       2016 & 4 & chair & UK & 130,000 & 63,000 \\
%       \multicolumn{6}{|c|}{\vdots} \\
%     \end{tabular}
% }
% \quad
% \scalebox{0.71}{
%     \begin{tabular}{|c|}
%       \hline
%       X \\ \hline
%       year \\ \hline
%       month \\ \hline
%     \end{tabular}
% }
% \quad
% \scalebox{0.71}{
%      \begin{tabular}{|c|}
%       \hline
%       Y \\ \hline
%       sales \\ \hline
%       profit \\ \hline
%   \end{tabular}
% }

% \scalebox{0.71}{
% \begin{tabular}{|c|c|c|c|c|c|c|c|}
%       \hline
%       X & Y & year & month & product & location & sales & profit \\ \hline
%       year & sales & \wild & \wild & \wild & \wild & \wild & \wild \\ \hline
%       year & profit & \wild & \wild & \wild & \wild & \wild & \wild \\ \hline
%       year & sales & \wild & \wild & chair & \wild & \wild & \wild \\ \hline
%       year & sales & \wild & \wild & chair & US & \wild & \wild \\
%       \multicolumn{8}{|c|}{\vdots} \\
%     \end{tabular}
% }


\begin{table*}[!t]
  \vspace{10pt}
  \begin{subtable}{0.4\linewidth}
    \scriptsize
    \begin{tabular}{|c|c|c|c|c|c|}
      \hline
      year & month & product & location & sales & profit \\ \hline
      2016 & 4 & chair & US & 623,000 & 314,000 \\ \hline
      2016 & 3 & chair & US & 789,000 & 410,000 \\ \hline
      2016 & 4 & table & US & 258,000 & 169,000 \\ \hline
      2016 & 4 & chair & UK & 130,000 & 63,000 \\
      \multicolumn{6}{|c|}{{\bf \vdots}} \\
    \end{tabular}
    \caption{Example $\calR$}
    \label{subtab:ex-R}
     \end{subtable}
       \hspace{-10pt}
  \begin{subtable}{.1\linewidth}
    \scriptsize
    \begin{tabular}{|c|}
      \hline
      X \\ \hline
      year \\ \hline
      month \\ \hline
    \end{tabular}
    \caption{$\calX$\hphantom{ for $\calR$}}
    \label{subtab:ex-X}
  \end{subtable}
  \hspace{-10pt}
  \begin{subtable}{.1\linewidth}
        \scriptsize
    \begin{tabular}{|c|}
      \hline
      Y \\ \hline
      sales \\ \hline
      profit \\ \hline
    \end{tabular}
    \caption{$\calY$\hphantom{  for $\calR$}}
    \label{subtab:ex-Y}
  \end{subtable}
  \hspace{-10pt}
  \begin{subtable}{0.3\linewidth}
    \scriptsize
    \begin{tabular}{|c|c|c|c|c|c|c|c|}
      \hline
      X & Y & year & month & product & location & sales & profit \\ \hline
      year & sales & \wild & \wild & \wild & \wild & \wild & \wild \\ \hline
      year & profit & \wild & \wild & \wild & \wild & \wild & \wild \\ \hline
      year & sales & \wild & \wild & chair & \wild & \wild & \wild \\ \hline
      year & sales & \wild & \wild & chair & US & \wild & \wild \\
      \multicolumn{8}{|c|}{{\bf \vdots}} \\
    \end{tabular}
    \caption{$\calV$ for $\calR$}
    \label{subtab:ex-V}
  \end{subtable}
  \vspace{-5pt}
\caption{An example relation $\calR$ and its resultant $\calX$, $\calY$,
  and $\calV$.}
  \label{tab:setup}
  \vspace{-15pt}
\end{table*}


Any subset relation $V \subseteq \calV$ is called a \emph{\vzgrp}, and any
$k+2$-tuple from $\calV$ is called a \emph{\vzchn}.
The last $k$ portions (or attributes) of a tuple from $\calV$
comprise the {\em data source} of the \vzchn.
Overall, a \vzchn
represents a visualization that can be rendered from a selected data source, and a
set of \vzchns is a \vzgrp. The X
and Y attributes of the \vzchn determine the x- and y- axes, and the
selection on the data source is determined by attributes $A_1,\dots,A_k$. If
an attribute has the wildcard symbol \wild as its value, no subselection is
performed on that attribute for the data source.
For example, the third row of Table~\ref{subtab:ex-V} is a \vzchn that
represents the visualization with year as the x-axis and sales as the y-axis for
chair products. Since the value of location is \wild, all locations are
considered valid or pertinent for the data source.
In relational algebra, the data source
for the third row can be written 
as $\sigma_{\text{product=chair}}(\calR)$.
The
\wild symbol therefore attempts to emulate the lack of presence 
of a selection condition on that attribute in the $\sigma$
operator of the relational algebra. 
Readers familiar with OLAP will notice the similarity between the use of the
symbol \wild here 
and the GROUPING SETS functionality in SQL.

Note that infinitely many
visualizations can be produced from a single visual source, due to different
granularities of binning, aggregation functions, and types
of visualizations that can be constructed, 
since a visualization generation engine can use a
visualization rendering grammar like ggplot~\cite{ggplot} that provides that functionality.
Our focus in defining the \vzalg is to specify the inputs to a visualization
and attributes of interest as opposed to the aesthetic aspects and encodings.
Thus, for our discussion, we assume that each visual source maps 
to a singular visualization.
Even if the details of the encoding and aesthetics are not provided,
standard rules may be applied for this mapping as alluded
earlier~\cite{wongvoyager2015,DBLP:journals/cacm/StolteTH08} in
Section~\ref{subsec:formalization}.
Furthermore,
a \vzchn does not specify the data representation of the underlying data source;
therefore the expressive power of \vzalg is not tied to any specific backend data storage
model.
The astute reader will have noticed that the format for a \vzchn looks fairly
similar to a collections of visualizations in \zq; this is no accident. In fact, we
will use the visualization collections of \zq as a proxy to \vzchns when proving that
\zq is \vzcmp.

\subsection{Functional Primitives}

Earlier, we mentioned that a \vzalg is \vzexp complete 
with respect to two functional primitives: $T$ and $D$.
Here we define the formal types for these functional primitives with respect to
\vzalg.


The function $T: \calV \rightarrow \mathbb{R}$ returns a real number given
a \vzchn. This function can be used to assess whether a trend:
defined by the visualization corresponding to a specific \vzchn,
is ``increasing'', or ``decreasing'', or satisfies some other fixed property. Many such $T$ can be defined and used within the \vzalg.

The function $D: \calV \times \calV \rightarrow \mathbb{R}$ returns a real
number given a pair of \vzchns. This function can be used
to compare pairs of visualizations (corresponding to the \vzchns)
with respect to each other. The most natural way to define $D$ is
via some notion of distance, e.g., Earth Mover's or Euclidian distance, 
but once again, the definition can be provided by the user or assumed as a fixed black box.


\input{viz-exp-operators}
\input{viz-exp-proof}
