package settings;

/* From SeedB  */
public class DBSettings {
	public String database;
	public String databaseType;
	public String username;
	public String password;
	
	public static DBSettings getLocalDefault() {
		DBSettings s = new DBSettings();
		s.database = "127.0.0.1/TODO";
		s.databaseType = "postgresql";
		s.username = "TODO";
		s.password = "TODO";
		return s;
	}
	
	public static DBSettings getDefault() {
		return getLocalDefault();
	}
	
	public static DBSettings getPostgresDefault() {
		DBSettings s = new DBSettings();
		s.database = "TODO";
		s.databaseType = "postgresql";
		s.username = "TODO";
		s.password = "TODO";
		return s;
	}

}